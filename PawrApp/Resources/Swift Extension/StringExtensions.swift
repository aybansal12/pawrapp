//
//  StringExtensions.swift
//  PawrApp
//
//  Created by Ayush Bansal on 15/03/21.
//

import Foundation
import UIKit

extension String {
//    func inserting(separator: String, every n: Int) -> String {
//        var result: String = ""
//        let characters = Array(self.characters)
//        stride(from: 0, to: characters.count, by: n).forEach {
//            result += String(characters[$0..<min($0+n, characters.count)])
//            if $0+n < characters.count {
//                result += separator
//            }
//        }
//        return result
//    }
    
    //TODO :- Func to seperate string after every n characters
//    func separateString(every stride: Int = 4, with separator: Character = " ") -> String {
//        return String(enumerated().map { $0 > 0 && $0 % stride == 0 ? [separator, $1] : [$1]}.joined())
//    }
    
    var localized: String {
        let localizedString = NSLocalizedString(self, comment: "")
        if localizedString == self {
            return Bundle.base.localizedString(forKey: self, value: nil, table: nil)
        } else {
            return localizedString
        }
    }
    
    func separateString(every: Int, with separator: String) -> String {
        return String(stride(from: 0, to: Array(self).count, by: every).map {
            Array(Array(self)[$0..<min($0 + every, Array(self).count)])
        }.joined(separator: separator))
    }
}

extension Bundle {
    static let base: Bundle = {
        if let path = Bundle.main.path(forResource: "Base", ofType: "lproj") {
            if let baseBundle = Bundle(path: path) {
                return baseBundle
            }
        }
        return Bundle.main
    }()
}
