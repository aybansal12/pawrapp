//
//  MoreExtension.swift
//  PawrApp
//
//  Created by Ayush Bansal on 31/03/21.
//

import Foundation
import UIKit
import CoreLocation

//extension UITextFieldDelegate {
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//            CommonHelper.sharedInstance.setTextFieldCharacterSpacing(txtField: textField as! TextFieldCustomize)
//        return true
//    }
//}

extension CLLocation {
    func placemark(completion: @escaping (_ placemark: CLPlacemark?, _ error: Error?) -> ()) {
        CLGeocoder().reverseGeocodeLocation(self) { completion($0?.first, $1) }
    }
}
