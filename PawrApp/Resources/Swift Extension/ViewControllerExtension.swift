//
//  ViewControllerExtension.swift
//  PawrApp
//
//  Created by Ayush Bansal on 12/03/21.
//

import Foundation
import UIKit

var loaderView : UIView!
let defaults = UserDefaults.standard
var userLoginData = NSDictionary()

extension UIViewController {
    
    func showToast(message : String, duration:CGFloat) {  //font: UIFont
        let toastLabel = UILabel()
        
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.font = UIFont(name: TextFontStyle.MONTSERRAT_MEDIUM, size: CGFloat(FontSize.HEADINGFONT12))
        toastLabel.textAlignment = .center;
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        toastLabel.numberOfLines = 2
        
        //calculating toast label frame as per message content
        let maxSizeTitle : CGSize = CGSize(width: self.view.frame.size.width-16, height: self.view.frame.size.height)
        var expectedSizeTitle : CGSize = toastLabel.sizeThatFits(maxSizeTitle)
        
        // UILabel can return a size larger than the max size when the number of lines is 1
        expectedSizeTitle = CGSize(width:expectedSizeTitle.width, height: expectedSizeTitle.height)
        
        let lblHeight = self.view.frame.size.height - (expectedSizeTitle.height+60)
        toastLabel.frame = CGRect(x:((self.view.frame.size.width)/2) - ((expectedSizeTitle.width+16)/2), y:lblHeight, width: expectedSizeTitle.width+16, height: expectedSizeTitle.height+16)
        
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: TimeInterval(duration), delay: 1.5, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    
    //TODO :- Func to show activity loader
    func showActivityLoader() {
        loaderView = UIView(frame: self.view.bounds)
        loaderView.backgroundColor = customColor.BLACK_COLOR_WITH_ALPHA
        
        let activityIndi = UIActivityIndicatorView(style: .whiteLarge)
        activityIndi.center = loaderView.center
        activityIndi.color = customColor.NEW_THEME_GREEN_COLOR
        activityIndi.startAnimating()
        
        loaderView.addSubview(activityIndi)
        self.view.addSubview(loaderView)
    }
    
    //TODO :- Func to hide activity loader
    func hideActivityLoader() {
        loaderView.removeFromSuperview()
        loaderView = nil
    }
    
    //TODO :- Func to upload profile pic
    func uploadProfilePic(profileImage : UIImage , completion: @escaping ((Int) -> Void)) {
        
        let Base_URL = Utilities.returnBaseURL(liveURL: 0)
        let apiURl = Base_URL + ApiList.UPLOAD_MEDIA_FILE
        
        let userPassword = defaults.getUserLoginPassword()
        
        if (defaults.object(forKey:UserDefaults.UserDefaultsKeys.userLoginData.rawValue) != nil) {
            userLoginData = defaults.getUserLoginData() as NSDictionary
        }
        let userMobile = userLoginData["mobile"] as! String
        
        let paramValues = ["username": userMobile, "password": userPassword] as [String : Any]
        
        self.showActivityLoader()
        ServiceManager.sharedInstance.uploadImageOnServer(url:apiURl, param_Values: paramValues, requestMethod: "post", imageData: profileImage, callbackSuccess: { (responseData, apiStatusCode) -> Void in

            let responseDic = responseData as! NSDictionary
            if apiStatusCode == 200 {
                let profileID = responseDic["fileId"] as! Int
                completion(profileID)
            }
            else {
                self.hideActivityLoader()
                let mesgString = responseDic["message"] as! String
                self.showToast(message: mesgString, duration: 2.0)
                completion(-1)
            }
            self.hideActivityLoader()
            
        },callbackFaliure: { (error:String, message:String) -> Void in
            self.hideActivityLoader()
            self.showToast(message: message, duration: 2.0)
            completion(-1)
        })
    }
    
    //TODO :- Func to fetch mobi coin balance
    func fetchMobiCoinBalance() {
        let Base_URL = Utilities.returnBaseURL(liveURL: liveApi)
        let apiURl = Base_URL + ZYPP_COIN_API.FETCH_USER_MOBICOIN_BALANCE

        self.showActivityLoader()
        ServiceManager.sharedInstance.callApi(url:apiURl, param_Values: nil, requestMethod: "get", callbackSuccess: { (responseData, apiStatusCode) -> Void in

            if apiStatusCode == 200 {
                if !Utilities.isEmptyValue(responseData) {
                    let responseDic = responseData as! NSDictionary
                    let mobiCoin = String(responseDic["paytmWalletBalance"] as! Int)
                    
                    defaults.removeObject(forKey:UserDefaults.UserDefaultsKeys.userZyppCoinBalance.rawValue)
                    defaults.setUserZyppCoinBalance(value: mobiCoin)
                }
            }
            self.hideActivityLoader()
           
        },callbackFaliure: { (error:String, message:String) -> Void in
            self.hideActivityLoader()
            self.showToast(message: message, duration: 2.0)
        })
    }
}
