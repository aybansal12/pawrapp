//
//  UserDefaultsData.swift
//  PawrApp
//
//  Created by Ayush Bansal on 09/03/21.
//

import Foundation

extension UserDefaults {
    enum UserDefaultsKeys : String {
        case isLoggedIn
        case userLoginData
        case paymentLinkedWalletData
        case userLoginPassword
        case userZyppCoinBalance
        case nStationSwapAmount
        case smartStationSwapAmount
    }
    
    //MARK: Set user login status
    func setUserLogIn(value: String) {
        set(value, forKey: UserDefaultsKeys.isLoggedIn.rawValue)
    }
    
    //TODO :- Func to set user login complete data
    func setUserLoginData(value: NSDictionary) {
        set(value, forKey: UserDefaultsKeys.userLoginData.rawValue)
    }
    
    //TODO :- Func to set user payment linked wallet
    func setUserPaymentLinkWallet(value: String) {
        set(value, forKey: UserDefaultsKeys.paymentLinkedWalletData.rawValue)
    }
    
    //TODO :- Func to set user login password
    func setUserLoginPassword(value: String) {
        set(value, forKey: UserDefaultsKeys.userLoginPassword.rawValue)
    }
    
    //TODO :- Func to set user zypp coin Balance
    func setUserZyppCoinBalance(value: String) {
        set(value, forKey: UserDefaultsKeys.userZyppCoinBalance.rawValue)
    }
    
    //TODO :- Func to set normal station swap amount
    func setNStationSwapAmount(value: String) {
        set(value, forKey: UserDefaultsKeys.nStationSwapAmount.rawValue)
    }
    
    //TODO :- Func to set smart station swap amount
    func setQStationSwapAmount(value: String) {
        set(value, forKey: UserDefaultsKeys.smartStationSwapAmount.rawValue)
    }
    /************************* End Set Data and Start Retriving Data    ******************************/
    
    //TODO: Retrive user login status
    func getUserLogIn() -> String {
        return string(forKey: UserDefaultsKeys.isLoggedIn.rawValue)!
    }
    
    //TODO :- Func to get user login complete data
    func getUserLoginData() -> NSDictionary {
        return self.value(forKey: UserDefaultsKeys.userLoginData.rawValue) as! NSDictionary
    }
    
    //TODO :- Func to get user linked payment wallet
    func getUserPaymentLinkWallet() -> String {
        return string(forKey: UserDefaultsKeys.paymentLinkedWalletData.rawValue)!
    }
    
    //TODO :- Func to get user login password
    func getUserLoginPassword() -> String {
        return string(forKey: UserDefaultsKeys.userLoginPassword.rawValue)!
    }
    
    //TODO :- Func to get user zypp coin balance
    func getUserZyppCoinBalance() -> String {
        return string(forKey: UserDefaultsKeys.userZyppCoinBalance.rawValue)!
    }
    
    //TODO :- Func to get user normal station swap amount
    func getNStationSwapAmount() -> String {
        return string(forKey: UserDefaultsKeys.nStationSwapAmount.rawValue)!
    }
    
    //TODO :- Func to get smart station swap amount
    func getQStationSwapAmount() -> String {
        return string(forKey: UserDefaultsKeys.smartStationSwapAmount.rawValue)!
    }
}
