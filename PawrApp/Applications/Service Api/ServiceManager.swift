//
//  ServiceManager.swift
//  PawrApp
//
//  Created by Ayush Bansal on 13/03/21.
//

import Foundation
import Alamofire
import RxAlamofire
import RxSwift
import UIKit

//enum APIError: Error {
//    case invalidInput
//    case badResponse
//
//    func toNSError() -> NSError {
//        let domain = "com.github.rxswiftcommunity.rxalamofire"
//        switch self {
//        case .invalidInput:
//            return NSError(domain: domain, code: -1, userInfo: [NSLocalizedDescriptionKey: "Input should be a valid number"])
//        case .badResponse:
//            return NSError(domain: domain, code: -2, userInfo: [NSLocalizedDescriptionKey: "Bad response"])
//        }
//    }
//}

class ServiceManager {
    
    static let sharedInstance:ServiceManager = ServiceManager()
    let disposeBag = DisposeBag()
    let defaults = UserDefaults.standard
    
    func callApi(url:String, param_Values:[String:Any]? = nil, requestMethod:String, callbackSuccess:@escaping (_ responseData: Any, _ apiStatusCode: Int) -> Void, callbackFaliure:@escaping ( _ errorTitle:String,_ errorMessage:String) -> Void) {
        
        let completeUrlString = "\(url)".addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        var authStr = ""
        var authValue = ""
        var request_Method:HTTPMethod = HTTPMethod.get
        var Auth_header : HTTPHeaders = ["Content-Type":"application/json"]
        
        if requestMethod == "post" {
            request_Method = HTTPMethod.post
        }
        else {
            request_Method = HTTPMethod.get
            
            if url.contains("secure/user/login") {
                if param_Values != nil {
                    for objDic in param_Values! {
                        if objDic.key == "username" {
                            authStr =  authStr + "\(objDic.value as! String):"
                        }
                        else if objDic.key == "password" {
                            authStr =  authStr + "\(objDic.value as! String)"
                        }
                    }
                    
                    let authData = authStr.data(using: .ascii)
                    authValue = "Basic \(authData?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 80)) ?? "")"
                    Auth_header.add(name: "Authorization", value: authValue)
                }
            }
        }
        
        if (defaults.object(forKey:UserDefaults.UserDefaultsKeys.userLoginData.rawValue) != nil) {
            let userLoginData = defaults.getUserLoginData() as NSDictionary
            
            if userLoginData.count > 0 {
                let loginAccess = userLoginData["accessToken"] as! String
                
                Auth_header.add(name: "Authorization", value: "Bearer \(loginAccess)")
            }
        }
        
        RxAlamofire.requestJSON(request_Method, completeUrlString!, encoding: JSONEncoding.default, headers: Auth_header) .debug() .subscribe(onNext: {(r, json) in
            if (r.statusCode == 200 || r.statusCode == 401 || r.statusCode == 403){
                callbackSuccess(json, r.statusCode)
            }
            else {
                callbackFaliure("Error", customMessages.SOMETHING_WENT_WRONG)
            }
        }, onError: {(error) in
            callbackFaliure("Error", error.localizedDescription)
            //self?.displayError(error as NSError)
        })
        .disposed(by: disposeBag)
    }
    
    //TODO :- Func to call post request api
    func callPostApi(url:String, param_Values:[String:Any]? = nil, requestMethod:String, callbackSuccess:@escaping (_ responseData: Any, _ apiStatusCode: Int) -> Void, callbackFaliure:@escaping ( _ errorTitle:String,_ errorMessage:String) -> Void) {
        
//        let completeUrlString = "\(url)".addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
//        let url = URL(string: completeUrlString!)!
//        var request = URLRequest(url: url)
//
//        request.addValue("Content-Type", forHTTPHeaderField: "application/json")
//        request.addValue("Accept", forHTTPHeaderField: "application/json")
//        if (defaults.object(forKey:UserDefaults.UserDefaultsKeys.userLoginData.rawValue) != nil) {
//            let userLoginData = defaults.getUserLoginData() as NSDictionary
//
//            if userLoginData.count > 0 {
//                let loginAccess = userLoginData["accessToken"] as! String
//                request.addValue("Authorization", forHTTPHeaderField: "Bearer \(loginAccess)")
//            }
//        }
//        else {
//            request.addValue("Authorization", forHTTPHeaderField: "Bearer ")
//        }
//
//        if requestMethod == "post" {
//            if (param_Values != nil) {
//                let jsonData = try? JSONSerialization.data(withJSONObject: param_Values as Any)
//                request.httpBody = jsonData
//            }
//            request.httpMethod = "POST"
//        }
    
//        RxAlamofire.requestJSON(request) .debug() .subscribe(onNext: {(r, json) in
//            if r.statusCode == 200 {
//                callbackSuccess(json, r.statusCode)
//            }
//            else if r.statusCode == 401 {
//                callbackSuccess(json, r.statusCode)
//            }
//            else {
//                callbackFaliure("Error", customMessages.SOMETHING_WENT_WRONG)
//            }
//        }, onError: {(error) in
//            callbackFaliure("Error", error.localizedDescription)
//            //self?.displayError(error as NSError)
//        })
//        .disposed(by: disposeBag)
        
        let completeUrlString = "\(url)".addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        var request_Method:HTTPMethod = HTTPMethod.get
        var Auth_header : HTTPHeaders = ["Content-Type":"application/json"]
        
        if requestMethod == "post" {
            request_Method = HTTPMethod.post
        }
        else {
            request_Method = HTTPMethod.get
        }
        
        if (defaults.object(forKey:UserDefaults.UserDefaultsKeys.userLoginData.rawValue) != nil) {
            let userLoginData = defaults.getUserLoginData() as NSDictionary
            
            if userLoginData.count > 0 {
                let loginAccess = userLoginData["accessToken"] as! String
                
                Auth_header.add(name: "Authorization", value: "Bearer \(loginAccess)")
            }
        }
        
        RxAlamofire.requestJSON(request_Method, completeUrlString!, parameters:param_Values, encoding: JSONEncoding.default, headers: Auth_header) .debug() .subscribe(onNext: {(r, json) in
            if (r.statusCode == 200 || r.statusCode == 401 || r.statusCode == 403){
//                if let dict = json as? [String: AnyObject] {
//                    callbackSuccess(dict, r.statusCode)
//                }
                callbackSuccess(json, r.statusCode)
            }
            else {
                callbackFaliure("Error", customMessages.SOMETHING_WENT_WRONG)
            }
        }, onError: {(error) in
            callbackFaliure("Error", error.localizedDescription)
            //self?.displayError(error as NSError)
        })
        .disposed(by: disposeBag)
    }
    
    //TODO :- Func to show errors
    func displayError(_ error: NSError?) {
        print("Print Error")
//        if let e = error {
//            let alertController = UIAlertController(title: "Error", message: e.localizedDescription, preferredStyle: .alert)
//            let okAction = UIAlertAction(title: "OK", style: .default) { _ in
//                // do nothing...
//            }
//            alertController.addAction(okAction)
//            present(alertController, animated: true, completion: nil)
//        }
    }
    
    //TODO :- Func to upload image on the server
    func uploadImageOnServer(url:String, param_Values:[String:Any]? = nil, requestMethod:String, imageData: UIImage, callbackSuccess:@escaping (_ responseData: Any, _ apiStatusCode: Int) -> Void, callbackFaliure:@escaping ( _ errorTitle:String,_ errorMessage:String) -> Void) {
        
        var authStr = ""
        var authValue = ""
        let imgData = imageData.jpegData(compressionQuality: 0.3)!
        let completeUrlString = "\(url)".addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        let url = URL(string: completeUrlString!)!
        var request = URLRequest(url: url)
        
        request.cachePolicy = .reloadIgnoringLocalCacheData
        request.httpShouldHandleCookies = false
        request.timeoutInterval = 50
        
        let boundary = "------VohpleBoundary4QuqLuM1cE5lMwCy"

        // set Content-Type in HTTP header
        let contentType = "multipart/form-data; boundary=\(boundary)"
        request.setValue(contentType, forHTTPHeaderField: "Content-Type")
        
//        request.addValue("multipart/form-data", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        
        if param_Values != nil {
            for objDic in param_Values! {
                if objDic.key == "username" {
                    authStr =  authStr + "\(objDic.value as! String):"
                }
                else if objDic.key == "password" {
                    authStr =  authStr + "\(objDic.value as! String)"
                }
            }
            
            let authData = authStr.data(using: .ascii)
            authValue = "Basic \(authData?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 80)) ?? "")"
            request.addValue("Authorization", forHTTPHeaderField: authValue)
        }
        
        var body = Data()
        if let data = "--\(boundary)\r\n".data(using: .utf8) {
            body.append(data)
        }
        if let data = "Content-Disposition: form-data; name=\"\("file")\"; filename=\"image.jpg\"\r\n".data(using: .utf8) {
            body.append(data)
        }
        if let data = "Content-Type: image/jpg\r\n\r\n".data(using: .utf8) {
            body.append(data)
        }
        body.append(imgData)

        if let data = "\r\n".data(using: .utf8) {
            body.append(data)
        }

        if let data = "--\(boundary)--\r\n".data(using: .utf8) {
            body.append(data)
        }

        request.httpBody = body
        
        let sessionConfiguration = URLSessionConfiguration.default
        sessionConfiguration.httpAdditionalHeaders = [
            "apiVersion": "5.0"
        ]
        let session = URLSession(configuration: sessionConfiguration)
        
        let uploadTask = session.uploadTask(with: request, from: nil, completionHandler: { data, response, error in
            if error == nil {
                var jsonError: Error?
                
                var json: [AnyHashable : Any]? = nil
                do {
                    json = try JSONSerialization.jsonObject(with: data!, options: []) as? [AnyHashable : Any]
                    
                    if (json == nil) {
                        callbackFaliure("Error", customMessages.SOMETHING_WENT_WRONG)
                    }
                    else {
                        callbackSuccess(json as Any, 200)
                    }
                    
                } catch let jsonError {
                    callbackFaliure("Error", customMessages.SOMETHING_WENT_WRONG)
                }
            }
            else {
                callbackFaliure("Error", customMessages.SOMETHING_WENT_WRONG)
            }
        })
        uploadTask.resume()
    }
}
