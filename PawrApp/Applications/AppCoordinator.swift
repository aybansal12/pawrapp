//
//  AppCoordinator.swift
//  PawrApp
//
//  Created by Ayush Bansal on 13/03/21.
//

import Foundation
import UIKit

@available(iOS 13.0, *)
class AppCoordinator {
    private let window : UIWindow
    var defaults = UserDefaults.standard
    //var navController = UINavigationController()
    
    init(window: UIWindow) {
        self.window = window
    }
    
    func start() {
        var userLogin = ""
        if (defaults.object(forKey:UserDefaults.UserDefaultsKeys.isLoggedIn.rawValue) != nil) {
            userLogin = defaults.getUserLogIn()
        }
        else {
            userLogin = "false"
        }
//        userLogin = "false"
        //let isLoggin = defaults.getUserLogIn()
        if userLogin == "true" || userLogin == "True" {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let HomeVC = storyboard.instantiateViewController(withIdentifier: "MainTabBarID")
            
            let navController = UINavigationController(rootViewController: HomeVC)
            navController.setNavigationBarHidden(true, animated: false)
            window.rootViewController = navController
            window.makeKeyAndVisible()
        }
        else {
            let navController = UINavigationController(rootViewController: LoginScreenVC())
            navController.setNavigationBarHidden(true, animated: false)
            window.rootViewController = navController
            window.makeKeyAndVisible()
        }
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let HomeVC = storyboard.instantiateViewController(withIdentifier: "MainTabBarID")
////        self.navController.pushViewController(HomeVC, animated: true)
//
//        let navController = UINavigationController(rootViewController: HomeVC)
//        navController.setNavigationBarHidden(true, animated: false)
//        window.rootViewController = navController
//        window.makeKeyAndVisible()
    }
}
