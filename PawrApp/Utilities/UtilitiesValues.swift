//
//  UtilitiesValues.swift
//  PawrApp
//
//  Created by Ayush Bansal on 25/03/21.
//

import UIKit
import Foundation
import CoreLocation

@available(iOS 13.0, *)
class UtilitiesValues : NSObject {
    
    /*
     TODO :- Func to get user location longitude and latitude
     Return :- User current location longitude and latitude
     */
    static func getUserCurrentLocation() ->  CLLocationCoordinate2D {
        let AppDelegate = UIApplication.shared.delegate as! AppDelegate
        return AppDelegate.locationValue
    }
    
    /*
     TODO :- Func to get user location address string
     Parameters :- User longitude and latitude
     Return :- User current location Address String
     */
    static func getUserAddressString(userLat: Double, userLong: Double, completion: @escaping ((String) -> Void)) {
        
        let location = CLLocation(latitude: userLat, longitude: userLong)
        location.placemark { placemark, error in
            guard let placemark = placemark else {
                print("Error:", error ?? "nil")
                return
            }
            
            var subLocality = ""
            var locality = ""
            
            if placemark.administrativeArea != nil {
                if placemark.thoroughfare != nil {
                    locality = placemark.name!  + placemark.thoroughfare!  + placemark.locality!
                }
                else {
                    locality = placemark.name! + placemark.locality!
                }
                subLocality = placemark.subLocality! + placemark.administrativeArea! + placemark.postalCode!
            }
            else {
                locality = placemark.name!  + placemark.thoroughfare!  + placemark.locality!
                subLocality = placemark.subLocality! + placemark.postalCode!
            }
            
            completion(locality + subLocality)
        }
    }
    
    /*
     TODO :- Func to get user login status
     Return :- user login status
     */
    static func getUserLoginStatus() -> Bool {
        let defaults = UserDefaults.standard
        var userLogin = ""
        if (defaults.object(forKey:UserDefaults.UserDefaultsKeys.isLoggedIn.rawValue) != nil) {
            userLogin = defaults.getUserLogIn()
        }
        else {
            userLogin = "false"
        }
        
        if (userLogin == "true" || userLogin == "True") {
            return true
        }
        else {
            return false
        }
    }
    
    /*
     TODO :- Func to get user login data of the selected parameters
     Return :- user login data string
     */
    static func getLoginData_ParameterString(parameterName: String) -> String {
        let defaults = UserDefaults.standard
        var returnValue = ""
        if (defaults.object(forKey:UserDefaults.UserDefaultsKeys.userLoginData.rawValue) != nil) {
            let userLoginData = defaults.getUserLoginData() as NSDictionary
            
            let parameterString = userLoginData[parameterName] as! String
            returnValue = parameterString
        }
        else {
            returnValue = ""
        }
        
        return returnValue
    }
    
    /*
    TODO :- Func to compare current time and pickNdrop schedule task time
    Parameters : Get pickNDrop schedule task time
    Return : Return true or false
    */
    static func compareCurrentTimeDeliveryTime(batterySwapTime: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss.ss"
        let dateFromString = formatter.date(from: batterySwapTime)
        
        let newFormatter = DateFormatter()
        newFormatter.dateFormat = "EEE, MMM dd, hh:mm aa"
        let StringFromDate = newFormatter.string(from: dateFromString!)
        
        return StringFromDate
    }
}

