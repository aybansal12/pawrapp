//
//  Utilities.swift
//  PawrApp
//
//  Created by Ayush Bansal on 10/03/21.
//

import Foundation
import UIKit
import CoreLocation
import GooglePlaces
import GoogleMaps
import AVKit

class Utilities : NSObject {
    
    /*
     TODO :- Check staging and production url
     Parameters :- Get Value for return api url //0 for staging and 1 for production
     Return : return staging/production url
     */
    static func returnBaseURL(liveURL : Int) -> String {
        if (liveURL == 0) {
            return "http://13.126.228.36/mobycy/api/";
        }
        else if (liveURL == 1){
            return "http://13.126.16.169/mobycy/api/";
        }
        else{
            return "https://220901b3.ngrok.io/mobycy/api/";
        }
    }
    
    static func returnOld_BASEURL(liveURL : Int) -> String  {
        if (liveURL == 0) {
            return "http://13.126.228.36/mobycy/api/";
        }
        else {
            return "http://13.126.16.169/mobycy/api/";
        }
    }
    
    static func returnPayment_BaseURL(liveURL : Int) -> String  {
        if (liveURL == 0) {
            return "http://13.126.16.169/mobycy/";
        }
        else {
            return "http://13.126.16.169/mobycy/";
        }
    }
    
    /**
    This method is used to get screen height
    Return:- Return Screen Height
    */
    static func getScreenHeight() -> CGFloat {
        let screenSize: CGRect = UIScreen.main.bounds
        return screenSize.height
    }
    
    /**
    This method is used to get screen width
    Return:- Return Screen Width
    */
    static func getScreenWidth() -> CGFloat {
        let screenSize: CGRect = UIScreen.main.bounds
        return screenSize.width
    }
    
    /**
     This method is used to add shadow
     parameters : UIVeiw, radius, opacity value and color value
     targetView : target view on which we want to add shadow
     radius : define the radius on the target view
     */
    static func addShadow(targetView:UIView, radius:CGFloat, opacityValue: CGFloat, colorValue : UIColor){
        targetView.layer.shadowColor = colorValue.cgColor
        targetView.layer.shadowOffset = CGSize.zero
        targetView.layer.masksToBounds = false
        targetView.layer.shadowRadius = radius
        targetView.layer.shadowOpacity = Float(opacityValue)
    }
    
    /**
     This method is used to add shadow
     parameters : UIView, shadow offsetValue (Height and Width and opacity value)
     targetView : target view on which we want to add shadow
     radius : define the radius on the target view
     */
    static func addShadowWithRadius(targetView:UIView, widthValue: CGFloat, HeightValue: CGFloat, shadowOpacity: Float, radiusValue: CGFloat, colorName : UIColor){
        targetView.layer.shadowColor = colorName.cgColor
        targetView.layer.shadowOffset = CGSize(width: widthValue, height: HeightValue)
        targetView.layer.masksToBounds = false
        targetView.layer.shadowRadius = radiusValue
        targetView.layer.shadowOpacity = shadowOpacity
    }
    
    /*
     This methood is used to add status bar color
     */
    static func setStatusBarColor(currentView : UIView) {
        if #available(iOS 13.0, *) {
            let app = UIApplication.shared
            let statusBarHeight: CGFloat = app.statusBarFrame.size.height

            let statusbarView = UIView()
            statusbarView.backgroundColor = customColor.NEW_THEME_GREEN_COLOR
            currentView.addSubview(statusbarView)

            statusbarView.translatesAutoresizingMaskIntoConstraints = false
            statusbarView.heightAnchor
                .constraint(equalToConstant: statusBarHeight).isActive = true
            statusbarView.widthAnchor
                .constraint(equalTo: currentView.widthAnchor, multiplier: 1.0).isActive = true
            statusbarView.topAnchor
                .constraint(equalTo: currentView.topAnchor).isActive = true
            statusbarView.centerXAnchor
                .constraint(equalTo: currentView.centerXAnchor).isActive = true
       
        } else {
            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
            statusBar?.backgroundColor = customColor.NEW_THEME_GREEN_COLOR
        }
    }
    
    /**
     This method used to show alert message
     Parameters :-  title for alert message
     message to show alert message
     */
    static func showAlertView(_ message:String,title:String){
        DispatchQueue.main.async { () -> Void in
            let finalString2 = message.separateString(every:35, with: " \n ")
            let alert:UIAlertController = UIAlertController.init(title: title, message: finalString2, preferredStyle:.alert)
            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(defaultAction)
            UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
        }
    }
    
    /**
     This method used for showing alert with performing action
     - Parameters:
     - message: This method take the alert message in string format
     - title: This method take the alert message in string format
     - onView: This method take the viewcontroller for ditermining where we have to show alert
     */
    
    static func showAlertControllerAction(message:String,title:String, styleType: String, onView controller:UIViewController, andFirstButtonTitle firstButtonTitle:String,andSecondButtonTitle secondButtonTitle:String, andFirstAction firstAction:Selector!, andSecondAction secondAction:Selector!){
        
        DispatchQueue.main.async() { () -> Void in
            var alert = UIAlertController()
            
            if styleType == "sheet" {
                alert = UIAlertController.init(title: title, message: message, preferredStyle: UIAlertController.Style.actionSheet)
            }
            else {
                alert = UIAlertController.init(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
            }
//            let alert:UIAlertController = UIAlertController.init(title: title, message: message, preferredStyle: UIAlertController.Style.actionSheet)
            
            let defaultAction = UIAlertAction(title:firstButtonTitle, style: .default, handler:{(alert:UIAlertAction) -> Void in
                if firstAction != nil{
                    controller.perform(firstAction)
                }
            })
            
            let yesAction = UIAlertAction(title:secondButtonTitle, style: .default, handler: {(alert:UIAlertAction) -> Void in
                if secondAction != nil{
                    controller.perform(secondAction)
                }
            })
            
            alert.addAction(defaultAction)
            alert.addAction(yesAction)
            controller.present(alert, animated: true, completion: nil)
        }
    }
    
    /**
     This method check that give string message is in numeric value or not
     - Parameters:
     - value: This method take the value as string format
     - returns: true or false
     */
    
    static func onlyNumeric(value: String) -> Bool {
        let PHONE_REGEX = "^[0-9]*$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        return result
    }
    
    /**
     This method check that give string message is vaild email id or not
     - Parameters:
     - value: This method take the value as string format
     - returns: true or false
     */
    class  func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    
        let result = emailTest.evaluate(with: testStr)
        return result
    }
    
    /*
     This function is used to check response data is null or not
     */
    static func isEmptyValue(_ object: Any?) -> Bool {
        if (object is NSNull) || (object as? NSNull) == NSNull() || object == nil {
            return true
        }
        return false
    }
    
    /*
     This method is used to show price and final prices
     - Parameters :
     - value : This method takes final price and actual price
     - returns : return the attribute string
     **/
    class func setAttributeString(preDiscountPrice: String?, finalPrice: String?) -> NSAttributedString? {

        let discountedTitleAttr = NSMutableAttributedString(string: preDiscountPrice ?? "")
        discountedTitleAttr.addAttribute(.font, value: UIFont(name: TextFontStyle.MONTSERRAT_SEMIBOLD, size: CGFloat(FontSize.HEADINGFONT18)) as Any, range: NSRange(location: 0, length: preDiscountPrice?.count ?? 0))
        discountedTitleAttr.addAttribute(.foregroundColor, value: UIColor.black, range: NSRange(location: 0, length: preDiscountPrice?.count ?? 0))
//        discountedTitleAttr.addAttribute(
//            .strikethroughStyle,
//            value: NSNumber(value: 2),
//            range: NSRange(location: 0, length: preDiscountPrice?.count ?? 0))

        let priceAttr = NSMutableAttributedString(string: finalPrice ?? "")
        priceAttr.addAttribute(.font, value: UIFont(name: TextFontStyle.MONTSERRAT_SEMIBOLD, size: CGFloat(FontSize.HEADINGFONT14)) as Any, range: NSRange(location: 0, length: finalPrice?.count ?? 0))
        priceAttr.addAttribute(.foregroundColor, value: UIColor.black, range: NSRange(location: 0, length: finalPrice?.count ?? 0))
        
        discountedTitleAttr.append(priceAttr)
        return discountedTitleAttr
    }
    
    /*
     This function is used to check the camera permission
     */
    static func checkCameraPermissions(withDelegte delegate: Any?) -> Bool {
        let mediaType = AVMediaType.video
        let authStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType(mediaType.rawValue))
        
        switch authStatus {
        case .authorized:
            return true
        case .denied, .restricted, .notDetermined :
            return false
        default:
            return false
        }
//        else if authStatus == .notDetermined {
//            // not determined?!
//            AVCaptureDevice.requestAccess(for: AVMediaType(mediaType.rawValue), completionHandler: { granted in
//                if granted {
//                    //nslog(@"Granted access to %@", mediaType);
//                } else {
//                    //nslog(@"Not granted access to %@", mediaType);
//                }
//            })
//            return false
//        }
    }
    
    /*
     This function is used to check the location permission
     */
    static func checkLocationPermissions() -> Bool {
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
                case .notDetermined, .restricted, .denied:
                    return false
            case .authorizedAlways, .authorizedWhenInUse:
                return true
            @unknown default:
                return false
            }
        }
        else {
            return false
        }
    }
    
    /*
     TODO :- Get route array between two loaction
     Parameters : Start Point Locations and End Point Location
     Return : Return route array
     */
    static func getPolylineRoute(startPoint_Lat: CLLocationDegrees, startPoint_Long: CLLocationDegrees, endPoint_Lat: CLLocationDegrees, endPoint_Long: CLLocationDegrees, callbackSuccess:@escaping (_ newRouteArray:NSArray) -> Void){
        
        var newRouteArray = NSArray()
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let url = URL(string: "https://maps.googleapis.com/maps/api/directions/json?origin=\(startPoint_Lat),\(startPoint_Long)&destination=\(endPoint_Lat),\(endPoint_Long)&mode=Bicycling&key=\(GOOGLE_DIRECTION_API_KEY)")!
        
        let task = session.dataTask(with: url, completionHandler: {
            (data, response, error) in
            if error != nil {
                print(error!.localizedDescription)
                //self.activityIndicator.stopAnimating()
            }
            else {
                do {
                    if let json : [String:Any] = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any]{
                        
                        guard let routes = json["routes"] as? NSArray else {
                            DispatchQueue.main.async {
                                //self.activityIndicator.stopAnimating()
                            }
                            return
                        }
                        
                        if (routes.count > 0) {
                            newRouteArray = routes
                            callbackSuccess(newRouteArray)
                        }
                        else {
                            DispatchQueue.main.async {
                                callbackSuccess(newRouteArray)
                                //self.activityIndicator.stopAnimating()
                            }
                        }
                    }
                }
                catch {
                    print("error in JSONSerialization")
                    DispatchQueue.main.async {
                        //self.activityIndicator.stopAnimating()
                    }
                }
            }
        })
        task.resume()
    }
    
    /*
     TODO :- Get search address details
     Parameters : Enter search adress as string
     Return : Return search address list array
     */
    static func getSearchAddress_Data(searchString: String, callbackSuccess:@escaping (_ searchPredArray:NSArray) -> Void){
        
        var newRouteArray = NSArray()
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let urlString = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=\(searchString)&types=establishment|geocode&radius=500&language=en&key=\(GOOGLE_DIRECTION_API_KEY)"
        
        let completeUrlString = "\(urlString)".addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)

        let url = URL(string: completeUrlString!)

        let task = session.dataTask(with: url!, completionHandler: {
            (data, response, error) in
            if error != nil {
                print(error!.localizedDescription)
                //self.activityIndicator.stopAnimating()
            }
            else {
                do {
                    if let json : [String:Any] = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any]{

                        guard let routes = json["predictions"] as? NSArray else {
                            DispatchQueue.main.async {
                            }
                            return
                        }

                        if (routes.count > 0) {
                            newRouteArray = routes
                            callbackSuccess(newRouteArray)
                        }
                        else {
                            DispatchQueue.main.async {
                                callbackSuccess(newRouteArray)
                            }
                        }
                    }
                }
                catch {
                    print("error in JSONSerialization")
                    DispatchQueue.main.async {
                    }
                }
            }
        })
        task.resume()
    }
    
    /*
     TODO :- Get Lat and Long Value by Google PLace ID
     Parameters : Google Place ID
     Return : Return Location lat and long value
     */
    static func getLatLongByPlace_Id(placeIDString: String, callbackSuccess:@escaping (_ locationDataDic:NSDictionary) -> Void){
        let emptyDic = NSDictionary()
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let urlString = "https://maps.googleapis.com/maps/api/place/details/json?placeid=\(placeIDString)&key=\(GOOGLE_DIRECTION_API_KEY)"
        
        let completeUrlString = "\(urlString)".addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)

        let url = URL(string: completeUrlString!)

        let task = session.dataTask(with: url!, completionHandler: {
            (data, response, error) in
            if error != nil {
                print(error!.localizedDescription)
                //self.activityIndicator.stopAnimating()
            }
            else {
                do {
                    if let json : [String:Any] = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any]{
                    
                        guard let resultDic = json["result"] as? NSDictionary else {
                            DispatchQueue.main.async {
                            }
                            return
                        }

                        if (resultDic.count > 0) {
                            callbackSuccess(resultDic)
                        }
                        else {
                            DispatchQueue.main.async {
                                callbackSuccess(emptyDic)
                            }
                        }
                    }
                }
                catch {
                    print("error in JSONSerialization")
                    DispatchQueue.main.async {
                    }
                }
            }
        })
        task.resume()
    }
}
