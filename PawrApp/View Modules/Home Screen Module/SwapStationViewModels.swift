//
//  SwapStationViewModels.swift
//  PawrApp
//
//  Created by Ayush Bansal on 24/03/21.
//

import Foundation
import UIKit

struct chargingStationModel: Codable {
    let address: String?
    let batteryCount: Int?
    let bc_id: Int?
    let cId: Int?
    let cityName: String?
    let latitude: String?
    let longitude: String?
    let phone: String?
    let placeName: String?
    let qrCode: String?
    let status: Int?
    let visible: Int?
    
    private enum CodingKeys: String, CodingKey {
        case address
        case batteryCount
        case bc_id
        case cId
        case cityName
        case latitude
        case longitude
        case phone
        case placeName
        case qrCode
        case status
        case visible
    }
}

//TODO :- Battery Swapping Models
struct newSwappingStationModel {
    var address: String?
    var batteryCount: Int?
    var bc_id: Int?
    var cId: Int?
    var cityName: String?
    var latitude: NSNumber?
    var longitude: NSNumber?
    var phone: String?
    var placeName: String?
    var qrCode: String?
    var status: Int?
    var visible: Int?
}

class batterySwap_Module : NSObject {
    
    static func BatterySwapListData(dataArray: NSMutableArray) -> [newSwappingStationModel] {
        
        var dataModel = [newSwappingStationModel]()
        for dataDic in dataArray {
            let newDic = dataDic as! NSDictionary
            
            let addressString = newDic["address"] as! String
            let battery_Count = newDic["batteryCount"] as! Int
//            let bc_Id = newDic["bc_id"] as! Int
            let c_Id = newDic["cId"] as! Int
            let city_Name = newDic["cityName"] as! String
            let latitude_Value = newDic["latitude"] as! NSNumber
            let longitude_Value = newDic["longitude"] as! NSNumber
            let phone_Value = newDic["phone"] as! String
            let placeName_Value = newDic["placeName"] as! String
            let qrCode_Value = newDic["qrCode"] as! String
            
            let status_Value = newDic["status"] as! Int
            let visible_Value = newDic["visible"] as! Int
            
            dataModel.append(newSwappingStationModel(
                address: addressString, batteryCount: battery_Count, cId: c_Id, cityName: city_Name, latitude:latitude_Value, longitude:longitude_Value, phone:phone_Value, placeName:placeName_Value, qrCode:qrCode_Value, status:status_Value, visible:visible_Value
            ))
        }
        
        return dataModel
    }
}

class stationListSingleton {
    static let shared = stationListSingleton()
    var stationListDetails = [newSwappingStationModel]()
}


//TODO :- Charging Sockets models
struct chargingStationListModel {
    var address: String?
    var batteryCount: Int?
    var cId: Int?
    var cityName: String?
    var latitude: NSNumber?
    var longitude: NSNumber?
    var phone: String?
    var placeName: String?
    var qrCode: String?
    var status: Int?
    var visible: Int?
}

class chargingStaion_Module : NSObject {
    
    static func chargingStationListData(dataArray: NSMutableArray) -> [chargingStationListModel] {
        
        var dataModel = [chargingStationListModel]()
        for dataDic in dataArray {
            let newDic = dataDic as! NSDictionary
            
            let addressString = newDic["address"] as! String
            let battery_Count = newDic["batteryCount"] as! Int
            let c_Id = newDic["cId"] as! Int
            let city_Name = newDic["cityName"] as! String
            let latitude_Value = newDic["latitude"] as! NSNumber
            let longitude_Value = newDic["longitude"] as! NSNumber
            let phone_Value = newDic["phone"] as! String
            let placeName_Value = newDic["placeName"] as! String
            let qrCode_Value = newDic["qrCode"] as! String
            
            let status_Value = newDic["status"] as! Int
            let visible_Value = newDic["visible"] as! Int
            
            dataModel.append(chargingStationListModel(
                address: addressString, batteryCount: battery_Count, cId: c_Id, cityName: city_Name, latitude:latitude_Value, longitude:longitude_Value, phone:phone_Value, placeName:placeName_Value, qrCode:qrCode_Value, status:status_Value, visible:visible_Value
            ))
        }
        
        return dataModel
    }
}

class chargingStationSingleton {
    static let shared = chargingStationSingleton()
    var chargingStationDetails = [chargingStationListModel]()
}
