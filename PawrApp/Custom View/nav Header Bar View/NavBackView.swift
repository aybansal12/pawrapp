//
//  NavBackView.swift
//  PawrApp
//
//  Created by Ayush Bansal on 27/03/21.
//
import UIKit

protocol navBakButtonWithPressed {
    func didPressButton(button:UIButton)
}

class NavBackView: UIView {

    @IBOutlet weak var mainContentView: UIView!
    @IBOutlet var navBackButton: UIButton!
    @IBOutlet var navTitleLabel: UILabel!
    
    var navBackBtnDelegates:navBakButtonWithPressed!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        mainContentView.backgroundColor = customColor.NEW_THEME_GREEN_COLOR
        
        navTitleLabel.textColor = UIColor.white
        navTitleLabel.font = UIFont(name: TextFontStyle.MONTSERRAT_SEMIBOLD, size: CGFloat(FontSize.HEADINGFONT18))
    }
    
    @IBAction func navBackButton(_ sender: UIButton) {
        navBackBtnDelegates.didPressButton(button: navBackButton)
    }

}
