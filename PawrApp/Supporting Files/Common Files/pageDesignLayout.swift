//
//  pageDesignLayout.swift
//  PawrApp
//
//  Created by Ayush Bansal on 31/03/21.
//

import Foundation
import UIKit

class pageDesign : NSObject {
    
    /*
     TODO :- Func to set text field, corner Radius, background colors, text color and text font style
     */
    static func textFieldLayout(to textfield: UITextField, textColor: UIColor, font_Size: Double, fontFamily: String) {
        textfield.textColor = textColor
        textfield.font = UIFont(name: fontFamily, size: CGFloat(font_Size))
    }
    
    /*
     TODO :- Func to set UIlabel text color and text font style
     */
    static func labelLayout(to labelValue: UILabel, textColor: UIColor, font_Size: Double, fontFamily: String) {
        labelValue.textColor = textColor
        labelValue.font = UIFont(name: fontFamily, size: CGFloat(font_Size))
    }
    
    /*
     TODO :- Func to set UIButton, corner Radius, background colors, text color and text font style
     */
    static func buttonLayout(to buttonValue: UIButton, textColor: UIColor, font_Size: Double, fontFamily: String, bgColor: UIColor) {
        buttonValue.backgroundColor = bgColor
        buttonValue.setTitleColor(textColor, for: .normal)
        buttonValue.titleLabel?.font = UIFont(name: fontFamily, size: CGFloat(font_Size))
    }
    
    /*
     TODO :- Func to set text field padding
     */
    static func textFieldPadding(to textfield: UITextField, paddingWidth: CGFloat, paddingHeight: CGFloat) {
        let leftView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: paddingWidth, height: paddingHeight))
        textfield.leftView = leftView
        textfield.leftViewMode = .always
    }
}
