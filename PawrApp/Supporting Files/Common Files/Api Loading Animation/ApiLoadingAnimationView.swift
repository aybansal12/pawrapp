//
//  ApiLoadingAnimationView.swift
//  PawrApp
//
//  Created by Ayush Bansal on 01/04/21.
//

import UIKit

class ApiLoadingAnimationView: UIView {

    @IBOutlet weak var activityLoader: UIActivityIndicatorView!
    
    var animationView : ApiLoadingAnimationView?
    
    func startLoadingView() {
        stopLoadingView()
        //animation View
        animationView = Bundle.main.loadNibNamed("ApiLoadingAnimationView", owner: self, options: nil)?.first as? ApiLoadingAnimationView
        animationView?.frame = UIScreen.main.bounds
        UIApplication.shared.keyWindow?.addSubview(animationView!)

        animationView!.tag = 6766;
        activityLoader.startAnimating()
    }
    
    func stopLoadingView() {
        
        let loadingView = UIApplication.shared.keyWindow!.viewWithTag(6766) as? ApiLoadingAnimationView
        loadingView?.removeFromSuperview()
        
        activityLoader.stopAnimating()
    }
}
