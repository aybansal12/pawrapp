//
//  SwiftConstant.swift
//  PawrApp
//
//  Created by Ayush Bansal on 12/03/21.
//

import Foundation
import UIKit

//Google map styling for hiding place from google maps
let kMapStyle = "[" +
    "  {" +
    "    \"featureType\": \"poi.business\"," +
    "    \"elementType\": \"all\"," +
    "    \"stylers\": [" +
    "      {" +
    "        \"visibility\": \"off\"" +
    "      }" +
    "    ]" +
    "  }," +
    "  {" +
    "    \"featureType\": \"transit\"," +
    "    \"elementType\": \"labels.icon\"," +
    "    \"stylers\": [" +
    "      {" +
    "        \"visibility\": \"off\"" +
    "      }" +
    "    ]" +
    "  }," +
    "  {" +
    "    \"featureType\": \"poi.temples\"," +
    "    \"elementType\": \"labels.icon\"," +
    "    \"stylers\": [" +
    "      {" +
    "        \"visibility\": \"off\"" +
    "      }" +
    "    ]" +
    "  }" +
"]"

//TODO :- Google place api key
let GOOGLE_PLACE_API_KEY = "AIzaSyBaxgSQieVnwQGjkyoLgheTpR26FO9KyrY"
let GOOGLE_DIRECTION_API_KEY = "AIzaSyB2v1qSbFUb1gEhRBzV7B1OQChdZykBtM0"

//TODO :- Constant Values
struct constantValue {
    static let ROUTECAMERA_ZOOM_VALUE = 13.0
}

//TODO :- Custom Message String
struct customMessages {
    static let SOMETHING_WENT_WRONG = "Something went wrong. Please try after some time"
    static let ENTER_MOBILE_NO = "Enter the mobile number."
    static let ENTER_VALID_MOBILE_NO = "Enter valid mobile number."
    static let ENTER_PASSWORD = "Enter the password"
    static let ENTER_VALID_PASSWORD = "Password should be of minimum 6 characters"
    static let Enter_OTP_VALUE = "Enter the Otp"
    static let LOGIN_ALERT = "Login!"
    static let LOGIN_FIRST_ALERT = "Please login first!"
    static let LOGOUT_ALERT = "Logout Alert!"
    static let LOGOUT_MESG = "Do you want to logout?"
    static let SELECT_YOUR_GENDER = "Select your gender"
    static let ENTER_YOUR_NAME = "Enter your name"
    static let ENTER_YOUR_EMAIL = "Enter your email id"
    static let ENTER_VALID_EMAIL_ID = "Enter valid email id"
    static let ALERT_TITLE = "\("PAWWRR APP") Would Like to Access the Camera"
    static let CAMERA_SETTING_MESSAGE = "Need to access Camera to click your photo"  //"Go to settings > Privacy Polocy > Camera > Enable"
    static let CAMERA_SETTING_MESSAGE_QRCODE = "Need to access Camera for scan the Battery/Battery Container information."
}

//TODO :- Battery Charging custom message
struct batteryChargingMesg {
    static let BLUETOOTH_ALERT = "Bluetooth Setting"
    static let OPEN_BLUETOOTH_FIRST = "Open your bluetooth to use charging points"
    static let CONNECT_BT_AGAIN = "Please connect your phone bluetooth again!"
    static let SELECT_CHARGING_PLAN = "Please select charging plan first!"
}

//TODO :- Battery Swaping custom messages
struct batterySwapMesg {
    static let SELECT_BATTERY_SWAP_STATION = "Select battery swap station first!"
    static let ENTER_OLD_BATTERY_QR_CODE = "Enter/Scan your old battery qr code"
    static let ENTER_BATTERY_CONTAINER_QR_CODE = "Enter/Scan battery container qr code "
    static let ENTER_NEW_BATTERY_QR_CODE  = "Enter/Scan your new battery qr code"
}

//TODO :- Payment custom messages
struct paymentCustomMesg {
    static let SELECT_PAYMENT_OPTION = "Select payment option first"
    static let DEFAULT_PAYMENT_MODE = "This is already your default payment mode."
    static let LINKED_PAYMENT_FIRST = "Before making default, please link this wallet."
}

//TODO :- Coupon Code Message String
struct COUPONCODE_MESG {
    static let ENTER_COUPON_CODE = "Enter coupon code"
    static let COUPON_CODE_NOT_VALID = "Coupon Code is not valid"
}


//TOOD :- App status code
struct constantStatusCode {
    static let NOT_REGISTERED_USER = 4401
    static let API_SUCCESS_CODE = 20000
}

struct chargingConstantCode {
    static let CHARGING_PAYMENT_DONE = 15007
    static let BLUETOOTH_CONNECT_FAILED = 2001
    static let BLUETOOTH_CHARGING_STARTED = 15005
}

//TODO :- Func to set Battery Swapping Code
struct batterySwap_ModuleCode {
    static let INVALID_BATTERY_QR_CODE = 15003
    static let INVALID_BATTERY_CONTAINER_QR_CODE = 15004
    
    static let SWAP_BATTERY_PAYMENT_DONE = 15001
}

//TODO :-  coupon ID type Code
struct couponIDType_Code {
    static let COUPON_BATTERY_SWAP = 5
}

//TODO :- Func to set payment type constant
struct paymentTypeConstant {
    static let PAYMENT_TYPE_PAYTM = "paytmWallet"
    static let PAYMENT_TPYE_MOBIKWIK_WALLET = "mobikwikWallet"
    
    
    static let OTP_SEND_FAIL = 5038
    static let LOW_WALLET_BALANCE = 20005
    static let PAYMENT_WALLET_NOT_ATTACHED = 20006
}
