//
//  AppFontStyle.swift
//  PawrApp
//
//  Created by Ayush Bansal on 09/03/21.
//

import Foundation

struct TextFontStyle {
    static let FONTLIGHT = "Roboto-Light"
    static let FONTREGULAR = "Roboto-Regular"
    static let FONTMEDIUM = "Roboto-Medium"
    static let FONTBOLD = "Roboto-Bold"
    
    static let MONTSERRAT_BOLDITALIC = "Montserrat-BoldItalic"
    static let MONTSERRAT_BOLD = "Montserrat-Bold"
    static let MONTSERRAT_SEMIBOLD = "Montserrat-SemiBold"
    static let MONTSERRAT_MEDIUMITALIC = "Montserrat-MediumItalic"
    static let MONTSERRAT_REGULAR = "Montserrat-Regular"
    static let MONTSERRAT_MEDIUM = "Montserrat-Medium"
    static let MONTSERRAT_LIGHT = "Montserrat-Light"
}

struct FontSize {
    static let HEADINGFONT9 = 9.0
    static let HEADINGFONT10 = 10.0
    static let HEADINGFONT11 = 11.0
    static let HEADINGFONT12 = 12.0
    static let HEADINGFONT13 = 13.0
    static let HEADINGFONT14 = 14.0
    static let HEADINGFONT15 = 15.0
    static let HEADINGFONT16 = 16.0
    static let HEADINGFONT17 = 17.0
    static let HEADINGFONT18 = 18.0
    static let HEADINGFONT19 = 19.0
    static let HEADINGFONT20 = 20.0
    static let HEADINGFONT22 = 22.0
    static let HEADINGFONT25 = 25.0
}
