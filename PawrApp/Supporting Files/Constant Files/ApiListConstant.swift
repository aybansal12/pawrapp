//
//  ApiListConstant.swift
//  PawrApp
//
//  Created by Ayush Bansal on 13/03/21.
//

import UIKit
import Foundation

let liveApi = 0 // 0 for staging and 1 for production and 2 for local servers

struct ApiList {
    static let CHECK_MOBILE_NUMBER = "check/mobile"
    static let USER_LOGIN = "secure/user/login"
    static let VERIFY_MOBILE_NUMBER = "user/mobile/verify"
    static let GENERATE_OTP = "user/generate/otp"
    static let VERIFY_ENTER_OTP = "user/mobile/verify"
    static let SET_USER_PASSWORD = "secure/user/password/set"
    static let SET_USER_FORGOT_PASSWORD = "user/forgot/password"
    
    //Load Pic by ID from server
    static let LOAD_PIC_FROM_SERVER = "media/file/"
    static let UPLOAD_MEDIA_FILE = "media/file/upload"
    static let UPLOAD_USER_PROFILE_PIC = "secure/user/profilepic/update"
    
    static let UPLOAD_USER_PRIMARY_KYC_DETAILS = "secure/v2/user/profile/update"
}

//TODO :- Charging Station Module Api's
struct Charging_ModuleApi {
    //Fetch Charging Station List
    static let FETCH_CHARGING_STATION_LIST = "insecure/getBluetoothChargingStation"
    
    static let BASE_CHARGING_AMOUNT = "secure/getBaseChargingAmount"
    static let CHARGING_PAYMENT_SUCCESS = "secure/purchase/bluetoothChargerPay"
    static let GET_BLUETOOTH_AVAILABILITY = "secure/getBtAvailability"
    
    static let ON_BLUETOOTH_API = "secure/bluetoothChargerRequest"
    static let OFF_BLUETOOTH_API = "secure/bluetoothChargerRequest"
}

//TODO :- Battery Swap Module Api's
struct BatterySwap_ModuleApi {
    //Fetch Battery Swap Station List
    static let FETCH_BATTERY_SWAP_STATION = "insecure/newstations"
    static let BATTERY_SWAP_AMOUNT = "secure/batterySwapAmount"
    static let PURCHASE_PAY_BATTERY = "secure/purchase/batterypay"
    static let SWAPPING_MODULE_BATTERY_SWAP = "secure/batterySwap"
    static let SWAPPING_MODULE_BATTERY_SWAP_COMPLETE = "secure/swapComplete"
    static let PREVIOUS_BATTERY_SWAP_HISTORY = "secure/swapTransactions"
    static let CHECK_BATTERY_CONTAINER_QR = "insecure/validateDeviceQr"
    static let GET_SWAP_AMOUNT_DETAILS = "insecure/getSwapAmount"
}

//TODO :- Payment Api's
struct PaymentApi {
    static let FETCH_PAYMENT_MODE = "secure/battery/v4/payment/modes"
    static let GENERATE_WALLET_OTP_URL = "secure/user/pay/wallet/send/otp"
    static let VALIDATE_WALLET_OTP_URL = "secure/user/pay/wallet/validate/otp"
    static let SWITCH_PAYMENT_MODE = "secure/switch/payment/mode"
    static let DELINK_PAYMENT_MODE = "secure/delink/payment/mode"
    
    static let PAYTM_WALLET_ADD_MONEY = "paywallet/add/money/"
    static let PAYTM_ADD_MONEY_SUCCESS = "paywallet/add/money/success"
    static let PAYTM_BROWSER_FAILURE_URL = "paywallet/add/money/failure"
}

//TODO :- Zypp Coin Api
struct ZYPP_COIN_API {
    //Fetch User MobiCoin Balance
    static let FETCH_USER_MOBICOIN_BALANCE = "secure/user/get/mobycy/wallet/balance"
    static let FETCH_ZYPP_COIN_PACKAGES = "secure/get/mobycoins/packages"
    
    static let PURCHASE_ZYPP_COIN = "secure/purchase/mobycoins"
}

//TODO :- Coupon Code Api
struct COUPON_CODE_API {
    static let COUPON_CODE_LIST = "secure/coupons"
    static let APPLY_COUPON_CODE_API = "secure/coupon/apply"
    static let CHECK_COUPON_CODE = "secure/check"
}
