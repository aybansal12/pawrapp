//
//  Globals.h
//  PawrApp
//
//  Created by Ayush Bansal on 19/04/21.
//

#ifndef Globals_h
#define Globals_h

#define START_LOADING [ApiLoadingAnimationView startLoadingView]
#define END_LOADING [ApiLoadingAnimationView stopLoadingView]

#endif /* Globals_h */
