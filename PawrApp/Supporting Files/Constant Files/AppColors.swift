//
//  AppColors.swift
//  PawrApp
//
//  Created by Ayush Bansal on 09/03/21.
//

import UIKit
import Foundation

struct customColor {

    //Theme Main Color
    static let NEW_THEME_GREEN_COLOR = UIColor(red: 0/255.0, green: 203/255.0, blue: 96/255.0, alpha: 1.0)
    static let NEW_THEME_GREEN_COLOR_WITH_ALPHA = UIColor(red: 0/255.0, green: 203/255.0, blue: 96/255.0, alpha: 0.6)
    
    //text Dark & Light Gray Color
    static let TEXT_DARK_GRAY_COLOR = UIColor(red: 80/255.0, green: 80/255.0, blue: 80/255.0, alpha: 1.0)
    static let TEXT_LIGHT_GRAY_COLOR = UIColor(red: 100/255.0, green: 100/255.0, blue: 100/255.0, alpha: 1.0)
    
    //Backgroung gray color
    static let BG_GRAY_COLOR = UIColor(red: 230/255.0, green: 230/255.0, blue: 230/255.0, alpha: 1.0)
    static let LIGHT_BG_GRAY_COLOR = UIColor(red: 245/255.0, green: 245/255.0, blue: 245/255.0, alpha: 1.0)
    
    //Light Black Color
    static let BLACK_COLOR_WITH_ALPHA = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.6)
    static let LIGHT_BLACK_COLOR = UIColor(red: 40/255.0, green: 40/255.0, blue: 40/255.0, alpha: 1.0)
    
    //Battery Swapping Map PolyLine
    static let SWAPPING_MAP_POLYLINE_COLOR = UIColor(red: 58/255.0, green: 74/255.0, blue: 244/255.0, alpha: 1.0)
    
    //Red Color Message
    static let RED_MESG_COLOR = UIColor(red: 219/255.0, green: 10/255.0, blue: 10/255.0, alpha: 1.0)
    
}

