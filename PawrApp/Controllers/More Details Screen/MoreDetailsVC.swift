//
//  MoreDetailsVC.swift
//  PawrApp
//
//  Created by Ayush Bansal on 28/04/21.
//

import UIKit

@available(iOS 13.0, *)
class MoreDetailsVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var UserDetailsView: UIView!
    @IBOutlet weak var settingLbl: UILabel!
    @IBOutlet weak var userProfileImageView: UIImageView!
    @IBOutlet weak var helloLbl: UILabel!
    @IBOutlet weak var userNameLbl: UILabel!
    
    @IBOutlet weak var roundedView: UIView!
    @IBOutlet weak var menuTableView: UITableView!
    
    @IBOutlet weak var appVersionLbl: UILabel!
    
    let defaults = UserDefaults.standard
    var userLoginData = NSDictionary()
    var menuListArray = ["Payment List", "Swap History", "Support", "Logout"]
    
    var userLogin = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (defaults.object(forKey:UserDefaults.UserDefaultsKeys.userLoginData.rawValue) != nil) {
            self.userLoginData = defaults.getUserLoginData() as NSDictionary
        }
        
        Utilities.setStatusBarColor(currentView: self.view)
        
        menuTableView.dataSource = self
        menuTableView.delegate = self
        menuTableView.separatorStyle = .none

        let blutoothList = UINib(nibName : "BluetoothListTableViewCell" , bundle:nil)
        menuTableView.register(blutoothList, forCellReuseIdentifier: "BluetoothListTableCell")
        
        pageDesignLayout()
        setPageData()
    }
    
    //TODO :- Func to set page design layout
    func pageDesignLayout() {
        UserDetailsView.backgroundColor = customColor.NEW_THEME_GREEN_COLOR
        
        settingLbl.text = "Settings"
        pageDesign.labelLayout(to: settingLbl, textColor: UIColor.black, font_Size: FontSize.HEADINGFONT22, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
        
        userProfileImageView.layer.cornerRadius = userProfileImageView.frame.height/2
        userProfileImageView.layer.masksToBounds = true
        
        helloLbl.text = "Hello"
        pageDesign.labelLayout(to: helloLbl, textColor: customColor.LIGHT_BLACK_COLOR, font_Size: FontSize.HEADINGFONT16, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
        
        pageDesign.labelLayout(to: userNameLbl, textColor: customColor.LIGHT_BLACK_COLOR, font_Size: FontSize.HEADINGFONT18, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
        
        roundedView.layer.cornerRadius = 35.0
        roundedView.layer.masksToBounds = true
        
        pageDesign.labelLayout(to: appVersionLbl, textColor: customColor.TEXT_DARK_GRAY_COLOR, font_Size: FontSize.HEADINGFONT14, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
    }
    
    //TODO :- Func to set page data
    func setPageData() {
        if (defaults.object(forKey:UserDefaults.UserDefaultsKeys.isLoggedIn.rawValue) != nil) {
            userLogin = defaults.getUserLogIn()
        }
        else {
            userLogin = "false"
        }
        if (userLogin == "true" || userLogin == "True") {
            if userLoginData.count > 0 {
                let user_Name = userLoginData["name"] as! String
                userNameLbl.text = user_Name
            }
            else {
                userNameLbl.text = "Guest"
            }
        }
        else {
            menuListArray.removeAll()
            menuListArray = ["Support"]
            userNameLbl.text = "Guest"
        }
        
        let nsObject: AnyObject? = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as AnyObject
        let version = nsObject as! String
        appVersionLbl.text = "v " + version
    }
    
    //TODO :- Func to set Table view Data source and delegates
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if menuListArray.count > 0 {
            return menuListArray.count
        }
        else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = menuTableView.dequeueReusableCell(withIdentifier: "BluetoothListTableCell", for: indexPath as IndexPath) as! BluetoothListTableViewCell
        cell.selectionStyle = .none
        
        let indexString = menuListArray[indexPath.row]
        cell.setCellData(textString: indexString)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.row {
        case 0:
            if (userLogin == "true" || userLogin == "True") {
                let paymentVC = PaymentModeVC()
                paymentVC.selectedModule = "MoreMenu"
                self.navigationController?.pushViewController(paymentVC, animated: true)
            }
            else {
                print("Support Details")
            }
            
        case 1:
            let swapSwapDetailsVC = PreviousSwapListVC()
            self.navigationController?.pushViewController(swapSwapDetailsVC, animated: true)
        
        case 2:
            print("Support Details")
            
        default:
            Utilities.showAlertControllerAction(message: customMessages.LOGOUT_MESG, title: customMessages.LOGOUT_ALERT, styleType: "alert", onView: self, andFirstButtonTitle: "Cancel", andSecondButtonTitle:"OK", andFirstAction: nil, andSecondAction: #selector(self.logoutButtonAction))
        }
    }
    
    // TODO :- Func to logout btn action
    @objc func logoutButtonAction(){
        self.defaults.removeObject(forKey:UserDefaults.UserDefaultsKeys.isLoggedIn.rawValue)
        self.defaults.removeObject(forKey:UserDefaults.UserDefaultsKeys.userLoginData.rawValue)
        
        self.defaults.setUserLogIn(value: "false")
        
        let loginVC = LoginScreenVC()
        self.navigationController?.pushViewController(loginVC, animated: true)
    }
    
}
