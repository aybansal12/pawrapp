//
//  ChargingStationVC.swift
//  PawrApp
//
//  Created by Ayush Bansal on 26/03/21.
//

import UIKit

@available(iOS 13.0, *)
class ChargingStationVC: UIViewController {

    @IBOutlet weak var dataContainerView: UIView!
    @IBOutlet weak var startBtn: UIButton!
    
    var userCurrentLatitude : Double = 0
    var userCurrentLongitude : Double = 0
    var chargingListModel = [chargingStationListModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        userCurrentLatitude = UtilitiesValues.getUserCurrentLocation().latitude
//        userCurrentLongitude = UtilitiesValues.getUserCurrentLocation().longitude
        Utilities.setStatusBarColor(currentView: self.view)
        
        getChargingStation()
        pageDesginLayout()
    }
    
    //TODO :- Func to set page design layout
    func pageDesginLayout() {
        startBtn.layer.cornerRadius = 5.0
        startBtn.layer.masksToBounds = true
        
        startBtn.backgroundColor = customColor.NEW_THEME_GREEN_COLOR
        startBtn.setTitle("Start Connecting", for: .normal)
        startBtn.setTitleColor(UIColor.white, for: .normal)
        startBtn.titleLabel?.font = UIFont(name: TextFontStyle.MONTSERRAT_SEMIBOLD, size: CGFloat(FontSize.HEADINGFONT14))
    }

    func addViewController(_ childController: UIViewController) {
        childController.view.frame = self.dataContainerView.bounds;
        self.addChild(childController)
        self.dataContainerView.addSubview(childController.view)
        childController.didMove(toParent: self)
    }
    
    //TODO :- Func to fetch all charging stations
    func getChargingStation() {
        let Base_URL = Utilities.returnBaseURL(liveURL: liveApi)
        let apiURl = Base_URL + Charging_ModuleApi.FETCH_CHARGING_STATION_LIST + "?latitude=\(self.userCurrentLatitude)&longitude=\(self.userCurrentLongitude)"

        ServiceManager.sharedInstance.callApi(url:apiURl, param_Values: nil, requestMethod: "get", callbackSuccess: { (responseData, apiStatusCode) -> Void in

            if apiStatusCode == 200 {
                if !Utilities.isEmptyValue(responseData) {
                    
                    let chargingListArrayData = (responseData as! NSArray).mutableCopy() as! NSMutableArray
                    self.chargingListModel.removeAll()
                    self.chargingListModel = chargingStaion_Module.chargingStationListData(dataArray: chargingListArrayData)
                    chargingStationSingleton.shared.chargingStationDetails = self.chargingListModel
                    
                    self.loadGoogleMap()
                }
                else {
                    self.loadGoogleMap()
                }
            }
            else if apiStatusCode == 401 {
                self.loadGoogleMap()
            }
           
        },callbackFaliure: { (error:String, message:String) -> Void in
            self.loadGoogleMap()
            self.showToast(message: message, duration: 2.0)
        })
    }
    
    //TODO :- Func to load google map view
    func loadGoogleMap() {
        let vcObj = GoogleMapVC()
        vcObj.stationType = 1
        addViewController(vcObj)
    }
    
    //TODO :- Func to set start btn action to connect with bluetooth
    @IBAction func startBtnAction(_ sender: Any) {
        let BluetoothVC = BluetoothViewController()
        self.navigationController?.pushViewController(BluetoothVC, animated: true)
    }
}
