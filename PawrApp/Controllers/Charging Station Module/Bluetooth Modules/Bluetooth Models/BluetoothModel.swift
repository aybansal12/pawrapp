//
//  BluetoothModel.swift
//  PawrApp
//
//  Created by Ayush Bansal on 17/03/21.
//

import UIKit
import CoreBluetooth

class ParticlePeripheral: NSObject {

        /// MARK: - Particle LED services and charcteristics Identifiers

//    public static let particleLEDServiceUUID     = CBUUID.init(string: "b4250400-fb4b-4746-b2b0-93f0e61122c6")
    public static let particleLEDServiceUUID     = CBUUID.init(string: "F8FC6568-6A0C-7782-082E-6125ADBF073E")
    public static let redLEDCharacteristicUUID   = CBUUID.init(string: "b4250401-fb4b-4746-b2b0-93f0e61122c6")
    public static let greenLEDCharacteristicUUID = CBUUID.init(string: "b4250402-fb4b-4746-b2b0-93f0e61122c6")
    public static let blueLEDCharacteristicUUID  = CBUUID.init(string: "b4250403-fb4b-4746-b2b0-93f0e61122c6")
    
    
    //CHARACTERISTIC_UUID //BEB5483E-36E1-4688-B7F5-EA07361B26A8
}
