//
//  BluetoothViewController.swift
//  PawrApp
//
//  Created by Ayush Bansal on 17/03/21.
//

import UIKit
import CoreBluetooth

class BluetoothViewController: UIViewController, CBPeripheralDelegate, CBCentralManagerDelegate, UITableViewDataSource, UITableViewDelegate, navBakButtonWithPressed, paymentSuccessProtocol {
    
    @IBOutlet weak var navHeaderBarView: UIView!
    @IBOutlet weak var bluetoothListTableView: UITableView!
    @IBOutlet weak var scanBluetoothBtn: UIButton!
    @IBOutlet weak var bluetoothMesgLbl: UILabel!
    
    @IBOutlet weak var bluetoothListView: UIView!
    
    @IBOutlet weak var BTStatusContainView: UIView!
    @IBOutlet var bluetoothStartOffView: UIView!
    
    @IBOutlet weak var btStatusMesgLbl: UILabel!
    @IBOutlet weak var btStartOffBtn: UIButton!
    
    var navBarView : NavBackView?
    var animationView : ApiLoadingAnimationView?
    
    // Properties
    private var centralManager: CBCentralManager!
    private var myPeripheal: CBPeripheral!
    var myCharacteristic:CBCharacteristic?
    var peripheralsArray : [CBPeripheral] = []
    
    var bluetoothStatus = 0 //0 for not connected, 1 for started command status, 2 for end command
    var paymentSuccess = 0 //0 payment is not completed 1 payment is done
    
    var peripheralsTestArray = ["Bluetooth 1", "Bluetooth 2", "Bluetooth 3"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navBarView = Bundle.main.loadNibNamed("NavBackView", owner: self, options: nil)?.first as? NavBackView
        navBarView?.navBackBtnDelegates = self
        navBarView?.navTitleLabel.text = "Bluetooth"
        navBarView?.navBackButton.setImage(UIImage(named: "whiteBack"), for: .normal)
        navBarView?.mainContentView.frame = navHeaderBarView.frame
        
        self.navHeaderBarView.addSubview(navBarView!)
        self.navHeaderBarView.addSubview(scanBluetoothBtn)
        
        Utilities.setStatusBarColor(currentView: self.view)
        //animation View
        animationView = Bundle.main.loadNibNamed("ApiLoadingAnimationView", owner: self, options: nil)?.first as? ApiLoadingAnimationView
        
        centralManager = CBCentralManager(delegate: self, queue: nil)
        centralManager.scanForPeripherals(withServices: nil)
        
        bluetoothListTableView.dataSource = self
        bluetoothListTableView.delegate = self
        bluetoothListTableView.separatorStyle = .none

        let blutoothList = UINib(nibName : "BluetoothListTableViewCell" , bundle:nil)
        bluetoothListTableView.register(blutoothList, forCellReuseIdentifier: "BluetoothListTableCell")
        
        scanBluetoothBtn.isHidden = false
        BTStatusContainView.isHidden = true
        
        //Extra code need to remove that code
        bluetoothListTableView.isHidden = true
        
        BTStatusContainView.addSubview(bluetoothStartOffView)
        pageDesignLayout()
    }
    
    //TODO :- Did Press Back Button
    func didPressButton(button: UIButton) {
        navigationController?.popViewController(animated: true)
        dismiss(animated: false, completion: nil)
    }

    //TODO :- Func to set page design layout
    func pageDesignLayout() {
        scanBluetoothBtn.setTitle("Scan BT", for: .normal)
        scanBluetoothBtn.setTitleColor(UIColor.white, for: .normal)
        scanBluetoothBtn.titleLabel?.font = UIFont(name: TextFontStyle.MONTSERRAT_SEMIBOLD, size: CGFloat(FontSize.HEADINGFONT14))
        
        bluetoothMesgLbl.text = "Select Bluetooth Name. If you now found Bluetooth Name then click on scan Bt button."
        bluetoothMesgLbl.textColor = customColor.LIGHT_BLACK_COLOR
        bluetoothMesgLbl.font = UIFont(name: TextFontStyle.MONTSERRAT_SEMIBOLD, size: CGFloat(FontSize.HEADINGFONT14))
        
        //Bluettooth Start and Off screen
        btStatusMesgLbl.textColor = customColor.LIGHT_BLACK_COLOR
        btStatusMesgLbl.font = UIFont(name: TextFontStyle.MONTSERRAT_SEMIBOLD, size: CGFloat(FontSize.HEADINGFONT14))
        
        btStartOffBtn.layer.cornerRadius = 10.0
        btStartOffBtn.layer.masksToBounds = true
        btStartOffBtn.backgroundColor = customColor.NEW_THEME_GREEN_COLOR
        btStartOffBtn.setTitleColor(UIColor.white, for: .normal)
        btStartOffBtn.titleLabel?.font = UIFont(name: TextFontStyle.MONTSERRAT_SEMIBOLD, size: CGFloat(FontSize.HEADINGFONT14))
    }
    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        print("Central state update")
        if central.state != .poweredOn {
            peripheralsArray.removeAll()
            self.myPeripheal = nil
            self.BTStatusContainView.isHidden = true
            
            Utilities.showAlertControllerAction(message: batteryChargingMesg.OPEN_BLUETOOTH_FIRST, title: batteryChargingMesg.BLUETOOTH_ALERT, styleType: "alert", onView: self, andFirstButtonTitle: "Cancel", andSecondButtonTitle:"OK", andFirstAction: #selector(self.notAllowBluetoothPer), andSecondAction: #selector(self.allowLocationPermission))
        } else {
            self.scanBluetoothBtn.isHidden = false
//            print("Central scanning for", ParticlePeripheral.particleLEDServiceUUID);
//            centralManager.scanForPeripherals(withServices: [ParticlePeripheral.particleLEDServiceUUID],
//                                              options: [CBCentralManagerScanOptionAllowDuplicatesKey : true])
           // print("Central scanning for", self.peripheral as Any)
            
            //centralManager.scanForPeripherals(withServices: nil)
        }
    }
    
    // TODO :- function to allow location permission
    @objc func allowLocationPermission(){
//        let url = URL(string: "App-Prefs:root=Bluetooth") //for bluetooth setting
//        let app = UIApplication.shared
//        app.canOpenURL(url!)
//        guard let settingsUrl = URL(string: "App-Prefs:root=Bluetooth") else {
//            return
//        }
//        if UIApplication.shared.canOpenURL(settingsUrl) {
//            UIApplication.shared.open(settingsUrl, completionHandler: { (success) in })
//        }
        
        print("Hide for some time")
    }
    
    //TODO :- Func to redirect on previous screen if user not allow bluetooth on permission
    @objc func notAllowBluetoothPer() {
        self.navigationController?.popViewController(animated: true)
    }
    
    // Handles the result of the scan
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {

        print(peripheral)
        if (peripheral.name != "" && peripheral.name != nil) {
            peripheralsArray.append(peripheral)
        }
        
        // Copy the peripheral instance
        //self.myPeripheal = peripheral
        //self.myPeripheal.delegate = self

        // Connect!
        //self.centralManager.connect(self.peripheral, options: nil)
        
        //stop scanning after 3 seconds
        DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
            
            self.animationView?.stopLoadingView()
            self.animationView?.removeFromSuperview()
            
            // We've found it so stop scan
            self.centralManager.stopScan()
            self.bluetoothListTableView.reloadData()
            self.scanBluetoothBtn.isHidden = false
            //self.stopScanForBLEDevices()
        }
    }
    
    // The handler if we do connect succesfully
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
//        if peripheral == self.peripheral {
//            print("Connected to your Particle Board")
//            peripheral.discoverServices([ParticlePeripheral.particleLEDServiceUUID])
//        }
        print(peripheral)
        self.myPeripheal = peripheral
        self.myPeripheal.delegate = self
        self.myPeripheal.discoverServices(nil)
        
        let BTIdentifier = myPeripheal.identifier
        let BTName = myPeripheal.name
        self.checkBTStationStatus(BT_Identifier: BTIdentifier, BT_Name: BTName!)
        //self.myPeripheal.discoverCharacteristics([ParticlePeripheral.particleLEDServiceUUID], for: CBService)
//        if let services = peripheral.services {
//            for service in services {
//                print(service.uuid)
//                peripheral.discoverCharacteristics(nil, for: service)
//            }
//        }
    }
    
    //centralManager?.cancelPeripheralConnection(myPeripheal!)
    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        print("Disconnected from " +  peripheral.name!)
        
        myPeripheal = nil
        myCharacteristic = nil
    }
    
    // Handles discovery event
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        
        guard let services = peripheral.services else { return }
        
        for service in services {
            peripheral.discoverCharacteristics(nil, for: service)
        }
        
//        if let services = peripheral.services {
//            for service in services {
//                print(service.uuid)
//                if service.uuid == ParticlePeripheral.particleLEDServiceUUID {
//                    print("LED service found")
//                    //Now kick off discovery of characteristics
//                    peripheral.discoverCharacteristics([ParticlePeripheral.redLEDCharacteristicUUID, ParticlePeripheral.greenLEDCharacteristicUUID, ParticlePeripheral.blueLEDCharacteristicUUID], for: service)
//                            return
//                }
//            }
//        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        guard let characteristics = service.characteristics else { return }
        myCharacteristic = characteristics[0]
    }
    
    //TODO :- Func to set bluetooth list on tableview
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        if peripheralsArray.count > 0 {
//            return peripheralsArray.count
//        }
//        else {
//            return 0
//        }
        if peripheralsTestArray.count > 0 {
            return peripheralsTestArray.count
        }
        else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = bluetoothListTableView.dequeueReusableCell(withIdentifier: "BluetoothListTableCell", for: indexPath as IndexPath) as! BluetoothListTableViewCell
        cell.selectionStyle = .none
        
//        let pheriphalData = peripheralsArray[indexPath.row]
//        cell.bluetoothNameLbl.text = pheriphalData.name!
        
        let pheriphalData = peripheralsTestArray[indexPath.row]
        cell.bluetoothNameLbl.text = pheriphalData
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let pheriphalData = peripheralsArray[indexPath.row]
//        self.checkBtAvailability(pheriphal_Data: pheriphalData)

        let BTIdentifier = UUID.init(uuidString: "F8FC6568-6A0C-7782-082E-6125ADBF073E")
        self.checkBtAvailabilityTest(BT_Identifier: BTIdentifier!)
    }
    
    //TODO :- Func to check bluetooth availability    //pheriphal_Data: CBPeripheral
//    func checkBtAvailability(pheriphal_Data: CBPeripheral) {
//        let Base_URL = Utilities.returnBaseURL(liveURL: liveApi)
//        let apiURl = Base_URL + Charging_ModuleApi.GET_BLUETOOTH_AVAILABILITY + "?btId=\(pheriphal_Data.identifier)"
//
//        ServiceManager.sharedInstance.callApi(url:apiURl, param_Values: nil, requestMethod: "get", callbackSuccess: { (responseData, apiStatusCode) -> Void in
//
//            let responseDic = responseData as! NSDictionary
//            let statusValue = responseDic["code"] as! Int
//            if (statusValue == 2000) {
//                self.paymentSuccess = 0
//                self.centralManager?.connect(pheriphal_Data, options: nil)
//            }
//            else if (statusValue == chargingConstantCode.CHARGING_PAYMENT_DONE) {
//                self.paymentSuccess = 1
//                self.centralManager?.connect(pheriphal_Data, options: nil)
//            }
//            else if (statusValue == chargingConstantCode.BLUETOOTH_CHARGING_STARTED) {
//                self.bluetoothStatus = 1
//                self.paymentSuccess = 1
//                self.centralManager?.connect(pheriphal_Data, options: nil)
//            }
//            else if (statusValue == chargingConstantCode.BLUETOOTH_CONNECT_FAILED) {
//                self.paymentSuccess = 0
//                self.showToast(message: "Invalid Bluetooth Identifier!", duration: 2.0)
//            }
//
//        },callbackFaliure: { (error:String, message:String) -> Void in
//            self.showToast(message: message, duration: 2.0)
//        })
//    }
    func checkBtAvailabilityTest(BT_Identifier: UUID) {
        let Base_URL = Utilities.returnBaseURL(liveURL: liveApi)
        let apiURl = Base_URL + Charging_ModuleApi.GET_BLUETOOTH_AVAILABILITY + "?btId=\(BT_Identifier)"

        ServiceManager.sharedInstance.callApi(url:apiURl, param_Values: nil, requestMethod: "get", callbackSuccess: { (responseData, apiStatusCode) -> Void in

            let responseDic = responseData as! NSDictionary
            let statusValue = responseDic["code"] as! Int
            let BTName = "Bluetooth 1"
            if (statusValue == 2000) {
                self.paymentSuccess = 0
                self.checkBTStationStatus(BT_Identifier: BT_Identifier, BT_Name: BTName)
            }
            else if (statusValue == chargingConstantCode.CHARGING_PAYMENT_DONE) {
                self.paymentSuccess = 1
                self.checkBTStationStatus(BT_Identifier: BT_Identifier, BT_Name: BTName)
            }
            else if (statusValue == chargingConstantCode.BLUETOOTH_CHARGING_STARTED) {
                self.bluetoothStatus = 1
                self.paymentSuccess = 1
                self.checkBTStationStatus(BT_Identifier: BT_Identifier, BT_Name: BTName)
            }
            else if (statusValue == chargingConstantCode.BLUETOOTH_CONNECT_FAILED) {
                self.paymentSuccess = 0
                self.showToast(message: "Invalid Bluetooth Identifier!", duration: 2.0)
            }
            
        },callbackFaliure: { (error:String, message:String) -> Void in
            self.showToast(message: message, duration: 2.0)
        })
    }
    
    //TODO :- Func to scan bluetooth list again
    @IBAction func scanBtnAction(_ sender: Any) {
//        if centralManager.state != .poweredOn {
//            Utilities.showAlertControllerAction(message: batteryChargingMesg.OPEN_BLUETOOTH_FIRST, title: batteryChargingMesg.BLUETOOTH_ALERT, onView: self, andFirstButtonTitle: "Cancel", andSecondButtonTitle:"OK", andFirstAction: nil, andSecondAction: #selector(self.allowLocationPermission))
//        }
//        else {
//            peripheralsArray.removeAll()
//            centralManager.scanForPeripherals(withServices: nil)
//
//            self.animationView?.startLoadingView()
//            self.view.addSubview(self.animationView!)
//        }
        bluetoothListTableView.reloadData()
        bluetoothListTableView.isHidden = false
    }
    
    func sendText(text: String) {
        if (myPeripheal != nil && myCharacteristic != nil) {
            let data = text.data(using: .utf8)
            myPeripheal.writeValue(data!, for: myCharacteristic!, type: CBCharacteristicWriteType.withResponse)
        }
    }
    
    //TODO :- Func to set bluetooth status
    func checkBTStationStatus(BT_Identifier: UUID, BT_Name: String) {
        print("call API") //37FC19AB-98CA-4543-A68B-D183DA78ACDC
        
        //self.scanBluetoothBtn.isHidden = true
        bluetoothListView.isHidden = true
        BTStatusContainView.isHidden = false
        if (paymentSuccess == 1 && bluetoothStatus == 1) {
            bluetoothStatus = 1
            btStatusMesgLbl.text = "Your Charging is Started. Click on End Button to stop your charging"
            btStartOffBtn.setTitle("Stop", for: .normal)
        }
        else {
            btStatusMesgLbl.text = "Click on Start Button to start your charging"
            btStartOffBtn.setTitle("Start", for: .normal)
        }
        scanBluetoothBtn.isHidden = true
    }
    
    //TODO :- Func to set bluetooth on and off
    @IBAction func btStartOffBtnAction(_ sender: Any) {
        if bluetoothStatus == 0 {
            if paymentSuccess == 1 {
                callStartCommandApi()
            }
            else {
                callPaymentApi()
            }
        }
        else if bluetoothStatus == 1 {
            
            var BT_Identifier = ""
            BT_Identifier = "F8FC6568-6A0C-7782-082E-6125ADBF073E"
            let Base_URL = Utilities.returnBaseURL(liveURL: liveApi)
            let apiURl = Base_URL + Charging_ModuleApi.OFF_BLUETOOTH_API + "?btId=\(BT_Identifier)&command=OFF"

            ServiceManager.sharedInstance.callApi(url:apiURl, param_Values: nil, requestMethod: "get", callbackSuccess: { (responseData, apiStatusCode) -> Void in

                let responseDic = responseData as! NSDictionary
                let statusValue = responseDic["code"] as! Int
                print(statusValue)
                self.changeBluetoothStatus(BT_Status: 2)
                
            },callbackFaliure: { (error:String, message:String) -> Void in
                self.showToast(message: message, duration: 2.0)
            })
//            if (myPeripheal != nil) {
//                BT_Identifier = myPeripheal.identifier.uuidString
//
//                let Base_URL = Utilities.returnBaseURL(liveURL: liveApi)
//                let apiURl = Base_URL + Charging_ModuleApi.OFF_BLUETOOTH_API + "?btId=\(BT_Identifier)&command=OFF"
//
//                ServiceManager.sharedInstance.callApi(url:apiURl, param_Values: nil, requestMethod: "get", callbackSuccess: { (responseData, apiStatusCode) -> Void in
//
//                    let responseDic = responseData as! NSDictionary
//                    let statusValue = responseDic["code"] as! Int
//                    print(statusValue)
//                    self.changeBluetoothStatus(BT_Status: 2)
//
//                },callbackFaliure: { (error:String, message:String) -> Void in
//                    self.showToast(message: message, duration: 2.0)
//                })
//            }
//            else {
//                self.showToast(message: "Please connect bluetooth first!", duration: 2.0)
//            }
        }
        else {
            navigationController?.popViewController(animated: true)
            dismiss(animated: false, completion: nil)
        }
    }
    
    //TODO :- Func to change bluetooth status
    func changeBluetoothStatus(BT_Status: Int) {
        if BT_Status == 1 {
            bluetoothStatus = 1
            print("send on command on bt")
            self.sendText(text : "ON")
            
            btStatusMesgLbl.text = "Your Charging is Started. Click on End Button to stop your charging"
            btStartOffBtn.setTitle("Stop", for: .normal)
        }
        else if BT_Status == 2 {
            bluetoothStatus = 2
            print("send off command on BT")
            btStatusMesgLbl.text = "Your Charging is Ended."
            self.sendText(text : "OFF")
            
            self.showToast(message: "Charging Complete", duration: 2.0)
            
            //stop scanning after .30 seconds
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    //TODO :- Func to call payment Api
    func callPaymentApi() {
//        var BT_Identifier = ""
        let paymentVC = chargingPaymentVC()
        paymentVC.paymentSuccessDelegate = self
//        paymentVC.connectPheriphal = self.myPeripheal
        self.navigationController?.pushViewController(paymentVC, animated: true)
        
//        if (myPeripheal != nil) {
//            BT_Identifier = myPeripheal.identifier.uuidString
//        }
//        else {
//            self.showToast(message: "Please connect bluetooth first!", duration: 2.0)
//        }
//        BT_Identifier = "F8FC6568-6A0C-7782-082E-6125ADBF073E"
//        let Base_URL = Utilities.returnBaseURL(liveURL: liveApi)
//        let apiURl = Base_URL + Charging_ModuleApi.BASE_CHARGING_AMOUNT + "?btId=\(BT_Identifier)"
//
//        ServiceManager.sharedInstance.callApi(url:apiURl, param_Values: nil, requestMethod: "get", callbackSuccess: { (responseData, apiStatusCode) -> Void in
//
//            let responseDic = responseData as! NSDictionary
//            let statusValue = responseDic["code"] as! Int
//
//            if statusValue == 2000 {
//                let paymentDic = responseDic["data"] as! NSDictionary
//                let paymentVC = chargingPaymentVC()
//                paymentVC.paymentSuccessDelegate = self
//                paymentVC.chargingPaymentDic = paymentDic
//                paymentVC.connectPheriphal = self.myPeripheal
//                self.navigationController?.pushViewController(paymentVC, animated: true)
//            }
//
//        },callbackFaliure: { (error:String, message:String) -> Void in
//            self.showToast(message: message, duration: 2.0)
//        })
    }
    
    //TODO :- Func to set payment success
    func paymentSuccess(paymentStatus: String) {
        callStartCommandApi()
    }
    
    //TODO :- Func to call start command api
    func callStartCommandApi() {
        var BT_Identifier = ""
        BT_Identifier = "F8FC6568-6A0C-7782-082E-6125ADBF073E"
        let Base_URL = Utilities.returnBaseURL(liveURL: liveApi)
        let apiURl = Base_URL + Charging_ModuleApi.ON_BLUETOOTH_API + "?btId=\(BT_Identifier)&command=ON"

        ServiceManager.sharedInstance.callApi(url:apiURl, param_Values: nil, requestMethod: "get", callbackSuccess: { (responseData, apiStatusCode) -> Void in

            let responseDic = responseData as! NSDictionary
            let statusValue = responseDic["code"] as! Int
            print(statusValue)
            self.changeBluetoothStatus(BT_Status: 1)
            
        },callbackFaliure: { (error:String, message:String) -> Void in
            self.showToast(message: message, duration: 2.0)
        })
        
//        if (myPeripheal != nil) {
//            BT_Identifier = myPeripheal.identifier.uuidString
//
//            let Base_URL = Utilities.returnBaseURL(liveURL: liveApi)
//            let apiURl = Base_URL + Charging_ModuleApi.ON_BLUETOOTH_API + "?btId=\(BT_Identifier)&command=ON"
//
//            ServiceManager.sharedInstance.callApi(url:apiURl, param_Values: nil, requestMethod: "get", callbackSuccess: { (responseData, apiStatusCode) -> Void in
//
//                let responseDic = responseData as! NSDictionary
//                let statusValue = responseDic["code"] as! Int
//                print(statusValue)
//                self.changeBluetoothStatus(BT_Status: 1)
//
//            },callbackFaliure: { (error:String, message:String) -> Void in
//                self.showToast(message: message, duration: 2.0)
//            })
//        }
//        else {
//            self.BTStatusContainView.isHidden = true
//            self.showToast(message: "Please connect bluetooth first!", duration: 2.0)
//        }
    }
}
