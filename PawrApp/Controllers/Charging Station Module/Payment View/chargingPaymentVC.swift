//
//  chargingPaymentVC.swift
//  PawrApp
//
//  Created by Ayush Bansal on 01/04/21.
//

import UIKit
import CoreBluetooth

protocol paymentSuccessProtocol {
    func paymentSuccess(paymentStatus: String)
}

class chargingPaymentVC: UIViewController, navBakButtonWithPressed, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate {
    
    @IBOutlet weak var navHeaderBarView: UIView!
    @IBOutlet weak var dataScrollView: UIScrollView!
    
    @IBOutlet weak var selectPlanLbl: UILabel!
    
    @IBOutlet weak var chargingPlanCollectionView: UICollectionView!
    @IBOutlet weak var paymentAmountLbl: UILabel!
    @IBOutlet weak var payNowBtn: UIButton!
    
    var navBarView : NavBackView?
    var paymentSuccessDelegate : paymentSuccessProtocol!
    var chargingPaymentDic = NSDictionary()
    var connectPheriphal : CBPeripheral!
    
    let defaults = UserDefaults.standard
    var screenWidth : CGFloat = 0.0
    var screenHeight : CGFloat = 0.0
    var collectionData = 4
    var selectedIndex = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        screenWidth = Utilities.getScreenWidth()
        screenHeight = Utilities.getScreenHeight()
        Utilities.setStatusBarColor(currentView: self.view)

        navBarView = Bundle.main.loadNibNamed("NavBackView", owner: self, options: nil)?.first as? NavBackView
        navBarView?.navBackBtnDelegates = self
        navBarView?.navTitleLabel.text = "Payment"
        navBarView?.navBackButton.setImage(UIImage(named: "whiteBack"), for: .normal)
        navBarView?.mainContentView.frame = navHeaderBarView.frame
        
        self.navHeaderBarView.addSubview(navBarView!)
        
        chargingPlanCollectionView.dataSource = self
        chargingPlanCollectionView.delegate = self
        chargingPlanCollectionView.isScrollEnabled = false
        
        let purchasePlanList = UINib(nibName : "PaymentPlanCollectionViewCell" , bundle:nil)
        chargingPlanCollectionView.register(purchasePlanList, forCellWithReuseIdentifier: "PaymentPlanCollectionCell")
        
        dataScrollView.delegate = self
        
        paymentAmountLbl.isHidden = true
        
        setPageData()
        pageDesignLayout()
    }

    //TODO :- Did Press Back Button
    func didPressButton(button: UIButton) {
        navigationController?.popViewController(animated: true)
        dismiss(animated: false, completion: nil)
    }
    
    //TODO :- Func to set page design layout
    func pageDesignLayout() {
        dataScrollView.backgroundColor = customColor.LIGHT_BG_GRAY_COLOR
        chargingPlanCollectionView.backgroundColor = customColor.LIGHT_BG_GRAY_COLOR
        
        pageDesign.labelLayout(to: paymentAmountLbl, textColor: UIColor.black, font_Size: FontSize.HEADINGFONT22, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
        
        selectPlanLbl.text = "Select Charging Plan"
        selectPlanLbl.textColor = UIColor.black
        selectPlanLbl.font = UIFont(name: TextFontStyle.MONTSERRAT_SEMIBOLD, size: CGFloat(FontSize.HEADINGFONT18))
        
        payNowBtn.backgroundColor = customColor.NEW_THEME_GREEN_COLOR
        payNowBtn.setTitle("Pay Now", for: .normal)
        payNowBtn.setTitleColor(UIColor.white, for: .normal)
        payNowBtn.titleLabel?.font = UIFont(name: TextFontStyle.MONTSERRAT_SEMIBOLD, size: CGFloat(FontSize.HEADINGFONT16))
        
        payNowBtn.layer.cornerRadius = payNowBtn.frame.height/2
        payNowBtn.layer.masksToBounds = true
    }
    
    //TODO :- Func to set page data
    func setPageData() {
//        let paidAmount = chargingPaymentDic["paidAmount"] as! String
//        paymentAmountLbl.text = "\u{20B9} " + paidAmount
        chargingPlanCollectionView.reloadData()
        setDataScrollView()
    }
    
    //TODO :- Func to set page collection view data
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionData
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = chargingPlanCollectionView.dequeueReusableCell(withReuseIdentifier: "PaymentPlanCollectionCell", for: indexPath as IndexPath) as! PaymentPlanCollectionViewCell
        
        cell.planNameLbl.text = "Plan \(indexPath.row + 1)"
        cell.planPriceLbl.text = "\u{20B9} \((indexPath.row + 1)*15)"
        if (indexPath.row == selectedIndex) {
            cell.changeSelectedCellShadow(selectedStatus: true)
        }
        else {
            cell.changeSelectedCellShadow(selectedStatus: false)
        }
        
        return cell
    }
    
    //TODO :- Collection view flow layouts
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        
        let cellSize = CGSize(width: (collectionView.bounds.width - 50)/2, height: 125)
        return cellSize
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.selectedIndex = indexPath.row
        self.chargingPlanCollectionView.reloadData()
    }
    
    //TODO :- Func to set data scroll view
    func setDataScrollView() {
        var planCollectionViewHeight : CGFloat = 0.0
        
        if collectionData % 2 == 0 {
            planCollectionViewHeight = CGFloat((collectionData / 2)) * 135.0
        }
        else {
            planCollectionViewHeight = CGFloat((collectionData / 2) + 1) * 135.0
        }
        
        selectPlanLbl.frame = CGRect(x: 15, y: 20, width: 250, height: 20)
        let planCollectionViewY = selectPlanLbl.frame.origin.y + selectPlanLbl.frame.height + 20
        chargingPlanCollectionView.frame = CGRect(x: 0, y: planCollectionViewY, width: screenWidth, height: planCollectionViewHeight)
        
        let payNowBtnY = planCollectionViewY + planCollectionViewHeight + 30
        let payNowBtnX = (screenWidth - 235)/2
        payNowBtn.frame = CGRect(x:payNowBtnX, y:payNowBtnY, width: 235, height: 40)
        
        dataScrollView.addSubview(selectPlanLbl)
        dataScrollView.addSubview(chargingPlanCollectionView)
        dataScrollView.addSubview(payNowBtn)
        
        let dataScrollViewHeight = payNowBtnY + payNowBtn.frame.height + 10
        if (screenHeight - 60) < dataScrollViewHeight {
            dataScrollView.contentSize = CGSize(width: screenWidth, height: dataScrollViewHeight)
        }
        else {
            dataScrollView.contentSize = CGSize(width: screenWidth, height: screenHeight - 60)
        }
    }
    
    //TODO :- Func to set pay now btn action
    @IBAction func payNowBtnAction(_ sender: Any) {
        if selectedIndex == -1 {
            self.showToast(message: batteryChargingMesg.SELECT_CHARGING_PLAN, duration: 2.0)
        }
        else {
//            let connectedBT_Identifer = connectPheriphal.identifier.uuidString
            let connectedBT_Identifer = "F8FC6568-6A0C-7782-082E-6125ADBF073E"
            //let apiBT_Identifier = chargingPaymentDic["btId"] as! String
            
//            if connectedBT_Identifer == apiBT_Identifier {
//
//            }
//            else {
//                self.showToast(message: batteryChargingMesg.CONNECT_BT_AGAIN, duration: 2.0)
//            }
            
            let Base_URL = Utilities.returnBaseURL(liveURL: liveApi)
            let apiURl = Base_URL + Charging_ModuleApi.CHARGING_PAYMENT_SUCCESS + "?btId=\(connectedBT_Identifer)&planId=\(self.selectedIndex + 1)"

            ServiceManager.sharedInstance.callApi(url:apiURl, param_Values: nil, requestMethod: "get", callbackSuccess: { (responseData, apiStatusCode) -> Void in

                let responseDic = responseData as! NSDictionary
                let statusValue = responseDic["code"] as! Int
                
                if statusValue == 2000 {
                    self.paymentSuccessDelegate.paymentSuccess(paymentStatus: "true")
                    self.navigationController?.popViewController(animated: true)
                }
               
            },callbackFaliure: { (error:String, message:String) -> Void in
                self.showToast(message: message, duration: 2.0)
            })
        }
    }
    
}
