//
//  PrimaryKYCVC.swift
//  PawrApp
//
//  Created by Ayush Bansal on 28/04/21.
//

import UIKit
import Lottie

@available(iOS 13.0, *)
class PrimaryKYCVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var userImageView: UIImageView!
    
    @IBOutlet weak var genderLbl: UILabel!
    @IBOutlet weak var maleBtn: UIButton!
    @IBOutlet weak var femaleBtn: UIButton!
    @IBOutlet weak var otherBtn: UIButton!
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var animationView: UIView!
    
    var defaults = UserDefaults.standard
    
    var userLoginData = NSDictionary()
    var userGender = -1  //0 for male //1 for female //2 for other
    var userImageSelected = 0
    var userImageProfile = UIImage()
    let lottieAnimationView1 = AnimationView(name: "BatterySwap_JSON")
    var screenWidth : CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        screenWidth = Utilities.getScreenWidth()
        
        if (defaults.object(forKey:UserDefaults.UserDefaultsKeys.userLoginData.rawValue) != nil) {
            userLoginData = defaults.getUserLoginData() as NSDictionary
            
            let userProfileID = userLoginData["profilePicId"] as! Int
            let Base_URL = Utilities.returnBaseURL(liveURL: liveApi)
            let urlString = Base_URL + ApiList.LOAD_PIC_FROM_SERVER + "\(userProfileID)"
            let placeholderImage = UIImage(named: "otherSelect")

            userImageView.sd_setImage(with: URL(string: urlString), placeholderImage: placeholderImage, completed: { image, error, cacheType, imageURL in
                if image != nil {
                    self.userImageView.image = image
                    self.userImageSelected = 1
                }
            })
        }
        
        //TODO :- Func to set user image selection
        userImageView.isUserInteractionEnabled = true
        let userImageGesture = UITapGestureRecognizer(target: self, action:  #selector (self.userImageSelectAction (_:)))
        self.userImageView.addGestureRecognizer(userImageGesture)
        
        DispatchQueue.main.async {
            self.uiAnimatedView()
        }
        pageDesignLayout()
    }
    
    //TODO :- Func to set page design layout
    func pageDesignLayout() {
        userImageView.layer.cornerRadius = userImageView.frame.height/2
        userImageView.layer.masksToBounds = true
        
        genderLbl.text = "Gender"
        genderLbl.textColor = UIColor.black
        genderLbl.font = UIFont(name: TextFontStyle.MONTSERRAT_SEMIBOLD, size: CGFloat(FontSize.HEADINGFONT14))
        
        maleBtn.setTitle("Male", for: .normal)
        maleBtn.setTitleColor(UIColor.black, for: .normal)
        maleBtn.titleLabel?.font = UIFont(name: TextFontStyle.MONTSERRAT_SEMIBOLD, size: CGFloat(FontSize.HEADINGFONT13))
        
        femaleBtn.setTitle("Female", for: .normal)
        femaleBtn.setTitleColor(UIColor.black, for: .normal)
        femaleBtn.titleLabel?.font = UIFont(name: TextFontStyle.MONTSERRAT_SEMIBOLD, size: CGFloat(FontSize.HEADINGFONT13))
        
        otherBtn.setTitle("Other", for: .normal)
        otherBtn.setTitleColor(UIColor.black, for: .normal)
        otherBtn.titleLabel?.font = UIFont(name: TextFontStyle.MONTSERRAT_SEMIBOLD, size: CGFloat(FontSize.HEADINGFONT13))
        
        nameTextField.layer.cornerRadius = 5.0
        nameTextField.layer.masksToBounds = true
        nameTextField.layer.borderWidth = 1.5
        nameTextField.layer.borderColor = customColor.BG_GRAY_COLOR.cgColor
        pageDesign.textFieldLayout(to: nameTextField, textColor: UIColor.black, font_Size: FontSize.HEADINGFONT15, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
        pageDesign.textFieldPadding(to: nameTextField, paddingWidth: 5.0, paddingHeight: 30.0)
        
        emailTextField.layer.cornerRadius = 5.0
        emailTextField.layer.masksToBounds = true
        emailTextField.layer.borderWidth = 1.5
        emailTextField.layer.borderColor = customColor.BG_GRAY_COLOR.cgColor
        pageDesign.textFieldLayout(to: emailTextField, textColor: UIColor.black, font_Size: FontSize.HEADINGFONT15, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
        pageDesign.textFieldPadding(to: emailTextField, paddingWidth: 5.0, paddingHeight: 30.0)
        
        submitBtn.setTitle("Submit", for: .normal)
        pageDesign.buttonLayout(to: submitBtn, textColor: UIColor.white, font_Size: FontSize.HEADINGFONT16, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD, bgColor: customColor.NEW_THEME_GREEN_COLOR)
        
        submitBtn.layer.cornerRadius = 5.0
        submitBtn.layer.masksToBounds = true
    }
    
    // TODO :- function to load animation
    func uiAnimatedView() {
        // Setup our animaiton view
        lottieAnimationView1.contentMode = .scaleAspectFit
        lottieAnimationView1.frame = CGRect(x: 0, y: 0, width: screenWidth, height: animationView.frame.height)
        self.animationView.addSubview(lottieAnimationView1)
        
        self.lottieAnimationView1.loopMode = .loop
        self.lottieAnimationView1.play()
    }
    
    //TODO :- Func to select normal station action
    @objc func userImageSelectAction(_ sender:UITapGestureRecognizer) {
        
        Utilities.showAlertControllerAction(message: "", title: "Open Camera", styleType: "sheet", onView: self, andFirstButtonTitle: "Open Camera", andSecondButtonTitle:"Cancel", andFirstAction: #selector(self.openCameraAction), andSecondAction: nil)
    }
    
    // TODO :- Func to open camera action
    @objc func openCameraAction(){
        guard Utilities.checkCameraPermissions(withDelegte: self) == true else {
            Utilities.showAlertControllerAction(message: customMessages.CAMERA_SETTING_MESSAGE, title: customMessages.ALERT_TITLE, styleType: "alert", onView: self, andFirstButtonTitle: "Don't Allow", andSecondButtonTitle: "OK", andFirstAction: nil, andSecondAction: #selector(self.openCameraSettingAction))
            return
        }
        
        if UIImagePickerController.isCameraDeviceAvailable(.front) || UIImagePickerController.isCameraDeviceAvailable(.rear) {
            let picker = UIImagePickerController()
            picker.sourceType = .camera
            picker.cameraDevice = .rear
            picker.delegate = self
            present(picker, animated: true)
        }
    }
    
    /*
     This function is used to open the camera permission from the mobile setting
     */
    @objc func openCameraSettingAction(){
        if let url = URL(string: UIApplication.openSettingsURLString) {
            UIApplication.shared.open(url, completionHandler: { (success) in })
        }
    }
    
    //#pragma  mark - UIImagePickerControllerDelegate
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {

        userImageProfile = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        picker.dismiss(animated: true)
        
        self.uploadProfilePic(profileImage: userImageProfile) { (value) in
            if value != -1 {
                self.uploadImageOnServer(profileID: value, userSelectedImage : self.userImageProfile)
            }
        }
    }
    
    //TODO :- Func to upload image on server
    func uploadImageOnServer(profileID: Int, userSelectedImage : UIImage) {
        
        let Base_URL = Utilities.returnBaseURL(liveURL: 0)
        let newApiURL = Base_URL + ApiList.UPLOAD_USER_PROFILE_PIC + "?profilePicId=\(profileID)"
        
        ServiceManager.sharedInstance.callApi(url:newApiURL, param_Values: nil, requestMethod: "get", callbackSuccess: { (responseData, apiStatusCode) -> Void in

            let responseDic = responseData as! NSDictionary
            if apiStatusCode == 200 {
                self.defaults.removeObject(forKey:UserDefaults.UserDefaultsKeys.userLoginData.rawValue)
                self.defaults.setUserLoginData(value: responseDic as NSDictionary)
                
                self.userImageSelected = 1
                self.userImageView.image = userSelectedImage
            }
            
        },callbackFaliure: { (error:String, message:String) -> Void in
            self.showToast(message: message, duration: 2.0)
        })
    }
    
    @IBAction func genderBtnAction(_ sender: UIButton) {
        maleBtn.setImage(UIImage(named: "radio_unselected"), for: .normal)
        femaleBtn.setImage(UIImage(named: "radio_unselected"), for: .normal)
        otherBtn.setImage(UIImage(named: "radio_unselected"), for: .normal)
        let tagValue = sender.tag
        if tagValue == 0 {
            maleBtn.setImage(UIImage(named: "radioGreenSelected"), for: .normal)
            if userImageSelected == 0 {
                userImageView.image = UIImage(named: "maleSelect")
            }
        }
        else if tagValue == 1 {
            femaleBtn.setImage(UIImage(named: "radioGreenSelected"), for: .normal)
            if userImageSelected == 0 {
                userImageView.image = UIImage(named: "femaleSelect")
            }
        }
        else {
            otherBtn.setImage(UIImage(named: "radioGreenSelected"), for: .normal)
            if userImageSelected == 0 {
                userImageView.image = UIImage(named: "otherSelect")
            }
        }
        userGender = tagValue
    }
    
    //TODO :- Func to set Submit Btn Action
    @IBAction func submitBtnAction(_ sender: Any) {
        guard userGender != -1 else {
            self.showToast(message: customMessages.SELECT_YOUR_GENDER, duration: 2.0)
            return
        }

        guard nameTextField.text != "" else {
            self.showToast(message: customMessages.ENTER_YOUR_NAME, duration: 2.0)
            return
        }

        guard emailTextField.text != "" else {
            self.showToast(message: customMessages.ENTER_YOUR_EMAIL, duration: 2.0)
            return
        }

        guard Utilities.isValidEmail(testStr: emailTextField.text!) == true else {
            self.showToast(message: customMessages.ENTER_VALID_EMAIL_ID, duration: 2.0)
            return
        }

        let userloginDic: NSMutableDictionary = NSMutableDictionary(dictionary: userLoginData)

        if userImageSelected == 0 {
            userloginDic["profilePicId"] = -1
        }

        userloginDic["gender"] = userGender
        userloginDic["name"] = nameTextField.text!
        userloginDic["email"] = emailTextField.text!
        
        callSubmitKYC(userlogin_Dic: userloginDic)
    }
    
    //TODO :- Func to submit your kyc details
    func callSubmitKYC(userlogin_Dic: NSMutableDictionary) {
        var userCurrentLatitude : Double
        var userCurrentLongitude : Double
        var addressString = ""
        
        if Utilities.checkLocationPermissions() == true {
            userCurrentLatitude = UtilitiesValues.getUserCurrentLocation().latitude
            userCurrentLongitude = UtilitiesValues.getUserCurrentLocation().longitude
            
            UtilitiesValues.getUserAddressString(userLat: userCurrentLatitude, userLong: userCurrentLongitude) { (value) in
                addressString = value
                self.callSubmitKYC_Api(address_String: addressString, userLong: userCurrentLongitude, userLat: userCurrentLatitude, userloginDic: userlogin_Dic)
            }
        }
        else {
            userCurrentLatitude = 0
            userCurrentLongitude = 0
            addressString = ""
            self.callSubmitKYC_Api(address_String: addressString, userLong: userCurrentLongitude, userLat: userCurrentLatitude, userloginDic: userlogin_Dic)
        }
    }
    
    func callSubmitKYC_Api(address_String : String, userLong: Double, userLat: Double, userloginDic: NSMutableDictionary) {
        
        self.defaults.removeObject(forKey:UserDefaults.UserDefaultsKeys.userLoginData.rawValue)
        self.defaults.setUserLoginData(value: userloginDic)
        
        let Base_URL = Utilities.returnBaseURL(liveURL: liveApi)
        let apiURl = Base_URL + ApiList.UPLOAD_USER_PRIMARY_KYC_DETAILS + "?secondaryKycCheck=false&latitude=\(userLat)&longitude=\(userLong)&address=\(address_String)&userType=0"
        
        self.view.isUserInteractionEnabled = false
        self.showActivityLoader()
        
        ServiceManager.sharedInstance.callPostApi(url:apiURl, param_Values: (userloginDic as! [String : Any]), requestMethod: "post", callbackSuccess: { (responseData, apiStatusCode) -> Void in

            let responseDic = responseData as! NSDictionary
            if apiStatusCode == 200 {
                
                let jsonDataDic = responseDic["data"] as! NSDictionary
                let loginDataString = jsonDataDic["loginData"] as! String
                let data = loginDataString.data(using: .utf8)
                var dict: [AnyHashable : Any]? = nil
                do {
                    if let data = data {
                        dict = try JSONSerialization.jsonObject(with: data, options: []) as! [String : Any]
                    }
                } catch {
                }
                
                self.defaults.removeObject(forKey:UserDefaults.UserDefaultsKeys.isLoggedIn.rawValue)
                self.defaults.removeObject(forKey:UserDefaults.UserDefaultsKeys.userLoginData.rawValue)
                self.defaults.setUserLoginData(value: dict! as NSDictionary)
                self.defaults.setUserLogIn(value: "true")
                
                let mainStroyBoard = UIStoryboard(name: "Main", bundle: nil)
                let tabBarVC = mainStroyBoard.instantiateViewController(withIdentifier: "MainTabBarID") as! MainTabBarViewController
                self.navigationController?.pushViewController(tabBarVC, animated: true)
            }
            else {
                let mesgString = responseDic["message"] as! String
                self.showToast(message: mesgString, duration: 2.0)
            }
            self.hideActivityLoader()
            self.view.isUserInteractionEnabled = true
            
        },callbackFaliure: { (error:String, message:String) -> Void in
            self.hideActivityLoader()
            self.view.isUserInteractionEnabled = true
            self.showToast(message: message, duration: 2.0)
        })
    }

}
