//
//  BatterySwapAnimationView.swift
//  PawrApp
//
//  Created by Ayush Bansal on 10/04/21.
//

import UIKit
import Lottie

class BatterySwapAnimationView: UIView {

    @IBOutlet weak var mainDataContentView: UIView!
   
    @IBOutlet weak var animationView: UIView!
    @IBOutlet weak var instructionHeadLbl: UILabel!
    @IBOutlet weak var instructionMesgLbl: UILabel!
 
    let lottieAnimationView1 = AnimationView(name: "BatterySwap_JSON")
    var screenWidth : CGFloat = 0.0
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        mainDataContentView.layer.cornerRadius = 15.0
        mainDataContentView.layer.masksToBounds = true
        
        instructionHeadLbl.text = "Instruction"
        instructionHeadLbl.textColor = customColor.LIGHT_BLACK_COLOR
        instructionHeadLbl.font = UIFont(name: TextFontStyle.MONTSERRAT_SEMIBOLD, size: CGFloat(FontSize.HEADINGFONT12))
        
        instructionMesgLbl.textColor = UIColor.black
        instructionMesgLbl.font = UIFont(name: TextFontStyle.MONTSERRAT_SEMIBOLD, size: CGFloat(FontSize.HEADINGFONT15))
        instructionMesgLbl.text = "Put old battery in the container"
        
        screenWidth = Utilities.getScreenWidth()
        // Setup our animaiton view
        lottieAnimationView1.contentMode = .scaleAspectFit
        lottieAnimationView1.frame = CGRect(x: 0, y: 0, width: screenWidth, height: animationView.frame.height)
        self.animationView.addSubview(lottieAnimationView1)
    }
    
    // TODO :- function to load animation
    func uiAnimatedView(){
        self.lottieAnimationView1.loopMode = .loop
        self.lottieAnimationView1.play()
    }
}
