//
//  SwapStationSelectionVC.swift
//  PawrApp
//
//  Created by Ayush Bansal on 09/04/21.
//

import UIKit
import Lottie

@available(iOS 13.0, *)
class SwapStationSelectionVC: UIViewController, enterQRCodeProtocol, QRCodeScanBtnProtocol, SelectSwapPaymentProtocol, SelectPaymentModeProtocol, SetSelectedPaymentProtocol, enterNewBTQRCodeProtocol, newQRCodeScanBtnProtocol, SelectedQRCodeProtocol, BatteryContainerQRScanProtocol, ApplyRemoveCouponProtocol, applyCouponCodeProtocol {
    
    @IBOutlet weak var mainDataView: UIView!
    @IBOutlet weak var viewDismissBtn: UIButton!
    @IBOutlet weak var dataContentView: UIView!
    @IBOutlet weak var selectSwapHeadLbl: UILabel!
    
    @IBOutlet weak var popUpDetailsView: UIView!
    @IBOutlet weak var popUpViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var swapBatteryPayPopUpView: UIView!
    @IBOutlet weak var swapBatteryPayHeightConstraint: NSLayoutConstraint!
    
    //Show Swap Battery Animation PopUp
    @IBOutlet weak var swapAnimationPopUpView: UIView!
    @IBOutlet weak var swapAnimationHeightConstraint: NSLayoutConstraint!
    
    //Enter New Battery QRCode PopUp
    @IBOutlet weak var newBatteryQRPopUpView: UIView!
    @IBOutlet weak var newBatteryQrHeightConstraint: NSLayoutConstraint!
    
    //Swap Success PopUp
    @IBOutlet weak var successBlackView: UIView!
    @IBOutlet weak var swapSuccessPopUpView: UIView!
    @IBOutlet weak var batterySccessLbl: UILabel!
    @IBOutlet weak var batterySwapMesgLbl: UILabel!
    @IBOutlet weak var swapSucccessAnimationView: UIView!
    
    @IBOutlet weak var normalStationView: UIView!
    @IBOutlet weak var nStationImageView: UIImageView!
    @IBOutlet weak var nStationLbl: UILabel!
    @IBOutlet weak var nStationPriceLbl: UILabel!
    
    @IBOutlet weak var smartStationView: UIView!
    @IBOutlet weak var qStationImageView: UIImageView!
    @IBOutlet weak var qStationLbl: UILabel!
    @IBOutlet weak var qStationPriceLbl: UILabel!
    
    @IBOutlet weak var instructionBtn: UIButton!
    @IBOutlet weak var proceedBtn: UIButton!
    
    var swapStationView : SwapBatteryStaionView?
    var swapBatteryPaymentView : BatterySwapPaymentView?
    var swapAnimationView : BatterySwapAnimationView?
    var newBatteryQRCodeView : NewBatteryQrCodeVC?
    
    let defaults = UserDefaults.standard
    var screenHeight : CGFloat = 0.0
    var screenWidth : CGFloat = 0.0
    var stationSelection = -1  //0 for normal station selection //1 for smart station selection
    var swapBatteryDataDic = NSMutableDictionary()
    var oldBatterySwapDataDic = NSMutableDictionary()
    
    var userCurrentLatitude : Double = 0
    var userCurrentLongitude : Double = 0
    var payment_Mode = ""
    let lottieAnimationView1 = AnimationView(name: "success_Submit")
    
    override func viewDidLoad() {
        super.viewDidLoad()

        screenHeight = Utilities.getScreenHeight()
        screenWidth = Utilities.getScreenWidth()
        
        swapBatteryPayPopUpView.isHidden = true
        popUpDetailsView.isHidden = true
        dataContentView.isHidden = false
        swapAnimationPopUpView.isHidden = true
        newBatteryQRPopUpView.isHidden = true
        successBlackView.isHidden = true
        
//        self.mainDataView.isUserInteractionEnabled = true
//        let hideViewGesture = UITapGestureRecognizer(target: self, action:  #selector (self.hideViewAction (_:)))
//        self.mainDataView.addGestureRecognizer(hideViewGesture)
        
        self.normalStationView.isUserInteractionEnabled = true
        let normalStation = UITapGestureRecognizer(target: self, action:  #selector (self.nStationSelectionAction (_:)))
        self.normalStationView.addGestureRecognizer(normalStation)
        
        self.smartStationView.isUserInteractionEnabled = true
        let smartStation = UITapGestureRecognizer(target: self, action:  #selector (self.qStationStelectionAction (_:)))
        self.smartStationView.addGestureRecognizer(smartStation)
        
        //Old and Container Battery View
        swapStationView = Bundle.main.loadNibNamed("SwapBatteryStaionView", owner: self, options: nil)?.first as? SwapBatteryStaionView
        
        swapStationView?.enterQRCodeDelegate = self
        swapStationView?.QRCodeScanBtnDelegate = self
        let subViewHeight = swapStationView?.mainDataContentView.frame.height
        popUpViewHeightConstraint.constant = subViewHeight!
        self.popUpDetailsView.addSubview(swapStationView!)
        
        //Battery Swap Payment View
        swapBatteryPaymentView = Bundle.main.loadNibNamed("BatterySwapPaymentView", owner: self, options: nil)?.first as? BatterySwapPaymentView
        swapBatteryPaymentView?.SelectSwapPaymentDelegate = self
        swapBatteryPaymentView?.SelectPaymentModeDelegate = self
        swapBatteryPaymentView?.applyRemoveCouponDelegate = self
        let swapBatteryPayHeight = swapBatteryPaymentView?.mainDataContentView.frame.height
        swapBatteryPayHeightConstraint.constant = swapBatteryPayHeight!
        self.swapBatteryPayPopUpView.addSubview(swapBatteryPaymentView!)
        
        //Battery Swap Payment Success after animation
        swapAnimationView = Bundle.main.loadNibNamed("BatterySwapAnimationView", owner: self, options: nil)?.first as? BatterySwapAnimationView
        let swapBatteryAnimationHeight = swapAnimationView?.mainDataContentView.frame.height
        swapAnimationHeightConstraint.constant = swapBatteryAnimationHeight!
        self.swapAnimationPopUpView.addSubview(swapAnimationView!)
        
        //New Battery QRCode Details View
        newBatteryQRCodeView = Bundle.main.loadNibNamed("NewBatteryQrCodeVC", owner: self, options: nil)?.first as? NewBatteryQrCodeVC
        newBatteryQRCodeView?.enterNewBTQRCodeDelegate = self
        newBatteryQRCodeView?.newQRCodeScanBtnDelegate = self
        let newBatteryAnimationHeight = newBatteryQRCodeView?.mainDataContentView.frame.height
        newBatteryQrHeightConstraint.constant = newBatteryAnimationHeight!
        self.newBatteryQRPopUpView.addSubview(newBatteryQRCodeView!)
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        pageDesignLayout()
        uiAnimatedView()
        
        NotificationCenter.default.addObserver(self, selector: #selector(ContainerBatteryQRAction(_:)), name: NSNotification.Name("BatteryContainerQR"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(paymentLinkSuccessAction(_:)), name: NSNotification.Name("Payment_Linked_Action"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(BatterySwapSuccessAction(_:)), name: NSNotification.Name("BatterySwapSuccess"), object: nil)
    }
    
    // TODO :- function to load animation
    func uiAnimatedView(){
        // Setup our animaiton view
        lottieAnimationView1.contentMode = .scaleAspectFill
        lottieAnimationView1.frame = CGRect(x: 0, y: 0, width: swapSucccessAnimationView.frame.width, height: swapSucccessAnimationView.frame.height)
        self.swapSucccessAnimationView.addSubview(lottieAnimationView1)

        self.lottieAnimationView1.loopMode = .loop
    }
    
    //TODO :- Func to set page design layout
    func pageDesignLayout() {
        dataContentView.layer.cornerRadius = 15.0
        dataContentView.layer.masksToBounds = true
        
        popUpDetailsView.layer.cornerRadius = 15.0
        popUpDetailsView.layer.masksToBounds = true
        
        swapBatteryPayPopUpView.layer.cornerRadius = 15.0
        swapBatteryPayPopUpView.layer.masksToBounds = true
        
        swapAnimationPopUpView.layer.cornerRadius = 15.0
        swapAnimationPopUpView.layer.masksToBounds = true
        
        newBatteryQRPopUpView.layer.cornerRadius = 15.0
        newBatteryQRPopUpView.layer.masksToBounds = true
        
        selectSwapHeadLbl.text = "Select Station"
        selectSwapHeadLbl.textColor = UIColor.black
        selectSwapHeadLbl.font = UIFont(name: TextFontStyle.MONTSERRAT_SEMIBOLD, size: CGFloat(FontSize.HEADINGFONT18))
        
        //Normal Station View
        nStationImageView.image = UIImage(named: "blackCircle")
        nStationLbl.text = "Normal Station"
        pageDesign.labelLayout(to: nStationLbl, textColor: UIColor.black, font_Size: FontSize.HEADINGFONT14, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
        
        //Smart Station View
        qStationImageView.image = UIImage(named: "blackCircle")
        qStationLbl.text = "PAWWRR Station"
        pageDesign.labelLayout(to: qStationLbl, textColor: UIColor.black, font_Size: FontSize.HEADINGFONT14, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
        
        proceedBtn.setTitle("Proceed", for: .normal)
        pageDesign.buttonLayout(to: proceedBtn, textColor: UIColor.white, font_Size: FontSize.HEADINGFONT16, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD, bgColor: customColor.NEW_THEME_GREEN_COLOR)
        
        proceedBtn.layer.cornerRadius = 5.0
        proceedBtn.layer.masksToBounds = true
        
        instructionBtn.setTitle("Instruction", for: .normal)
        pageDesign.buttonLayout(to: instructionBtn, textColor: customColor.LIGHT_BLACK_COLOR, font_Size: FontSize.HEADINGFONT12, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD, bgColor: UIColor.clear)
        
        //Swap Success Popup layout
        swapSuccessPopUpView.layer.cornerRadius = 10.0
        swapSuccessPopUpView.layer.masksToBounds = true
        
        batterySccessLbl.text = "Success"
        pageDesign.labelLayout(to: batterySccessLbl, textColor: customColor.LIGHT_BLACK_COLOR, font_Size: FontSize.HEADINGFONT14, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
        
        batterySwapMesgLbl.text = "Battery Swapped"
        pageDesign.labelLayout(to: batterySwapMesgLbl, textColor: customColor.LIGHT_BLACK_COLOR, font_Size: FontSize.HEADINGFONT14, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
        
        var normalStationAmount = ""
        var smartStationAmount = ""
        if (defaults.object(forKey:UserDefaults.UserDefaultsKeys.nStationSwapAmount.rawValue) != nil) {
            normalStationAmount = defaults.getNStationSwapAmount()
        }
        else {
            normalStationAmount = ""
        }
        
        if (defaults.object(forKey:UserDefaults.UserDefaultsKeys.smartStationSwapAmount.rawValue) != nil) {
            smartStationAmount = defaults.getQStationSwapAmount()
        }
        else {
            smartStationAmount = ""
        }
        
        if normalStationAmount != "" {
            nStationPriceLbl.isHidden = false
            let nSwapAmount = "\u{20B9} \(normalStationAmount) "
            let nPerSwapString = "/swap"
            nStationPriceLbl.attributedText = Utilities.setAttributeString(preDiscountPrice: nSwapAmount, finalPrice: nPerSwapString)
        }
        else {
            nStationPriceLbl.isHidden = true
        }
        
        if smartStationAmount != "" {
            qStationPriceLbl.isHidden = false
            let qSwapAmount = "\u{20B9} \(smartStationAmount) "
            let qPerSwapString = "/swap"
            qStationPriceLbl.attributedText = Utilities.setAttributeString(preDiscountPrice: qSwapAmount, finalPrice: qPerSwapString)
        }
        else {
            qStationPriceLbl.isHidden = true
        }
    }

    //TODO :- Func to hide view
    @IBAction func hideViewBtnAction(_ sender: Any) {
        dismiss(animated: true) {

        }
    }
    
    //TODO :- Func to select normal station action
    @objc func nStationSelectionAction(_ sender:UITapGestureRecognizer){
        stationSelection = 0
        self.stationSelectionAction()
    }
    
    //TODO :- Func to select smart station action
    @objc func qStationStelectionAction(_ sender:UITapGestureRecognizer){
        stationSelection = 1
        self.stationSelectionAction()
    }
    
    //TODO :- Func user select station action
    func stationSelectionAction() {
        nStationImageView.image = UIImage(named: "blackCircle")
        qStationImageView.image = UIImage(named: "blackCircle")
        if stationSelection == 0 {
            nStationImageView.image = UIImage(named: "fillBlackCircle")
        }
        else {
            qStationImageView.image = UIImage(named: "fillBlackCircle")
        }
    }
    
    //TODO :- Func to set proceed btn action
    @IBAction func proceedBtnAction(_ sender: Any) {
        if stationSelection == -1 {
            self.showToast(message: batterySwapMesg.SELECT_BATTERY_SWAP_STATION, duration: 2.0)
        }
        else if stationSelection == 0 {
            popUpDetailsView.isHidden = false
            dataContentView.isHidden = true
        }
        else {
            checkPreviousBatteryStatus()
        }
    }
    
    //TODO :- Func to check previous battery status
    func checkPreviousBatteryStatus() {
        let Base_URL = Utilities.returnBaseURL(liveURL: liveApi)
        let apiURl = Base_URL + BatterySwap_ModuleApi.GET_SWAP_AMOUNT_DETAILS

        ServiceManager.sharedInstance.callApi(url:apiURl, param_Values: nil, requestMethod: "get", callbackSuccess: { (responseData, apiStatusCode) -> Void in

            if apiStatusCode == 200 {
                
                if !Utilities.isEmptyValue(responseData) {
                    let responseDic = responseData as! NSDictionary
                    let statusCode = responseDic["code"] as! Int
                    if statusCode == 2000 {
                        //            guard Utilities.checkCameraPermissions(withDelegte: self) == true else {
                        //                Utilities.showAlertControllerAction(message: customMessages.CAMERA_SETTING_MESSAGE_QRCODE, title: customMessages.ALERT_TITLE, styleType: "alert", onView: self, andFirstButtonTitle: "Don't Allow", andSecondButtonTitle: "OK", andFirstAction: nil, andSecondAction: #selector(self.openCameraSettingAction))
                        //                return
                        //            }
                                    
                        let ScanContainerQRVC = BatteryContainerQRScanVC()
                        ScanContainerQRVC.qrCodeScanType = "containerQR"
                        ScanContainerQRVC.ContainerQRScanDelegate = self
                        self.navigationController?.pushViewController(ScanContainerQRVC, animated: true)
                    }
                }
            }
        },callbackFaliure: { (error:String, message:String) -> Void in
            self.showToast(message: message, duration: 2.0)
        })
    }
    
    //TODO :- When user scan Battery Container QRCode
    func containerQRCodeAction(qrCode_ScanType: String, qrCodeString: String) {
        if qrCode_ScanType == "batteryQR" {
            swapBatteryDataDic["oldBatteryQR"] = qrCodeString
            
            self.callBatterySwapAmountApi()
        }
        else {
            swapBatteryDataDic["batteryContainerQR"] = qrCodeString
            
            let ScanContainerQRVC = BatteryContainerQRScanVC()
            ScanContainerQRVC.qrCodeScanType = "batteryQR"
            ScanContainerQRVC.ContainerQRScanDelegate = self
            self.navigationController?.pushViewController(ScanContainerQRVC, animated: true)
        }
    }
    
    //TODO :- Func to check instruction
    @IBAction func instructionBtnAction(_ sender: Any) {
    }
    
    //TODO :- Func to get old battery QR Code and get battery Container QR Code
    func getQRCodeValue(oldBatteryQR: String, BatContainerQR: String) {
        guard oldBatteryQR != "" else {
            self.showToast(message: batterySwapMesg.ENTER_OLD_BATTERY_QR_CODE, duration: 2.0)
            return
        }
        guard BatContainerQR != "" else {
            self.showToast(message: batterySwapMesg.ENTER_NEW_BATTERY_QR_CODE, duration: 2.0)
            return
        }
        
        oldBatterySwapDataDic["oldBatteryQR"] = oldBatteryQR
        oldBatterySwapDataDic["newBatteryQR"] = BatContainerQR
        
        callOldBatterySwapAmount()
        
        //New Battery Swap Module
//        swapBatteryDataDic["oldBatteryQR"] = oldBatteryQR
//        swapBatteryDataDic["batteryContainerQR"] = BatContainerQR
//
//        callBatterySwapAmountApi()
    }
    
    //TODO :- Func to call old battery swap payment details api
    func callOldBatterySwapAmount() {
        self.popUpDetailsView.isHidden = true
        self.dataContentView.isHidden = true
        self.swapBatteryPayPopUpView.isHidden = false
        
        self.swapBatteryPaymentView?.moduleType = "OldBatterySwap"
        self.swapBatteryPaymentView?.setPageData(swapDetailsDic: self.oldBatterySwapDataDic)
    }
    
    //TODO :- Func to call battery swap payment details api
    func callBatterySwapAmountApi() {
        let oldBatteryQR = swapBatteryDataDic["oldBatteryQR"] as! String
        let containerQR = swapBatteryDataDic["batteryContainerQR"] as! String
        
        let Base_URL = Utilities.returnBaseURL(liveURL: liveApi)
        let apiURl = Base_URL + BatterySwap_ModuleApi.BATTERY_SWAP_AMOUNT + "?deviceQr=\(containerQR)&qrCode=\(oldBatteryQR)"

        ServiceManager.sharedInstance.callApi(url:apiURl, param_Values: nil, requestMethod: "get", callbackSuccess: { (responseData, apiStatusCode) -> Void in

            if apiStatusCode == 200 {
                
                if !Utilities.isEmptyValue(responseData) {
                    let responseDic = responseData as! NSDictionary
                    let statusCode = responseDic["code"] as! Int
                    if statusCode == batterySwap_ModuleCode.INVALID_BATTERY_QR_CODE {
                        let statusMesg = responseDic["message"] as! String
                        self.showToast(message: statusMesg, duration: 2.0)
                    }
                    else if statusCode == batterySwap_ModuleCode.INVALID_BATTERY_CONTAINER_QR_CODE {
                        let statusMesg = responseDic["message"] as! String
                        self.showToast(message: statusMesg, duration: 2.0)
                    }
                    else if statusCode == constantStatusCode.API_SUCCESS_CODE {
                        let responseDataDic = responseDic["data"] as! NSDictionary
                        
                        let deviceQR = responseDataDic["deviceQr"] as! String
                        let paidAmount = responseDataDic["paidAmount"] as! Int
                        let paymentMode = responseDataDic["paymentMode"] as! String
                        
                        self.swapBatteryDataDic["deviceQR"] = deviceQR
                        self.swapBatteryDataDic["paidAmount"] = paidAmount
                        self.swapBatteryDataDic["paymentMode"] = paymentMode
                        
                        self.popUpDetailsView.isHidden = true
                        self.dataContentView.isHidden = true
                        self.swapBatteryPayPopUpView.isHidden = false
                        
                        self.swapBatteryPaymentView?.setPageData(swapDetailsDic: self.swapBatteryDataDic)
                    }
                    else if statusCode == batterySwap_ModuleCode.SWAP_BATTERY_PAYMENT_DONE {
                        let responseDataDic = responseDic["data"] as! NSDictionary
                        let deviceQR = responseDataDic["deviceQr"] as! String
                        let bsr_id = responseDataDic["bsr_id"] as! Int
                        
                        self.swapBatteryDataDic["deviceQR"] = deviceQR
                        self.swapBatteryDataDic["bsr_ID"] = bsr_id
                        
                        self.showSwapBatteryAnimationView()
                    }
                    else {
                        let statusMesg = responseDic["message"] as! String
                        self.showToast(message: statusMesg, duration: 2.0)
                    }
                }
            }
        },callbackFaliure: { (error:String, message:String) -> Void in
            self.showToast(message: message, duration: 2.0)
        })
    }
    
    //TODO :- Func to apply and remove coupon code
    func applyRemoveCouponAction(actionType: Int) {
        if actionType == 1 {
            let couponVC = CouponListVC()
            couponVC.applyCouponCodeDelegate = self
            couponVC.idTypeValue = couponIDType_Code.COUPON_BATTERY_SWAP
            self.navigationController?.pushViewController(couponVC, animated: true)
        }
        else {
            self.swapBatteryPaymentView?.applyRemoveCouponCode(couponApply: actionType, coupon_Code: "", coupon_Desc: "")
        }
    }
    
    //TODO :- Func when user apply or remove coupon code
    func applyRemoveCoupon(couponStatus: Int, couponCode: String, couponDesc: String) {
        self.swapBatteryPaymentView?.applyRemoveCouponCode(couponApply: 1, coupon_Code: couponCode, coupon_Desc: couponDesc)
    }
    
    //TODO :- Func to user select Container/Battery QR Code
    @objc func ContainerBatteryQRAction(_ notification: NSNotification) {
        let notificationObject = notification.object
        let notificationDic = notificationObject as! NSDictionary
        
        let api_StatusCode = notificationDic["status_Code"] as! Int
        
        let containerQR_Code = notificationDic["batteryContainerQR"] as! String
        let old_BatteryQR_Code = notificationDic["oldBatteryQR"] as! String
        
        self.swapBatteryDataDic["deviceQR"] = containerQR_Code
        self.swapBatteryDataDic["batteryContainerQR"] = containerQR_Code
        self.swapBatteryDataDic["oldBatteryQR"] = old_BatteryQR_Code
        
        if api_StatusCode == constantStatusCode.API_SUCCESS_CODE {
            let paymentModeString = notificationDic["paymentMode"] as! String
            let paid_Amount = notificationDic["paidAmount"] as! Int
            
            if paymentModeString != "" {
                let data = paymentModeString.data(using: .utf8)
                var dict: [AnyHashable : Any]? = nil
                do {
                    if let data = data {
                        dict = try JSONSerialization.jsonObject(with: data, options: []) as! [String : Any]
                    }
                } catch {
                }
                
                let paymentMode = dict!["paymentMode"] as! String
                
                self.swapBatteryDataDic["paymentMode"] = paymentMode
                self.payment_Mode = paymentMode
            }
            else {
                self.payment_Mode = ""
            }
            self.swapBatteryDataDic["paidAmount"] = paid_Amount
            
            self.popUpDetailsView.isHidden = true
            self.dataContentView.isHidden = true
            self.swapBatteryPayPopUpView.isHidden = false
            
            self.swapBatteryPaymentView?.setPageData(swapDetailsDic: self.swapBatteryDataDic)
        }
        else {
            let bsr_id = notificationDic["bsr_Id"] as! Int
            self.swapBatteryDataDic["bsr_ID"] = bsr_id
            
            self.showSwapBatteryAnimationView()
        }
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("BatteryContainerQR"), object: nil)
    }
    
    
    //TODO :- Func to redirect on select payment mode screen
    func selectPaymentMode() {
        let payModeVC = PaymentModeVC()
        payModeVC.SetSelectedPaymentDelegate = self
        self.navigationController?.pushViewController(payModeVC, animated: true)
    }
    
    //TODO :- Func to set payment mode
    func setSelectedPayment(paymentString: String) {
        self.payment_Mode = paymentString
        self.swapBatteryPaymentView?.setPaymentMode(payment_Mode: paymentString)
    }
    
    //TODO :- Func to when user link upi
    @objc func paymentLinkSuccessAction(_ notification: NSNotification) {
        let notificationObject = notification.object
        let notificationDic = notificationObject as! NSDictionary
        
        let userPayment_Type = notificationDic["userPaymentType"] as! String
        
        var paymentTypeString = ""
        if userPayment_Type == "PayTm" {
            paymentTypeString = paymentTypeConstant.PAYMENT_TYPE_PAYTM
        }
        else if userPayment_Type == "Mobikwik" {
            paymentTypeString = paymentTypeConstant.PAYMENT_TPYE_MOBIKWIK_WALLET
        }
        self.payment_Mode = paymentTypeString
        self.swapBatteryPaymentView?.setPaymentMode(payment_Mode: paymentTypeString)
    }
    
    //TODO :- Func to call pay api when user select payment method
    func paymentMethodIsSelected() {
        
        guard payment_Mode != "" else {
            self.showToast(message: paymentCustomMesg.SELECT_PAYMENT_OPTION, duration: 2.0)
            return
        }
        let oldBatteryQR = swapBatteryDataDic["oldBatteryQR"] as! String
        let deviceQR = swapBatteryDataDic["deviceQR"] as! String

        userCurrentLatitude = UtilitiesValues.getUserCurrentLocation().latitude
        userCurrentLongitude = UtilitiesValues.getUserCurrentLocation().longitude

        let Base_URL = Utilities.returnBaseURL(liveURL: liveApi)
        let apiURl = Base_URL + BatterySwap_ModuleApi.PURCHASE_PAY_BATTERY + "?deviceQr=\(deviceQR)&qrCode=\(oldBatteryQR)&couponId=-1&latitude=\(userCurrentLatitude)&longitude=\(userCurrentLongitude)&paymentMode=\(payment_Mode)"

        ServiceManager.sharedInstance.callApi(url:apiURl, param_Values: nil, requestMethod: "get", callbackSuccess: { (responseData, apiStatusCode) -> Void in

            if apiStatusCode == 200 {
                if !Utilities.isEmptyValue(responseData) {
                    let responseDic = responseData as! NSDictionary
                    let statusCode = responseDic["code"] as! Int
                    
                    if statusCode == 20000 {
                        let swapData = responseDic["data"] as! NSDictionary
                        let bsr_id = swapData["bsr_id"] as! Int
                        self.swapBatteryDataDic["bsr_ID"] = bsr_id
                
                        self.showSwapBatteryAnimationView()
                    }
                    else if statusCode == paymentTypeConstant.LOW_WALLET_BALANCE {
                        let paidAmount = self.swapBatteryDataDic["paidAmount"] as! Int
                        
                        let addMoneyVC = AddMoneyVC()
                        addMoneyVC.paymentMethod = self.payment_Mode
                        addMoneyVC.addAmount = paidAmount
                        addMoneyVC.moduleName = "BatterySwap"
                        self.navigationController?.pushViewController(addMoneyVC, animated: true)
                    }
                    else if statusCode == paymentTypeConstant.PAYMENT_WALLET_NOT_ATTACHED {
                        let mesgString = responseDic["message"] as! String
                        self.showToast(message: mesgString, duration: 2.0)
                    }
                }
            }
            self.viewDismissBtn.isUserInteractionEnabled = false
        },callbackFaliure: { (error:String, message:String) -> Void in
            self.showToast(message: message, duration: 2.0)
        })
    }
    
    //TODO :- Func to show swap battery animation view
    func showSwapBatteryAnimationView() {
        self.swapBatteryPayPopUpView.isHidden = true
        self.popUpDetailsView.isHidden = true
        self.dataContentView.isHidden = true
        self.swapAnimationPopUpView.isHidden = false
        self.newBatteryQRPopUpView.isHidden = true
        self.swapAnimationView!.uiAnimatedView()
        
        self.callSwapBatteryApi()
    }
    
    //TODO :- Func to call swap battery api
    func callSwapBatteryApi() {
        let oldBatteryQR = swapBatteryDataDic["oldBatteryQR"] as! String
        let deviceQR = swapBatteryDataDic["deviceQR"] as! String
        let bsr_ID = swapBatteryDataDic["bsr_ID"] as! Int
        
        let Base_URL = Utilities.returnBaseURL(liveURL: liveApi)
        let apiURl = Base_URL + BatterySwap_ModuleApi.SWAPPING_MODULE_BATTERY_SWAP + "?deviceQr=\(deviceQR)&batteryQr=\(oldBatteryQR)&bsr_id=\(bsr_ID)"

        ServiceManager.sharedInstance.callApi(url:apiURl, param_Values: nil, requestMethod: "get", callbackSuccess: { (responseData, apiStatusCode) -> Void in

            if apiStatusCode == 200 {
                if !Utilities.isEmptyValue(responseData) {
                    let responseDic = responseData as! NSDictionary
                    let statusCode = responseDic["code"] as! Int
                    
                    if statusCode == 20000 {
                        let swapData = responseDic["data"] as! NSDictionary
                        let c_ID = swapData["cId"] as! Int
                        self.swapBatteryDataDic["c_ID"] = c_ID
                
                        self.swapBatteryPayPopUpView.isHidden = true
                        self.popUpDetailsView.isHidden = true
                        self.dataContentView.isHidden = true
                        self.swapAnimationPopUpView.isHidden = true
                        //self.newBatteryQRPopUpView.isHidden = false
                        
                        let ScanContainerQRVC = BatteryContainerQRScanVC()
                        ScanContainerQRVC.qrCodeScanType = "newBatteryQR"
                        ScanContainerQRVC.ContainerQRScanDelegate = self
                        ScanContainerQRVC.swapBatteryData_Dic = self.swapBatteryDataDic
                        self.navigationController?.pushViewController(ScanContainerQRVC, animated: true)
                    }
                }
            }
        },callbackFaliure: { (error:String, message:String) -> Void in
            self.showToast(message: message, duration: 2.0)
        })
    }
  
    //TODO :- Func to redirect on QRCode Scan Screen
    func qrCodeBtnAction(qrScanBtn: String) {
//        guard Utilities.checkCameraPermissions(withDelegte: self) == true else {
//            Utilities.showAlertControllerAction(message: customMessages.CAMERA_SETTING_MESSAGE_QRCODE, title: customMessages.ALERT_TITLE, styleType: "alert", onView: self, andFirstButtonTitle: "Don't Allow", andSecondButtonTitle: "OK", andFirstAction: nil, andSecondAction: #selector(self.openCameraSettingAction))
//            return
//        }
        let qrScanVC = QRScannerVC()
        qrScanVC.qrCodeScanType = qrScanBtn
        qrScanVC.SelectedQRCodeDelegate = self
        self.navigationController?.pushViewController(qrScanVC, animated: true)
    }
    
    /*
     This function is used to open the camera permission from the mobile setting
     */
    @objc func openCameraSettingAction(){
        if let url = URL(string: UIApplication.openSettingsURLString) {
            UIApplication.shared.open(url, completionHandler: { (success) in })
        }
    }
    
    //TODO :- When user select QRCode from scanner screen
    func selectQRCodeAction(qrCode_ScanType: String, qrCodeString: String) {
        if (qrCode_ScanType == "oldBattery") {
            swapStationView?.oldBatteryTextField.text = qrCodeString
        }
        else if (qrCode_ScanType == "batteryContainer") {
            swapStationView?.batteryContainerTextField.text = qrCodeString
        }
        else {
            newBatteryQRCodeView?.newBatteryTextField.text = qrCodeString
        }
    }
    
    //TODO :- Func to get new battery QR Code
    func getNewQRCodeValue(newBatteryQR: String) {
        guard newBatteryQR != "" else {
            self.showToast(message: batterySwapMesg.ENTER_NEW_BATTERY_QR_CODE, duration: 2.0)
            return
        }
        callEnterNewBatteryApi(newBattery_QR: newBatteryQR)
    }
    
    //TODO :- Func to call qrcode scan screen
    func newQRCodeBtnAction(qrScanString: String) {
        self.qrCodeBtnAction(qrScanBtn: qrScanString)
    }
    
    //TODO :- Func to call new battery qr code api
    func callEnterNewBatteryApi(newBattery_QR: String) {
    
        let c_Id = swapBatteryDataDic["c_ID"] as! Int
        let bsr_ID = swapBatteryDataDic["bsr_ID"] as! Int

        let Base_URL = Utilities.returnBaseURL(liveURL: liveApi)
        let apiURl = Base_URL + BatterySwap_ModuleApi.SWAPPING_MODULE_BATTERY_SWAP_COMPLETE + "?batteryQr=\(newBattery_QR)&cId=\(c_Id)&bsr_id=\(bsr_ID)"

        ServiceManager.sharedInstance.callApi(url:apiURl, param_Values: nil, requestMethod: "get", callbackSuccess: { (responseData, apiStatusCode) -> Void in

            if apiStatusCode == 200 {
                if !Utilities.isEmptyValue(responseData) {
                    
                    self.swapBatteryPayPopUpView.isHidden = true
                    self.popUpDetailsView.isHidden = true
                    self.dataContentView.isHidden = true
                    self.swapAnimationPopUpView.isHidden = true
                    self.newBatteryQRPopUpView.isHidden = true
                    self.successBlackView.isHidden = false
                    self.lottieAnimationView1.play()
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) {
                        self.successBlackView.isHidden = true
                        
                        self.dismiss(animated: true) {

                        }
                    }
                }
            }
        },callbackFaliure: { (error:String, message:String) -> Void in
            self.showToast(message: message, duration: 2.0)
        })
    }
    
    //TODO :- Func to Battery Swap Success
    @objc func BatterySwapSuccessAction(_ notification: NSNotification) {
        self.swapBatteryPayPopUpView.isHidden = true
        self.popUpDetailsView.isHidden = true
        self.dataContentView.isHidden = true
        self.swapAnimationPopUpView.isHidden = true
        self.newBatteryQRPopUpView.isHidden = true
        self.successBlackView.isHidden = false
        self.lottieAnimationView1.play()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) {
            self.successBlackView.isHidden = true
            
            self.dismiss(animated: true) {

            }
        }
    }
    
}
