//
//  BatterySwapPaymentView.swift
//  PawrApp
//
//  Created by Ayush Bansal on 10/04/21.
//

import UIKit

protocol SelectSwapPaymentProtocol {
    func paymentMethodIsSelected()
}

protocol SelectPaymentModeProtocol {
    func selectPaymentMode()
}

protocol ApplyRemoveCouponProtocol {
    func applyRemoveCouponAction(actionType : Int)
}

class BatterySwapPaymentView: UIView {

    @IBOutlet weak var mainDataContentView: UIView!
    @IBOutlet weak var mainHeaderLbl: UILabel!
    
    @IBOutlet weak var oldBatteryHeadLbl: UILabel!
    @IBOutlet weak var oldBatteryTextField: UITextField!
    
    @IBOutlet weak var containerHeadLbl: UILabel!
    @IBOutlet weak var batteryContainerTextField: UITextField!
    
    @IBOutlet weak var paymentMethodHeadLbl: UILabel!
    
    @IBOutlet weak var selectPaymentView: UIView!
    @IBOutlet weak var selectPaymentLbl: UILabel!
    
    @IBOutlet weak var paymentSelectedView: UIView!
    @IBOutlet weak var paymentTypeImageView: UIImageView!
    @IBOutlet weak var paymentTypeLbl: UILabel!
    
    @IBOutlet weak var swapAmountHeadLbl: UILabel!
    @IBOutlet weak var batterySwapAmountLbl: UILabel!
    
    @IBOutlet weak var couponDetailView: UIView!
    @IBOutlet weak var couponCodeLbl: UILabel!
    @IBOutlet weak var removeCouponBtn: UIButton!
    
    @IBOutlet weak var proceedBtn: UIButton!
    
    var SelectSwapPaymentDelegate : SelectSwapPaymentProtocol!
    var SelectPaymentModeDelegate : SelectPaymentModeProtocol!
    var applyRemoveCouponDelegate : ApplyRemoveCouponProtocol!
    var moduleType : String!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        removeCouponBtn.isHidden = true
        
        mainDataContentView.layer.cornerRadius = 15.0
        mainDataContentView.layer.masksToBounds = true
        
        mainHeaderLbl.text = "Swap Battery"
        pageDesign.labelLayout(to: mainHeaderLbl, textColor: UIColor.black, font_Size: FontSize.HEADINGFONT18, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
        
        proceedBtn.layer.cornerRadius = 5.0
        proceedBtn.layer.masksToBounds = true
        
        oldBatteryTextField.isUserInteractionEnabled = false
        oldBatteryTextField.layer.cornerRadius = 5.0
        oldBatteryTextField.layer.masksToBounds = true
        oldBatteryTextField.layer.borderWidth = 1.0
        oldBatteryTextField.layer.borderColor = customColor.TEXT_LIGHT_GRAY_COLOR.cgColor
        
        oldBatteryHeadLbl.text = "Old Battery QR"
        pageDesign.labelLayout(to: oldBatteryHeadLbl, textColor: customColor.LIGHT_BLACK_COLOR, font_Size: FontSize.HEADINGFONT12, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
        pageDesign.textFieldPadding(to: oldBatteryTextField, paddingWidth: 5.0, paddingHeight: 30.0)
        pageDesign.textFieldLayout(to: oldBatteryTextField, textColor: UIColor.black, font_Size: FontSize.HEADINGFONT14, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
        
        //battery container
        batteryContainerTextField.isUserInteractionEnabled = false
        batteryContainerTextField.layer.cornerRadius = 5.0
        batteryContainerTextField.layer.masksToBounds = true
        batteryContainerTextField.layer.borderWidth = 1.0
        batteryContainerTextField.layer.borderColor = customColor.TEXT_LIGHT_GRAY_COLOR.cgColor
        
        containerHeadLbl.text = "Battery Container QR"
        pageDesign.labelLayout(to: containerHeadLbl, textColor: customColor.LIGHT_BLACK_COLOR, font_Size: FontSize.HEADINGFONT12, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
        pageDesign.textFieldPadding(to: batteryContainerTextField, paddingWidth: 5.0, paddingHeight: 30.0)
        pageDesign.textFieldLayout(to: batteryContainerTextField, textColor: UIColor.black, font_Size: FontSize.HEADINGFONT14, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
        
        //Proceed Btn Layouts
        proceedBtn.layer.cornerRadius = 5.0
        proceedBtn.layer.masksToBounds = true
        
        proceedBtn.setTitle("PAY NOW", for: .normal)
        pageDesign.buttonLayout(to: proceedBtn, textColor: UIColor.white, font_Size: FontSize.HEADINGFONT16, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD, bgColor: customColor.NEW_THEME_GREEN_COLOR)
        
        paymentMethodHeadLbl.text = "Payment Method"
        pageDesign.labelLayout(to: paymentMethodHeadLbl, textColor: customColor.TEXT_DARK_GRAY_COLOR, font_Size: FontSize.HEADINGFONT12, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
        
        selectPaymentLbl.text = "Select Payment Option"
        pageDesign.labelLayout(to: selectPaymentLbl, textColor: customColor.LIGHT_BLACK_COLOR, font_Size: FontSize.HEADINGFONT15, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
        
        swapAmountHeadLbl.text = "Swap Amount"
        pageDesign.labelLayout(to: swapAmountHeadLbl, textColor: UIColor.black, font_Size: FontSize.HEADINGFONT15, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
        pageDesign.labelLayout(to: batterySwapAmountLbl, textColor: UIColor.black, font_Size: FontSize.HEADINGFONT18, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
        
        selectPaymentView.isUserInteractionEnabled = true
        let selectPaymentGesture = UITapGestureRecognizer(target: self, action:  #selector (self.selectPaymentModeAction (_:)))
        self.selectPaymentView.addGestureRecognizer(selectPaymentGesture)
        
        paymentSelectedView.isUserInteractionEnabled = true
        let paymentGesture = UITapGestureRecognizer(target: self, action:  #selector (self.selectPaymentModeAction (_:)))
        self.paymentSelectedView.addGestureRecognizer(paymentGesture)
        
        pageDesign.labelLayout(to: paymentTypeLbl, textColor: UIColor.black, font_Size: FontSize.HEADINGFONT15, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
        
        couponCodeLbl.text = "Apply Coupon"
        pageDesign.labelLayout(to: couponCodeLbl, textColor: customColor.RED_MESG_COLOR, font_Size: FontSize.HEADINGFONT14, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
        
        pageDesign.buttonLayout(to: removeCouponBtn, textColor: customColor.RED_MESG_COLOR, font_Size: FontSize.HEADINGFONT12, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD, bgColor: UIColor.clear)
    }
    
    //TODO :- Func to set page data
    func setPageData(swapDetailsDic : NSMutableDictionary) {
        if moduleType == "OldBatterySwap" {
            oldBatteryTextField.text = (swapDetailsDic["oldBatteryQR"] as! String)
            batteryContainerTextField.text = (swapDetailsDic["newBatteryQR"] as! String)
        }
        else {
            oldBatteryTextField.text = (swapDetailsDic["oldBatteryQR"] as! String)
            batteryContainerTextField.text = (swapDetailsDic["batteryContainerQR"] as! String)
            
            batterySwapAmountLbl.text = "\u{20B9} " + String(swapDetailsDic["paidAmount"] as! Int)
            
            let paymentMode = swapDetailsDic["paymentMode"] as? String
            if paymentMode == nil {
                self.setPaymentMode(payment_Mode: "")
            }
            else {
                self.setPaymentMode(payment_Mode: paymentMode!)
            }
        }
    }
    
    //TODO :- Fun to set payment mode
    func setPaymentMode(payment_Mode: String) {
        if payment_Mode != "" {
            selectPaymentView.isHidden = true
            paymentSelectedView.isHidden = false
            
            if payment_Mode == paymentTypeConstant.PAYMENT_TYPE_PAYTM {
                paymentTypeLbl.text = "PAYTM"
                paymentTypeImageView.image = UIImage(named: "Paytmlogo")
            }
            else if payment_Mode == paymentTypeConstant.PAYMENT_TPYE_MOBIKWIK_WALLET {
                paymentTypeLbl.text = "MobiKwik"
                paymentTypeImageView.image = UIImage(named: "Mobikwik")
            }
            else {
                paymentTypeLbl.text = payment_Mode
            }
        }
        else {
            selectPaymentView.isHidden = false
            paymentSelectedView.isHidden = true
        }
    }
    
    //TODO :- Func to apply or remove coupon
    func applyRemoveCouponCode(couponApply: Int, coupon_Code: String, coupon_Desc: String) {
        if couponApply == 1 {
            couponCodeLbl.text = coupon_Code
            couponCodeLbl.textColor = customColor.NEW_THEME_GREEN_COLOR
            removeCouponBtn.isHidden = false
        }
        else {
            couponCodeLbl.text = "Apply Coupon"
            couponCodeLbl.textColor = customColor.RED_MESG_COLOR
            removeCouponBtn.isHidden = true
        }
    }
    
    //TODO :- Func to hide view or dismiss view
    @objc func selectPaymentModeAction(_ sender:UITapGestureRecognizer){
        self.SelectPaymentModeDelegate.selectPaymentMode()
    }
    
    //TODO :- Func to set proceed btn action 
    @IBAction func proceedBtnAction(_ sender: Any) {
        self.SelectSwapPaymentDelegate.paymentMethodIsSelected() 
    }
    
    //TODO :- Func to apply coupon btn action
    @IBAction func applyCouponBtnAction(_ sender: Any) {
        self.applyRemoveCouponDelegate.applyRemoveCouponAction(actionType: 1)
    }
    
    //TODO :- Func to remove coupon btn action
    @IBAction func removeCouponBtnAction(_ sender: Any) {
        self.applyRemoveCouponDelegate.applyRemoveCouponAction(actionType: 0)
    }
}
