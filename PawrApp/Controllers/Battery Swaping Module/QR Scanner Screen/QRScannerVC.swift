//
//  QRScannerVC.swift
//  PawrApp
//
//  Created by Ayush Bansal on 12/04/21.
//

import UIKit
import WebKit
import AVFoundation

protocol SelectedQRCodeProtocol {
    func selectQRCodeAction(qrCode_ScanType: String, qrCodeString: String)
}

class QRScannerVC: UIViewController, UITextFieldDelegate, WKUIDelegate, WKNavigationDelegate, AVCaptureMetadataOutputObjectsDelegate {

    @IBOutlet weak var previewView: UIView!
    @IBOutlet weak var darkBGView: UIView!
    @IBOutlet weak var cameraRectangleImageView: UIImageView!
    @IBOutlet weak var scanImageView: UIImageView!
    @IBOutlet weak var qrCodeTextField: UITextField!
    @IBOutlet weak var greenArrowImageView: UIImageView!
    @IBOutlet weak var torchBtn: UIButton!
    
    @IBOutlet weak var whiteCurveView: UIView!
    @IBOutlet weak var mesgLbl: UILabel!
    
    var captureSession: AVCaptureSession!
    var videoPreviewLayer: AVCaptureVideoPreviewLayer!
    
    private var scanWebView = WKWebView()
    private var greenArrowWebView = WKWebView()
    
    var SelectedQRCodeDelegate : SelectedQRCodeProtocol!
    var qrCodeScanType : String!
    var tourchOnStatus = 0 // 0 for off and 1 for on
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        captureSession = nil
        captureSession = AVCaptureSession()
        qrCodeTextField.delegate = self
        
        scanWebView.uiDelegate = self
        scanWebView.navigationDelegate = self
        
        greenArrowWebView.uiDelegate = self
        greenArrowWebView.navigationDelegate = self
    
        self.addwebView(webViewName : scanWebView, onView: scanImageView)
        self.addwebView(webViewName : greenArrowWebView, onView: greenArrowImageView)
        
        pageDesignLayout()
        setPageData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.loadHTMLFile("scanQR", webViewName: scanWebView)
        self.loadHTMLFile("greenArrowAnimation", webViewName: greenArrowWebView)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let path = UIBezierPath(rect: view.frame)
        path.append(UIBezierPath(roundedRect: cameraRectangleImageView.frame, cornerRadius: 0).reversing())
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = path.cgPath
        darkBGView.layer.mask = shapeLayer
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(0 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: { [self] in
            startReading()
        })
    }
    
    //TODO :- Func to set page design layout
    func pageDesignLayout() {
        qrCodeTextField.layer.cornerRadius = 5.0
        qrCodeTextField.layer.masksToBounds = true
        
        pageDesign.textFieldLayout(to: qrCodeTextField, textColor: customColor.TEXT_DARK_GRAY_COLOR, font_Size: FontSize.HEADINGFONT15, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
        
        whiteCurveView.layer.cornerRadius = 30.0
        whiteCurveView.layer.masksToBounds = true
        
        if Utilities.getScreenHeight() > 736 {
            pageDesign.labelLayout(to: mesgLbl, textColor: customColor.LIGHT_BLACK_COLOR, font_Size: FontSize.HEADINGFONT15, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
        }
        else {
            pageDesign.labelLayout(to: mesgLbl, textColor: customColor.LIGHT_BLACK_COLOR, font_Size: FontSize.HEADINGFONT13, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
        }
    }
    
    //TODO :- Func to set page data
    func setPageData() {
        if qrCodeScanType == "oldBattery" {
            qrCodeTextField.placeholder = "Type Old Battery QR code."
            mesgLbl.text = "Enter/Scan QR Code on the Old Battery."
        }
        else {
            qrCodeTextField.placeholder = "Type New Battery QR code."
            mesgLbl.text = "Enter/Scan QR Code on the New Battery."
        }
    }
    
    //TODO :- Func to set back btn action
    @IBAction func backBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //TODO :- Func to add webview over uiimage
    func addwebView(webViewName : WKWebView, onView: UIView) {
        onView.addSubview(webViewName)
        webViewName.translatesAutoresizingMaskIntoConstraints = false
        webViewName.frame = CGRect(x: 0, y: 0, width: onView.frame.size.width, height: onView.frame.size.height)
    }
    
    //TODO :- Func to load html file
    func loadHTMLFile(_ htmlFile: String?, webViewName: WKWebView) {
        webViewName.load(URLRequest(url: URL(fileURLWithPath: Bundle.main.path(forResource: htmlFile, ofType: "html") ?? "", isDirectory: false)))
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        // get HTML text
        let jScript = "var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport'); meta.setAttribute('content', 'width=device-width'); document.getElementsByTagName('head')[0].appendChild(meta);"

        webView.evaluateJavaScript(jScript, completionHandler: { _,_ in
            webView.isOpaque = false
            webView.scrollView.isOpaque = false
            webView.backgroundColor = UIColor.clear
            webView.scrollView.backgroundColor = UIColor.clear
        })
    }
    
    //TODO :- Func to start reading qrcode
    func startReading() {
        var error: Error?
        let captureDevice = AVCaptureDevice.default(for: .video)
        var input: AVCaptureDeviceInput? = nil
        do {
            if let captureDevice = captureDevice {
                input = try AVCaptureDeviceInput(device: captureDevice)
            }
        } catch {
        }

        if let input = input {
            self.upCaptureSession(input)
            setUpMetadataCapture()
            setUpPreviewLayer()
            layoutVideoPreview()

            captureSession?.startRunning()
        }
        else {
            
        }
    }
    
    //TODO :- Func to stop qr code reading
    func stopReading() {
        captureSession!.stopRunning()
        captureSession = nil
        videoPreviewLayer?.removeFromSuperlayer()
    }
    
    //TODO :- Func to capture session
    func upCaptureSession(_ input: AVCaptureDeviceInput?) {
        captureSession = AVCaptureSession()
        if let input = input {
            captureSession!.addInput(input)
        }
    }
    
    //TODO :- Func to set up meta data capture
    func setUpMetadataCapture() {
        let metadataOutput = AVCaptureMetadataOutput()

        if (captureSession.canAddOutput(metadataOutput)) {
            captureSession.addOutput(metadataOutput)

            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes = [.qr]
        } else {
            failed()
            return
        }
    }
    
    func failed() {
        Utilities.showAlertView(customMessages.SOMETHING_WENT_WRONG, title: "Alert")
        captureSession = nil
    }
    
    //TODO :- Func to set up preview layer
    func setUpPreviewLayer() {
        self.videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession!)
        self.videoPreviewLayer?.videoGravity = .resizeAspectFill
        previewView.layer.addSublayer(self.videoPreviewLayer!)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        coordinator.animate(alongsideTransition: { [self] context in
            layoutVideoPreview()
        })
    }
    
    func layoutVideoPreview() {
        self.videoPreviewLayer?.frame = previewView.layer.bounds

        let interfaceOrientation = UIApplication.shared.statusBarOrientation
        //let orientation = self.AVCaptureVideoOrientation(rawValue: interfaceOrientation.rawValue)
        let orientation = self.avCaptureVideoOrientation(for: UIInterfaceOrientation(rawValue: interfaceOrientation.rawValue)!)
        self.videoPreviewLayer?.connection?.videoOrientation = orientation
    }
    
    func avCaptureVideoOrientation(for interfaceOrientation: UIInterfaceOrientation) -> AVCaptureVideoOrientation {
        switch interfaceOrientation {
            case .portrait:
                return .portrait
            case .portraitUpsideDown:
                return .portraitUpsideDown
            case .landscapeLeft:
                return .landscapeLeft
            case .landscapeRight:
                return .landscapeRight
            case .unknown:
                return .portrait
            default:
                break
        }
        return .portrait
    }
    
    //pragma mark AVCaptureMetadataOutputObjectsDelegate
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        
        if (metadataObjects.count != 0){
            let metadataObject = metadataObjects[0] as? AVMetadataMachineReadableCodeObject
            
            if (metadataObject?.type == .qr) {
                if "\(metadataObject!)" != "" {
                    let qrCode = "\(metadataObject!)"
                    stopReading()
                    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
                    AudioServicesPlaySystemSound(SystemSoundID(1054))
                    
                    self.SelectedQRCodeDelegate.selectQRCodeAction(qrCode_ScanType: qrCodeScanType, qrCodeString: qrCode)
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
    
    //TODO :- Func to set flash light on and off
    @IBAction func torchBtnAction(_ sender: Any) {
    
        let captureDeviceClass: AnyClass? = NSClassFromString("AVCaptureDevice")
        if captureDeviceClass != nil {
            let device = AVCaptureDevice.default(for: .video)
            if device?.hasTorch ?? false && device?.hasFlash ?? false {

                do {
                    try device?.lockForConfiguration()
                } catch {
                }
                
                if (self.tourchOnStatus == 0) {
                    let btnImage = UIImage(named: "newGreenTorch")
                    torchBtn.setImage(btnImage, for: .normal)
                    device!.torchMode = .on
                    self.tourchOnStatus = 1
                }
                else {
                    let btnImage = UIImage(named: "newTorch")
                    torchBtn.setImage(btnImage, for: .normal)
                    device!.torchMode = .off
                    self.tourchOnStatus = 0
                }
                device!.unlockForConfiguration()
            }
        }
    }
    
    //TODO :- Func to check text field end editing
    func textFieldDidEndEditing(_ textField: UITextField) {
        if (textField.text!.count > 3) {
            self.SelectedQRCodeDelegate.selectQRCodeAction(qrCode_ScanType: qrCodeScanType, qrCodeString: textField.text!)
            self.navigationController?.popViewController(animated: true)
        }
    }
    
}
