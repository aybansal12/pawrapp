//
//  SwapBatteryStaionView.swift
//  PawrApp
//
//  Created by Ayush Bansal on 09/04/21.
//

import UIKit

protocol enterQRCodeProtocol {
    func getQRCodeValue(oldBatteryQR: String, BatContainerQR: String)
}

protocol QRCodeScanBtnProtocol {
    func qrCodeBtnAction(qrScanBtn: String)
}

class SwapBatteryStaionView: UIView {

    @IBOutlet weak var mainDataContentView: UIView!
    @IBOutlet weak var mainHeadLbl: UILabel!
    @IBOutlet weak var proceedBtn: UIButton!
    
    @IBOutlet weak var oldBatteryTextField: UITextField!
    @IBOutlet weak var oldBatteryBtn: UIButton!
    
    @IBOutlet weak var batteryContainerTextField: UITextField!
    
    @IBOutlet weak var instructionBtn: UIButton!
    @IBOutlet weak var batteryContainerBtn: UIButton!
    
    var enterQRCodeDelegate : enterQRCodeProtocol!
    var QRCodeScanBtnDelegate : QRCodeScanBtnProtocol!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        mainDataContentView.layer.cornerRadius = 15.0
        mainDataContentView.layer.masksToBounds = true
        
        mainHeadLbl.text = "Swap Battery"
        pageDesign.labelLayout(to: mainHeadLbl, textColor: UIColor.black, font_Size: FontSize.HEADINGFONT18, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
        
        proceedBtn.layer.cornerRadius = 5.0
        proceedBtn.layer.masksToBounds = true
        proceedBtn.setTitle("Proceed", for: .normal)
        pageDesign.buttonLayout(to: proceedBtn, textColor: UIColor.white, font_Size: FontSize.HEADINGFONT16, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD, bgColor: customColor.NEW_THEME_GREEN_COLOR)
        
        oldBatteryTextField.layer.cornerRadius = 5.0
        oldBatteryTextField.layer.masksToBounds = true
        oldBatteryTextField.layer.borderWidth = 1.0
        oldBatteryTextField.layer.borderColor = customColor.TEXT_LIGHT_GRAY_COLOR.cgColor
        
        oldBatteryBtn.layer.cornerRadius = oldBatteryBtn.frame.height/2
        oldBatteryBtn.layer.masksToBounds = true
        
        pageDesign.textFieldPadding(to: oldBatteryTextField, paddingWidth: 5.0, paddingHeight: 30.0)
        pageDesign.textFieldLayout(to: oldBatteryTextField, textColor: UIColor.black, font_Size: FontSize.HEADINGFONT14, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
        
        //battery container
        batteryContainerTextField.layer.cornerRadius = 5.0
        batteryContainerTextField.layer.masksToBounds = true
        batteryContainerTextField.layer.borderWidth = 1.0
        batteryContainerTextField.layer.borderColor = customColor.TEXT_LIGHT_GRAY_COLOR.cgColor
        
        pageDesign.textFieldPadding(to: batteryContainerTextField, paddingWidth: 5.0, paddingHeight: 30.0)
        pageDesign.textFieldLayout(to: batteryContainerTextField, textColor: UIColor.black, font_Size: FontSize.HEADINGFONT14, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
        
        batteryContainerBtn.layer.cornerRadius = oldBatteryBtn.frame.height/2
        batteryContainerBtn.layer.masksToBounds = true
        
        instructionBtn.setTitle("Instruction", for: .normal)
        pageDesign.buttonLayout(to: instructionBtn, textColor: customColor.LIGHT_BLACK_COLOR, font_Size: FontSize.HEADINGFONT12, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD, bgColor: UIColor.clear)
    }
    
    //TODO :- Func to set proceed btn action
    @IBAction func proceedBtnAction(_ sender: Any) {
        self.enterQRCodeDelegate.getQRCodeValue(oldBatteryQR: oldBatteryTextField.text!, BatContainerQR: batteryContainerTextField.text!)
    }
    
    //TODO :- Func to scan old battery qr code
    @IBAction func oldBatteryBtnAction(_ sender: Any) {
        self.QRCodeScanBtnDelegate.qrCodeBtnAction(qrScanBtn: "oldBattery")
    }
    
    //TODO :- Func to scan battery container qr code
    @IBAction func batteryContainerBtnAction(_ sender: Any) {
        self.QRCodeScanBtnDelegate.qrCodeBtnAction(qrScanBtn: "batteryContainer")
    }
    
    //TODO :- Func to perform instruction btn action
    @IBAction func instructionBtnAction(_ sender: Any) {
    }
    
}
