//
//  NewBatteryQrCodeVC.swift
//  PawrApp
//
//  Created by Ayush Bansal on 22/04/21.
//

import UIKit

protocol enterNewBTQRCodeProtocol {
    func getNewQRCodeValue(newBatteryQR: String)
}

protocol newQRCodeScanBtnProtocol {
    func newQRCodeBtnAction(qrScanString: String)
}

class NewBatteryQrCodeVC: UIView {
    
    @IBOutlet weak var mainDataContentView: UIView!
    @IBOutlet weak var mainHeadLbl: UILabel!
    @IBOutlet weak var proceedBtn: UIButton!
    
    @IBOutlet weak var newBatteryTextField: UITextField!
    @IBOutlet weak var newBatteryBtn: UIButton!
    
    @IBOutlet weak var instructionHeadLbl: UILabel!
    @IBOutlet weak var instructionMesgLbl: UILabel!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    var enterNewBTQRCodeDelegate : enterNewBTQRCodeProtocol!
    var newQRCodeScanBtnDelegate : newQRCodeScanBtnProtocol!
        
    override func awakeFromNib() {
        super.awakeFromNib()
        
        mainDataContentView.layer.cornerRadius = 15.0
        mainDataContentView.layer.masksToBounds = true
        
        mainHeadLbl.text = "New Battery"
        pageDesign.labelLayout(to: mainHeadLbl, textColor: UIColor.black, font_Size: FontSize.HEADINGFONT18, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
        
        proceedBtn.backgroundColor = customColor.NEW_THEME_GREEN_COLOR
        proceedBtn.setTitle("Submit", for: .normal)
        pageDesign.buttonLayout(to: proceedBtn, textColor: UIColor.white, font_Size: FontSize.HEADINGFONT16, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD, bgColor: customColor.NEW_THEME_GREEN_COLOR)
        
        proceedBtn.layer.cornerRadius = 5.0
        proceedBtn.layer.masksToBounds = true
        
        newBatteryTextField.layer.cornerRadius = 5.0
        newBatteryTextField.layer.masksToBounds = true
        newBatteryTextField.layer.borderWidth = 1.0
        newBatteryTextField.layer.borderColor = customColor.TEXT_LIGHT_GRAY_COLOR.cgColor
        
        newBatteryBtn.layer.cornerRadius = newBatteryBtn.frame.height/2
        newBatteryBtn.layer.masksToBounds = true
        
        pageDesign.textFieldPadding(to: newBatteryTextField, paddingWidth: 5.0, paddingHeight: 30.0)
        pageDesign.textFieldLayout(to: newBatteryTextField, textColor: UIColor.black, font_Size: FontSize.HEADINGFONT14, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
        
        instructionHeadLbl.text = "Instruction"
        pageDesign.labelLayout(to: instructionHeadLbl, textColor: customColor.LIGHT_BLACK_COLOR, font_Size: FontSize.HEADINGFONT13, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
        
        instructionMesgLbl.text = "Take out new battery from container"
        pageDesign.labelLayout(to: instructionMesgLbl, textColor: customColor.LIGHT_BLACK_COLOR, font_Size: FontSize.HEADINGFONT13, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
    }

    //TODO :- Func to set proceed btn action
    @IBAction func proceedBtnAction(_ sender: Any) {
        self.enterNewBTQRCodeDelegate.getNewQRCodeValue(newBatteryQR: newBatteryTextField.text!)
    }
    
    //TODO :- Func to open new battery qrcode scanner
    @IBAction func newBatteryBtnAction(_ sender: Any) {
        self.newQRCodeScanBtnDelegate.newQRCodeBtnAction(qrScanString: "newBattery")
    }
    
}
