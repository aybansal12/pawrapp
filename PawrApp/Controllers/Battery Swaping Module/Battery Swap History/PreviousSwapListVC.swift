//
//  PreviousSwapListVC.swift
//  PawrApp
//
//  Created by Ayush Bansal on 12/03/21.
//

import UIKit

@available(iOS 13.0, *)
class PreviousSwapListVC: UIViewController, navBakButtonWithPressed, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var navHeaderBarView: UIView!
    @IBOutlet weak var previousSwapTableView: UITableView!
    
    var navBarView : NavBackView?
    var SwapListArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navBarView = Bundle.main.loadNibNamed("NavBackView", owner: self, options: nil)?.first as? NavBackView
        navBarView?.navBackBtnDelegates = self
        navBarView?.navTitleLabel.text = "Battery Swap History"
        navBarView?.navTitleLabel.textColor = UIColor.black
        
        navBarView?.navBackButton.backgroundColor = UIColor.white
        navBarView?.navBackButton.setImage(UIImage(named: "filledBackIcon"), for: .normal)
        navBarView?.navBackButton.layer.cornerRadius = (navBarView?.navBackButton.frame.height)!/2
        navBarView?.navBackButton.layer.masksToBounds = true
        Utilities.addShadowWithRadius(targetView: (navBarView?.navBackButton)!, widthValue: 0.0, HeightValue: 0.0, shadowOpacity: 0.3, radiusValue: 2.5, colorName: UIColor.black)
        
        navBarView?.mainContentView.frame = navHeaderBarView.frame
        navBarView?.mainContentView.backgroundColor = UIColor.white
        
        self.navHeaderBarView.addSubview(navBarView!)

        previousSwapTableView.dataSource = self
        previousSwapTableView.delegate = self
        previousSwapTableView.separatorStyle = .none
        
        let previousSwapList = UINib(nibName : "PreviousSwapTableViewCell" , bundle:nil)
        previousSwapTableView.register(previousSwapList, forCellReuseIdentifier: "PreviousSwapTableCell")
        
        //Utilities.setStatusBarColor(currentView: self.view)
        DispatchQueue.main.async {
            self.getBatterySwapHistroy()
        }
        pageDesignLayout()
    }
    
    //TODO :- Did Press Back Button
    func didPressButton(button: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //TODO :- Func to set page design layout
    func pageDesignLayout() {
        //previousSwapTableView.backgroundColor = customColor.BG_GRAY_COLOR
    }
    
    //TODO :- Func to get battery swap history details
    func getBatterySwapHistroy() {
        let Base_URL = Utilities.returnBaseURL(liveURL: liveApi)
        let apiURl = Base_URL + BatterySwap_ModuleApi.PREVIOUS_BATTERY_SWAP_HISTORY
        
        self.view.isUserInteractionEnabled = false
        self.showActivityLoader()
        ServiceManager.sharedInstance.callApi(url:apiURl, param_Values: nil, requestMethod: "get", callbackSuccess: { (responseData, apiStatusCode) -> Void in

            if apiStatusCode == 200 {
                let dicData = responseData as! NSArray
                self.SwapListArray.removeAllObjects()
                self.SwapListArray = dicData.mutableCopy() as! NSMutableArray
                
                if self.SwapListArray.count > 0 {
                    self.previousSwapTableView.reloadData()
                }
            }
            self.view.isUserInteractionEnabled = true
            self.hideActivityLoader()

        },callbackFaliure: { (error:String, message:String) -> Void in
            self.view.isUserInteractionEnabled = true
            self.hideActivityLoader()
            self.showToast(message: message, duration: 2.0)
        })
    }
    
    //TODO :- Func to set table view data
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if SwapListArray.count > 0 {
            return SwapListArray.count
        }
        else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 215
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = previousSwapTableView.dequeueReusableCell(withIdentifier: "PreviousSwapTableCell", for: indexPath as IndexPath) as! PreviousSwapTableViewCell
        cell.selectionStyle = .none
        
        let indexDic = self.SwapListArray[indexPath.row] as! NSDictionary
        cell.setCellData(dataDic: indexDic)
        
        return cell
    }

}
