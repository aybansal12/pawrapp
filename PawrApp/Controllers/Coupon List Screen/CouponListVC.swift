//
//  CouponListVC.swift
//  PawrApp
//
//  Created by Ayush Bansal on 17/05/21.
//

import UIKit

protocol applyCouponCodeProtocol {
    func applyRemoveCoupon(couponStatus: Int, couponCode: String, couponDesc: String)
}

class CouponListVC: UIViewController, navBakButtonWithPressed, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var navHeaderBarView: UIView!
    
    @IBOutlet weak var couponDetailView: UIView!
    @IBOutlet weak var applyCouponView: UIView!
    @IBOutlet weak var couponTextField: UITextField!
    @IBOutlet weak var applyCouponBtn: UIButton!
    
    @IBOutlet weak var couponListTableView: UITableView!
    
    var navBarView : NavBackView?
    var idTypeValue = 0
    var couponListArray = NSMutableArray()
    var screenWidth : CGFloat = 0
    
    var applyCouponCodeDelegate : applyCouponCodeProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        screenWidth = Utilities.getScreenWidth()

        navBarView = Bundle.main.loadNibNamed("NavBackView", owner: self, options: nil)?.first as? NavBackView
        navBarView?.navBackBtnDelegates = self
        navBarView?.navTitleLabel.text = "Apply Coupon"
        navBarView?.navTitleLabel.textColor = UIColor.black
        
        navBarView?.navBackButton.backgroundColor = UIColor.white
        navBarView?.navBackButton.setImage(UIImage(named: "filledBackIcon"), for: .normal)
        navBarView?.navBackButton.layer.cornerRadius = (navBarView?.navBackButton.frame.height)!/2
        navBarView?.navBackButton.layer.masksToBounds = true
        Utilities.addShadowWithRadius(targetView: (navBarView?.navBackButton)!, widthValue: 0.0, HeightValue: 0.0, shadowOpacity: 0.3, radiusValue: 2.5, colorName: UIColor.black)
        
        navBarView?.mainContentView.frame = navHeaderBarView.frame
        navBarView?.mainContentView.backgroundColor = UIColor.white
        
        couponTextField.delegate = self
        
        couponListTableView.dataSource = self
        couponListTableView.delegate = self
        couponListTableView.separatorStyle = .none
        
        let couponList = UINib(nibName : "CouponListTableViewCell" , bundle:nil)
        couponListTableView.register(couponList, forCellReuseIdentifier: "CouponListTableCell")
        
        self.navHeaderBarView.addSubview(navBarView!)
        
        DispatchQueue.main.async {
            self.getAvailableRideCoupon()
        }
        pageDesignLayout()
    }
    
    //TODO :- Did Press Back Button
    func didPressButton(button: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //TODO :- Func to set page design layout
    func pageDesignLayout() {
        couponDetailView.backgroundColor = customColor.BG_GRAY_COLOR
        
        applyCouponView.layer.cornerRadius = 5.0
        applyCouponView.layer.masksToBounds = true
        
        couponListTableView.layer.cornerRadius = 5.0
        couponListTableView.layer.masksToBounds = true
        
        applyCouponBtn.setTitle("Apply", for: .normal)
        pageDesign.buttonLayout(to: applyCouponBtn, textColor: UIColor.white, font_Size: FontSize.HEADINGFONT15, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD, bgColor: customColor.NEW_THEME_GREEN_COLOR)
        
        couponTextField.placeholder = "Enter Coupon Code"
        pageDesign.textFieldLayout(to: couponTextField, textColor: UIColor.black, font_Size: FontSize.HEADINGFONT15, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
        
        applyCouponBtn.layer.cornerRadius = applyCouponBtn.frame.height/2
        applyCouponBtn.layer.masksToBounds = true
    }
    
    //TODO :- Func to get couponCode list
    func getAvailableRideCoupon() {
        let Base_URL = Utilities.returnBaseURL(liveURL: liveApi)
        let apiURl = Base_URL + COUPON_CODE_API.COUPON_CODE_LIST + "?idType=\(idTypeValue)&id=\(-1)"

        self.showActivityLoader()
        ServiceManager.sharedInstance.callApi(url:apiURl, param_Values: nil, requestMethod: "get", callbackSuccess: { (responseData, apiStatusCode) -> Void in

            if apiStatusCode == 200 {
                if !Utilities.isEmptyValue(responseData) {
                    let dicData = responseData as! NSArray
                    self.couponListArray.removeAllObjects()
                    self.couponListArray = dicData.mutableCopy() as! NSMutableArray
                    
                    if self.couponListArray.count > 0 {
                        self.couponListTableView.reloadData()
                    }
                }
            }
            self.hideActivityLoader()
        },callbackFaliure: { (error:String, message:String) -> Void in
            self.hideActivityLoader()
            self.showToast(message: message, duration: 2.0)
        })
    }
    
    //TODO :- Func to table view data source and delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if couponListArray.count > 0 {
            return couponListArray.count
        }
        else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 55
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 55))
        
        let lineView = UIView(frame: CGRect(x: 0, y: 58, width: screenWidth, height: 2))
        lineView.backgroundColor = customColor.BG_GRAY_COLOR
        
        let headLbl = UILabel(frame: CGRect(x: 10, y: 15, width: screenWidth - 20, height: 25))
        headLbl.text = "Available Promo Codes"
        pageDesign.labelLayout(to: headLbl, textColor: customColor.TEXT_DARK_GRAY_COLOR, font_Size: FontSize.HEADINGFONT15, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
        
        headerView.addSubview(headLbl)
        headerView.addSubview(lineView)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = couponListTableView.dequeueReusableCell(withIdentifier: "CouponListTableCell", for: indexPath as IndexPath) as! CouponListTableViewCell
        
        let indexDic = self.couponListArray[indexPath.row] as! NSDictionary
        cell.setCellData(DataDic: indexDic)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let indexDic = self.couponListArray[indexPath.row] as! NSDictionary

        let coupon_Code = indexDic["couponCode"] as! String
        self.applyCouponApi(couponCode: coupon_Code)
    }
    
    //TODO :- Func to apply coupon by entering coupon code
    @IBAction func applyCouponBtnAction(_ sender: Any) {
        guard couponTextField.text != nil else {
            self.showToast(message: COUPONCODE_MESG.ENTER_COUPON_CODE, duration: 2.0)
            return
        }
        
        self.applyCouponApi(couponCode: couponTextField.text!)
    }
    
    //TODO :- Func to call apply coupon api
    func applyCouponApi(couponCode: String) {
        let Base_URL = Utilities.returnBaseURL(liveURL: liveApi)
        let apiURl = Base_URL + COUPON_CODE_API.CHECK_COUPON_CODE + "/\(couponCode)/coupon/-1?idType=\(idTypeValue)&id=-1"

        self.showActivityLoader()
        ServiceManager.sharedInstance.callApi(url:apiURl, param_Values: nil, requestMethod: "get", callbackSuccess: { (responseData, apiStatusCode) -> Void in

            if apiStatusCode == 200 {
                if !Utilities.isEmptyValue(responseData) {
                    let responseDic = responseData as! NSDictionary
                    let statusCode = responseDic["code"] as! Int
                    
                    if statusCode == 2000 {
                        self.navigationController?.popViewController(animated: true)
                        self.applyCouponCodeDelegate.applyRemoveCoupon(couponStatus: 1, couponCode: couponCode, couponDesc: "")
                    }
                    else if statusCode == 2001 {
                        self.showToast(message: COUPONCODE_MESG.COUPON_CODE_NOT_VALID, duration: 2.0)
                    }
                    else {
                        let mesgString = responseDic["message"] as! String
                        self.showToast(message: mesgString, duration: 2.0)
                    }
                }
            }
            self.hideActivityLoader()
        },callbackFaliure: { (error:String, message:String) -> Void in
            self.hideActivityLoader()
            self.showToast(message: message, duration: 2.0)
        })
    }
    
}
