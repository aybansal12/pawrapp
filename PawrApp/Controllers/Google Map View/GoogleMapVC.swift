//
//  GoogleMapVC.swift
//  PawrApp
//
//  Created by Ayush Bansal on 26/03/21.
//

import UIKit
import GoogleMaps
import CoreLocation
import GooglePlaces

@available(iOS 13.0, *)
class GoogleMapVC: UIViewController, GMSMapViewDelegate, CLLocationManagerDelegate, UISearchBarDelegate, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var googleMapView: GMSMapView!
    @IBOutlet weak var relocateBtn: UIButton!
    
    @IBOutlet weak var searchActivityLoader: UIActivityIndicatorView!
    @IBOutlet weak var addressSearchBar: UISearchBar!
    @IBOutlet weak var addressSearchTableView: UITableView!
    @IBOutlet weak var searchTableViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var swapBatteryBtn: UIButton!
    
    @IBOutlet weak var stationDetailView: UIView!
    @IBOutlet weak var stationDetailHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var stationNameLbl: UILabel!
    @IBOutlet weak var stationTypeLbl: UILabel!
    @IBOutlet weak var batteryCountLbl: UILabel!
    @IBOutlet weak var scanBatteryBtn: UIButton!
    
    var routePolyline = GMSPolyline()
    var polyLineEnable : Bool!
    let defaults = UserDefaults.standard
    
    var centerMapCoordinate:CLLocationCoordinate2D!
    let locationManager = CLLocationManager()
    var userCurrentLatitude : Double = 0
    var userCurrentLongitude : Double = 0
    let currentLocationMarker = GMSMarker()
    
    var swapBatteryLatitude : CLLocationDegrees = 0.0
    var swapBatteryLongitude : CLLocationDegrees = 0.0
    var stationListDataModel = [newSwappingStationModel]()
    var chargingStationDataModel = [chargingStationListModel]()
    var stationType = 0 //0 for battery swapping station //1 for charging station
    var locationOnStatus = false

    var autoCompleteTimer: Timer?
    var searchAddress_String = ""
    var searchAddress_Array = NSMutableArray()
    var firstTimeLaunch = 0
    var screenHeight : CGFloat = 0
    var userLoginStatus : Bool!
    var verticalSpace = NSLayoutConstraint()
    var firstTym = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        userLoginStatus = UtilitiesValues.getUserLoginStatus()
        screenHeight = Utilities.getScreenHeight()
        googleMapView.delegate = self
        currentLocationMarker.icon = UIImage(named: "source_Marker")
        addressSearchBar.placeholder = "Search Location"
        
        if stationType == 0 {
            self.stationListDataModel = stationListSingleton.shared.stationListDetails
        }
        else {
            self.chargingStationDataModel = chargingStationSingleton.shared.chargingStationDetails
        }
        
        addressSearchBar.delegate = self
        
        addressSearchTableView.dataSource = self
        addressSearchTableView.delegate = self
        addressSearchTableView.separatorStyle = .none
        
        let addressNib = UINib(nibName : "AddressSearchTableViewCell" , bundle:nil)
        addressSearchTableView.register(addressNib, forCellReuseIdentifier: "AddressSearchTableCell")
        
        addressSearchTableView.isHidden = true
        
        searchActivityLoader.color = customColor.NEW_THEME_GREEN_COLOR
        searchActivityLoader.isHidden = true
        
        pageDesignLayout()
        self.showBatteryStationOnMap()
        
        self.stationDetailView.isHidden = true
        stationDetailHeightConstraint.constant = 0
        getSwapAmount()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        locationOnStatus = Utilities.checkLocationPermissions()
        updateLocationDetails()
    }
    
    //TODO :- Func to set page design layout
    func pageDesignLayout(){
        relocateBtn.backgroundColor = UIColor.white
        relocateBtn.layer.cornerRadius = relocateBtn.frame.height/2
        relocateBtn.layer.masksToBounds = true
        Utilities.addShadowWithRadius(targetView: relocateBtn, widthValue: 0.0, HeightValue: 0.0, shadowOpacity: 0.3, radiusValue: 0.2, colorName: UIColor.black)
        
        addressSearchBar.layer.cornerRadius = 10.0
        addressSearchBar.layer.masksToBounds = true
        
        swapBatteryBtn.layer.cornerRadius = swapBatteryBtn.frame.height/2
        swapBatteryBtn.layer.masksToBounds = true
        
        Utilities.addShadowWithRadius(targetView: swapBatteryBtn, widthValue: 0.0, HeightValue: 0.0, shadowOpacity: 0.3, radiusValue: 0.2, colorName: UIColor.black)
        
        stationDetailView.layer.cornerRadius = 15.0
        stationDetailView.layer.masksToBounds = true
        Utilities.addShadowWithRadius(targetView: stationDetailView, widthValue: 0.0, HeightValue: 0.0, shadowOpacity: 0.3, radiusValue: 0.2, colorName: UIColor.black)
        
        
        pageDesign.labelLayout(to: stationNameLbl, textColor: UIColor.black, font_Size: FontSize.HEADINGFONT15, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
        
        pageDesign.labelLayout(to: stationTypeLbl, textColor: UIColor.black, font_Size: FontSize.HEADINGFONT14, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
        pageDesign.labelLayout(to: batteryCountLbl, textColor: UIColor.black, font_Size: FontSize.HEADINGFONT15, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
        
        scanBatteryBtn.layer.cornerRadius = scanBatteryBtn.frame.height/2
        scanBatteryBtn.layer.masksToBounds = true
    }
    
    //TODO :- Func to fetch swap amount
    func getSwapAmount() {
        let Base_URL = Utilities.returnBaseURL(liveURL: liveApi)
        let apiURl = Base_URL + BatterySwap_ModuleApi.GET_SWAP_AMOUNT_DETAILS

        ServiceManager.sharedInstance.callApi(url:apiURl, param_Values: nil, requestMethod: "get", callbackSuccess: { (responseData, apiStatusCode) -> Void in

            if apiStatusCode == 200 {
                if !Utilities.isEmptyValue(responseData) {
                    let responseDic = responseData as! NSDictionary
                    let statusCode = responseDic["code"] as! Int
                    if statusCode == 2000 {
                        let DicData = responseDic["data"] as! NSDictionary
                        let nSwapAmount = DicData["normalSwapAmount"] as! Int
                        let qSwapAmount = DicData["qSmartSwapAmount"] as! Int
                        
                        self.defaults.removeObject(forKey:UserDefaults.UserDefaultsKeys.nStationSwapAmount.rawValue)
                        self.defaults.removeObject(forKey:UserDefaults.UserDefaultsKeys.smartStationSwapAmount.rawValue)
                        self.defaults.setNStationSwapAmount(value: String(nSwapAmount))
                        self.defaults.setQStationSwapAmount(value: String(qSwapAmount))
                    }
                    
                }
            }
        },callbackFaliure: { (error:String, message:String) -> Void in
            self.showToast(message: message, duration: 2.0)
        })
    }
    
    //TODO :- Func to update location status
    func updateLocationDetails() {
        if locationOnStatus == false {
            Utilities.showAlertControllerAction(message: "To see all battery swap station. Please enable location from setting", title: "Location Access Disabled!", styleType: "alert", onView: self, andFirstButtonTitle: "Cancel", andSecondButtonTitle:"OK", andFirstAction: nil, andSecondAction: #selector(self.allowLocationPermission))
        }
        else {
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
            self.locationManager.startMonitoringSignificantLocationChanges()
            
            let locValue = self.locationManager.location?.coordinate
            if locValue != nil {
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.25) {
                    self.userCurrentLatitude = (locValue!.latitude)
                    self.userCurrentLongitude = (locValue!.longitude)

                    self.setMarkerOnGoogleMap()
                }
            }
        }
    }
    
    //TOOD :- Func to set marker and user location on google map
    func setMarkerOnGoogleMap() {
        let camera = GMSCameraPosition.camera(withLatitude: userCurrentLatitude, longitude: userCurrentLongitude, zoom: Float(constantValue.ROUTECAMERA_ZOOM_VALUE))
        if !(googleMapView != nil) {
            googleMapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        }
        
        do {
            googleMapView.mapStyle = try GMSMapStyle(jsonString: kMapStyle)
        } catch {
            NSLog("One or more of the map styles failed to load. \(error)")
        }
        googleMapView.camera = camera
        googleMapView.isMyLocationEnabled = true
        
        currentLocationMarker.position = CLLocationCoordinate2D(latitude: userCurrentLatitude, longitude: userCurrentLongitude)
        currentLocationMarker.map = self.googleMapView
    }
    
    // TODO :- function to allow location permission
    @objc func allowLocationPermission(){
        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
            return
        }
        if UIApplication.shared.canOpenURL(settingsUrl) {
            UIApplication.shared.open(settingsUrl, completionHandler: { (success) in })
        }
    }
    
    //TODO :- Func to show swap station on map
    func showBatteryStationOnMap(){
        if stationType == 0 {
            for dicObj in self.stationListDataModel {
                
                let mapMarker = GMSMarker()
                let placeName = dicObj.placeName
                let pointLatitude = dicObj.latitude
                let pointLongitude = dicObj.longitude
                let stationVisible = dicObj.visible
                mapMarker.position = CLLocationCoordinate2D(latitude: Double(truncating: pointLatitude!), longitude: Double(truncating: pointLongitude!))
                
                if (stationVisible == 1) {
                    mapMarker.icon = UIImage(named: "smartSwapMarker")
                }
                else{
                    mapMarker.icon = UIImage(named: "swapNoBattMarker")
                }
                mapMarker.isTappable = true
                mapMarker.tracksViewChanges = false
                mapMarker.tracksInfoWindowChanges = false
                mapMarker.map = self.googleMapView
                mapMarker.userData = dicObj
                mapMarker.title = placeName
            }
        }
        else {
            for dicObj in self.chargingStationDataModel {
                
                let mapMarker = GMSMarker()
                let placeName = dicObj.placeName
                let pointLatitude = dicObj.latitude
                let pointLongitude = dicObj.longitude
                let stationVisible = dicObj.visible
                mapMarker.position = CLLocationCoordinate2D(latitude: Double(truncating: pointLatitude!), longitude: Double(truncating: pointLongitude!))
                
                if (stationVisible == 1) {
                    mapMarker.icon = UIImage(named: "chargingMarker")
                }
                else{
                    mapMarker.icon = UIImage(named: "noChargingMarker")
                }
                mapMarker.isTappable = true
                mapMarker.tracksViewChanges = false
                mapMarker.tracksInfoWindowChanges = false
                mapMarker.map = self.googleMapView
                mapMarker.userData = dicObj
                mapMarker.title = placeName
            }
        }
    }
    
    //TODO :- When camera position is changed
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        if firstTym == 0 {
            firstTym = 1
        }
        else {
            self.userCurrentLatitude = mapView.camera.target.latitude
            self.userCurrentLongitude = mapView.camera.target.longitude

            centerMapCoordinate = CLLocationCoordinate2D(latitude: self.userCurrentLatitude, longitude: self.userCurrentLongitude)
            self.placeMarkerOnCenter(centerMapCoordinate:centerMapCoordinate)
        }

//        self.setMarkerOnGoogleMap()
    }
    
    func placeMarkerOnCenter(centerMapCoordinate:CLLocationCoordinate2D) {
        self.googleMapView.animate(toLocation: centerMapCoordinate)
        currentLocationMarker.position = centerMapCoordinate
        currentLocationMarker.map = self.googleMapView
    }
    
    //TODO :- Func to current location address string
//    func getAddressStringByLat_Long(userLat: Double, userLong: Double) {
//
//        GMSGeocoder().reverseGeocodeCoordinate(CLLocationCoordinate2DMake(CLLocationDegrees(userLat), CLLocationDegrees(userLong)), completionHandler: { response, error in
//
//            if (response?.results()?.count ?? 0) > 0 && response?.results()?.first != nil {
//                let addressObj = response?.results()?.first
//                var CompleteAddress = ""
//                CompleteAddress = (addressObj?.lines?.first)!
//                //self.pickUpAddressTextField.text = CompleteAddress
//            }
//        })
//    }

    //TODO :- Func when user tap on marker
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        routePolyline.map = nil
        
//        userCurrentLatitude = UtilitiesValues.getUserCurrentLocation().latitude
//        userCurrentLongitude = UtilitiesValues.getUserCurrentLocation().longitude
        
        if marker != currentLocationMarker {
            swapBatteryLatitude = marker.position.latitude
            swapBatteryLongitude = marker.position.longitude
            
            var addressString = ""
            var batteryCount = 0
            var stationVisible = 0
            if stationType == 0 {
                let dataDic = marker.userData as! newSwappingStationModel
                addressString = dataDic.address!
                batteryCount = dataDic.batteryCount!
                stationVisible = dataDic.visible!
            }
            else {

            }
    //        if(dataDic.count > 0) {
    //            print(dataDic)
    //            self.selectedMarkerDic =  dataDic.mutableCopy() as! NSMutableDictionary
    //            drawPolyLineTwoPoint(startPointLat: userCurrentLatitude, startPointLong: userCurrentLongitude, endPointLat: swapBatteryLatitude, endPointLong: swapBatteryLongitude)
    //        }
            
            //drawPolyLineTwoPoint(startPointLat: userCurrentLatitude, startPointLong: userCurrentLongitude, endPointLat: swapBatteryLatitude, endPointLong: swapBatteryLongitude)
            if stationVisible == 1 {
                if batteryCount > 1 {
                    self.batteryCountLbl.text = String(batteryCount) + " Batteries Available"
                }
                else if batteryCount == 1 {
                    self.batteryCountLbl.text = String(batteryCount) + " Battery Available"
                }
                else {
                    self.batteryCountLbl.text = "No Battery Available"
                }
                stationTypeLbl.text = "Q-Smart Station"
            }
            else {
                self.batteryCountLbl.text = "No Battery Available"
                stationTypeLbl.text = "Normal Station"
            }
    
            self.stationNameLbl.text = addressString
            self.showHideStationDetailView(viewStatus: 1)
        }
        
        return false
    }
    
    //TODO :- Func to fetch loaction array to draw polyline
    func drawPolyLineTwoPoint(startPointLat: CLLocationDegrees, startPointLong: CLLocationDegrees, endPointLat: CLLocationDegrees, endPointLong: CLLocationDegrees){
        
        Utilities.getPolylineRoute(startPoint_Lat: startPointLat, startPoint_Long: startPointLong, endPoint_Lat: endPointLat, endPoint_Long: endPointLong, callbackSuccess: { (newRouteArray) -> Void in
            
            if (newRouteArray.count > 0) {
                
                let overview_polyline = newRouteArray[0] as? NSDictionary
                let dictPolyline = overview_polyline?["overview_polyline"] as? NSDictionary
                
                let points = dictPolyline?.object(forKey: "points") as? String
                
                DispatchQueue.main.async {
                    
                    self.showPath(polyStr: points!)
                    self.polyLineEnable = true
                }
            }
        })
    }
    
    func showPath(polyStr :String){
        let path = GMSPath(fromEncodedPath: polyStr)
        routePolyline = GMSPolyline(path: path)
        routePolyline.strokeWidth = 2.0
        routePolyline.strokeColor = customColor.SWAPPING_MAP_POLYLINE_COLOR
        routePolyline.map = googleMapView // Your map view
    }
    
    //TODO :- Func to relocate on the user current location
    @IBAction func relocateBtnAction(_ sender: Any) {
        guard  Utilities.checkLocationPermissions() == true else {
            updateLocationDetails()
            return
        }
        self.userCurrentLatitude = UtilitiesValues.getUserCurrentLocation().latitude
        self.userCurrentLongitude = UtilitiesValues.getUserCurrentLocation().longitude
        
        self.addressSearchBar.resignFirstResponder()
        self.setMarkerOnGoogleMap()
        
        self.showHideStationDetailView(viewStatus: 0)
    }
    
    //TODO :- When user click on search btn from the keyboard
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.addressSearchBar.resignFirstResponder()
    }
    
    //TODO :- Func to check uisearch bar text changes
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text != "" {
            let searchWordProtection = searchBar.text!.replacingOccurrences(of: " ", with: "")
            
            if searchWordProtection.count != 0 {
                DispatchQueue.main.async(execute: { [self] in
                    self.showHideLoader(hideLoader: false)
                })
                self.searchAddress()
            }
            else {
                DispatchQueue.main.async(execute: { [self] in
                    self.showHideLoader(hideLoader: true)
                })
            }
        }
        else {
            DispatchQueue.main.async {
                self.showHideLoader(hideLoader: true)
            }
            self.searchAddress_String = ""
            self.searchAddress_Array.removeAllObjects()
            self.addressSearchTableView.isHidden = true
            self.searchTableViewHeightConstraint.constant = 0
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        searchAddress_String = searchBar.text!
        searchAddress_String = searchAddress_String.replacingOccurrences(of: " ", with: "+")
        searchAddress_String = searchAddress_String.replacingCharacters(in: Range(range, in: searchAddress_String)!,
                                                             with: text)

//        if searchAddress_String.hasPrefix("+") && searchAddress_String.count > 1 {
//            searchAddress_String = searchAddress_String.substring(to: 1)
//        }

        return true
    }
    
    //TODO :- UISearch Bar Text End Editing
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        if searchBar.text != "" {
            let searchWordProtection = searchBar.text!.replacingOccurrences(of: " ", with: "")
            
            if searchWordProtection.count != 0 {
                DispatchQueue.main.async(execute: { [self] in
                    self.showHideLoader(hideLoader: false)
                })
                self.searchAddress()
            }
            else {
                DispatchQueue.main.async(execute: { [self] in
                    self.showHideLoader(hideLoader: true)
                })
            }
        }
    }
     
    //TODO :- Func to search enter address
    func searchAddress() {
        autoCompleteTimer?.invalidate()
        autoCompleteTimer = Timer.scheduledTimer(timeInterval: 0.7, target: self, selector: #selector(self.searchGoogleAddress), userInfo: nil, repeats: false)
    }
    
    //TODO :- Func to search google address
    @objc func searchGoogleAddress() {
        
        DispatchQueue.main.async {
            self.showHideLoader(hideLoader: false)
        }
        
        Utilities.getSearchAddress_Data(searchString: searchAddress_String, callbackSuccess: { (searchPredArray) -> Void in
            
            DispatchQueue.main.async {
                if (searchPredArray.count > 0) {
                    self.searchAddress_Array.removeAllObjects()
                    for predDataObj in searchPredArray {
                        let predArray = predDataObj as! NSDictionary
                        self.searchAddress_Array.add(predArray)
                    }
                    
                    if self.searchAddress_Array.count > 0 {
                        self.addressSearchTableView.isHidden = false
                        
                        let tableViewHeight = CGFloat(self.searchAddress_Array.count * 60)
                        if (tableViewHeight > self.screenHeight - 90) {
                            self.searchTableViewHeightConstraint.constant = self.screenHeight - 90
                        }
                        else {
                            self.searchTableViewHeightConstraint.constant = tableViewHeight
                        }
                        self.addressSearchTableView.reloadData()
                    }
                }
                self.showHideLoader(hideLoader: true)
            }
        })
    }
    
    //TODO :- Func to hide or show activityLoader
    func showHideLoader(hideLoader: Bool) {
        if hideLoader == true {
            self.searchActivityLoader.isHidden = true
            self.searchActivityLoader.stopAnimating()
        }
        else {
            self.searchActivityLoader.isHidden = false
            self.searchActivityLoader.startAnimating()
        }
    }
    
    //TODO :- Func to set table view search datasource and delegates
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchAddress_Array.count > 0 {
            return searchAddress_Array.count
        }
        else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = addressSearchTableView.dequeueReusableCell(withIdentifier: "AddressSearchTableCell", for: indexPath as IndexPath) as! AddressSearchTableViewCell
        cell.selectionStyle = .none
        
        let indexDic = searchAddress_Array[indexPath.row] as! NSDictionary
        cell.setCellData(dataDic: indexDic)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.showHideLoader(hideLoader: true)
        self.addressSearchBar.resignFirstResponder()
        
        self.searchAddress_String = ""
        self.addressSearchTableView.isHidden = true
        self.searchTableViewHeightConstraint.constant = 0
        
        let indexDic = searchAddress_Array[indexPath.row] as! NSDictionary
        let placeID = indexDic["place_id"] as! String
        
        Utilities.getLatLongByPlace_Id(placeIDString: placeID, callbackSuccess: { (locationDataDic) -> Void in
            
            DispatchQueue.main.async {
                if (locationDataDic.count > 0) {
                    let geometryDic = locationDataDic["geometry"] as! NSDictionary
                    let locationDic = geometryDic["location"] as! NSDictionary
                    
                    self.userCurrentLatitude = locationDic["lat"] as! Double
                    self.userCurrentLongitude = locationDic["lng"] as! Double
                    self.setMarkerOnGoogleMap()
                }
            }
        })
    }
    
    
    //TODO :- Func to swap battery btn action
    @IBAction func swapBatteryBtnAction(_ sender: Any) {
        if userLoginStatus == true {
            
            guard  Utilities.checkLocationPermissions() == true else {
                
                Utilities.showAlertControllerAction(message: "Please allow location permission", title: "Location Access Disabled!", styleType: "alert", onView: self, andFirstButtonTitle: "Cancel", andSecondButtonTitle:"OK", andFirstAction: nil, andSecondAction: #selector(self.allowLocationPermission))
                return
            }
            
            let swapStationVC = SwapStationSelectionVC(nibName: "SwapStationSelectionVC", bundle: nil)
            let swapVC = UINavigationController(rootViewController: swapStationVC)
            swapVC.view.backgroundColor = customColor.BLACK_COLOR_WITH_ALPHA
            swapVC.modalPresentationStyle = .overCurrentContext
            self.present(swapVC, animated: true)
            
//            let swapStationVC = SwapStationSelectionVC()
//            swapStationVC.view.backgroundColor = customColor.BLACK_COLOR_WITH_ALPHA
//            swapStationVC.modalPresentationStyle = .overCurrentContext
//            self.present(swapStationVC, animated: true, completion: nil)
//            self.navigationController?.pushViewController(swapStationVC, animated: true)
        }
        else {
            Utilities.showAlertControllerAction(message: customMessages.LOGIN_FIRST_ALERT, title: customMessages.LOGIN_ALERT, styleType: "alert", onView: self, andFirstButtonTitle: "Cancel", andSecondButtonTitle:"OK", andFirstAction: nil, andSecondAction: #selector(self.redirectLoginScreenAction))
        }
    }
    
    // TODO :- function to allow location permission
    @objc func redirectLoginScreenAction(){
        let loginVC = LoginScreenVC()
        self.navigationController?.pushViewController(loginVC, animated: true)
    }
    
    //TODO :- Func to cancel or hide marker details view
    @IBAction func cancelPopBtnAction(_ sender: Any) {
        self.showHideStationDetailView(viewStatus: 0)
    }
    
    //TODO :- Func to hide and show station detail view
    func showHideStationDetailView(viewStatus: Int) {
        var bottomConstant : CGFloat = 0
        if viewStatus == 0 {
            stationDetailView.isHidden = true
            stationDetailHeightConstraint.constant = 0
            bottomConstant = 0
        }
        else {
            stationDetailView.isHidden = false
            stationDetailHeightConstraint.constant = 140
            bottomConstant = -15
        }
        
        verticalSpace.isActive = false
        
        verticalSpace = NSLayoutConstraint(item: self.relocateBtn as Any, attribute: .bottom, relatedBy: .equal, toItem: self.stationDetailView, attribute: .top, multiplier: 1, constant: bottomConstant)
        NSLayoutConstraint.activate([verticalSpace])
        verticalSpace.isActive = true
    }
    
    //TODO :- Func to set google map navigation btn action
    @IBAction func googleNavigationBtnAction(_ sender: Any) {
//        if (UIApplication.shared.canOpenURL(NSURL(string:"comgooglemaps://")! as URL)) {
//            UIApplication.shared.ope(NSURL(string: "comgooglemaps://?saddr=&daddr=\(swapBatteryLatitude),\(swapBatteryLongitude)&directionsmode=driving")! as URL)
//
//        } else {
//            print("Can't use comgooglemaps://")
//        }
        
        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {  //if phone has an app

        if let url = URL(string: "comgooglemaps-x-callback://?saddr=&daddr=\(swapBatteryLatitude),\(swapBatteryLongitude)&directionsmode=driving") {
            UIApplication.shared.open(url, options: [:])
        }}
        else {
            //Open in browser
            if let urlDestination = URL.init(string: "https://www.google.co.in/maps/dir/?saddr=&daddr=\(swapBatteryLatitude),\(swapBatteryLongitude)&directionsmode=driving") {
                UIApplication.shared.open(urlDestination)
            }
        }

    }
}

