//
//  LoginScreenVC.swift
//  PawrApp
//
//  Created by Ayush Bansal on 09/03/21.
//

import UIKit

@available(iOS 13.0, *)
class LoginScreenVC: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var topHeaderView: UIView!
    
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var headermesgLbl: UILabel!
    @IBOutlet weak var skipBtn: UIButton!
    
    @IBOutlet weak var headerSubMesgLbl: UILabel!
    
    @IBOutlet weak var mobileNoHeadLbl: UILabel!
    @IBOutlet weak var mobilePrefixLbl: UILabel!
    
    @IBOutlet weak var mobileNoline: UIView!
    @IBOutlet weak var mobileNotextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    //Paswword View
    @IBOutlet weak var passwordDetailView: UIView!
    @IBOutlet weak var enterPassLbl: UILabel!
    @IBOutlet weak var forgotPassBtn: UIButton!
    @IBOutlet weak var passwordLine: UIView!
    
    //SignUp View
    @IBOutlet weak var signUpView: UIView!
    @IBOutlet weak var signUpLbl: UILabel!
    @IBOutlet weak var forgotPassLbl: UILabel!
    
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var nextButtonHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var nextButtonWidthConstraint: NSLayoutConstraint!
    
    var screenHeight : CGFloat = 0
    var setScreen = 0 // 0 number is not input and 1 is for mobile number is input
    let defaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()

        screenHeight = Utilities.getScreenHeight()
        mobileNotextField.delegate = self
        mobileNotextField.keyboardType = .numberPad
        passwordTextField.delegate = self
        
        signUpView.isUserInteractionEnabled = true
        let signUpScreen = UITapGestureRecognizer(target: self, action: #selector(self.signUpScreenAction(_:)))
        signUpView.addGestureRecognizer(signUpScreen)
        
        backBtn.isHidden = true
        passwordDetailView.isHidden = true
        
        Utilities.setStatusBarColor(currentView: self.view)
        pageDesignLayout()
    }

    //TODO :- Func to set page design layout
    func pageDesignLayout() {
        self.view.backgroundColor = customColor.BG_GRAY_COLOR
        topHeaderView.backgroundColor = customColor.NEW_THEME_GREEN_COLOR
        
        skipBtn.setTitle("Skip", for: .normal)
        skipBtn.setTitleColor(UIColor.white, for: .normal)
        skipBtn.titleLabel?.font = UIFont(name: TextFontStyle.MONTSERRAT_SEMIBOLD, size: CGFloat(FontSize.HEADINGFONT13))
        
        headermesgLbl.text = "Welcome Back"
        pageDesign.labelLayout(to: headermesgLbl, textColor: UIColor.white, font_Size: FontSize.HEADINGFONT22, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
        
        headerSubMesgLbl.text = "Login using your existing Zypp Account"
        
        mobileNoHeadLbl.text = "Enter your Mobile Number"
        mobileNoline.backgroundColor = customColor.TEXT_DARK_GRAY_COLOR
        
        mobilePrefixLbl.text = "+91"
        pageDesign.labelLayout(to: mobilePrefixLbl, textColor: UIColor.black, font_Size: FontSize.HEADINGFONT20, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
        
        pageDesign.textFieldLayout(to: mobileNotextField, textColor: UIColor.black, font_Size: FontSize.HEADINGFONT20, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
        
        signUpLbl.text = "Sign Up"
        pageDesign.labelLayout(to: signUpLbl, textColor: UIColor.black, font_Size: FontSize.HEADINGFONT20, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
        
        forgotPassLbl.text = "Forgot password?"
        pageDesign.labelLayout(to: forgotPassLbl, textColor: customColor.TEXT_DARK_GRAY_COLOR, font_Size: FontSize.HEADINGFONT13, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
        
        passwordLine.backgroundColor = customColor.TEXT_DARK_GRAY_COLOR
        enterPassLbl.text = "Enter Password"
        enterPassLbl.textColor = UIColor.black
        pageDesign.textFieldLayout(to: passwordTextField, textColor: UIColor.black, font_Size: FontSize.HEADINGFONT20, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
            
        forgotPassBtn.setTitle("Forgot password", for: .normal)
        pageDesign.buttonLayout(to: forgotPassBtn, textColor: customColor.TEXT_DARK_GRAY_COLOR, font_Size: FontSize.HEADINGFONT13, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD, bgColor: UIColor.clear)
  
        nextButton.imageView?.contentMode = .scaleAspectFit
        nextButton.imageEdgeInsets = UIEdgeInsets(top: 5.0, left: 5.0, bottom: 5.0, right: 5.0)
        nextButton.backgroundColor = customColor.NEW_THEME_GREEN_COLOR
        
        if screenHeight > 667 {
            pageDesign.labelLayout(to: headerSubMesgLbl, textColor: UIColor.black, font_Size: FontSize.HEADINGFONT14, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
            pageDesign.labelLayout(to: mobileNoHeadLbl, textColor: UIColor.black, font_Size: FontSize.HEADINGFONT15, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
            pageDesign.labelLayout(to: enterPassLbl, textColor: UIColor.black, font_Size: FontSize.HEADINGFONT14, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)

            nextButtonHeightConstraint.constant = 70
            nextButtonWidthConstraint.constant = 70

            self.nextButton.layer.cornerRadius = 35
        }
        else {
            pageDesign.labelLayout(to: headerSubMesgLbl, textColor: UIColor.black, font_Size: FontSize.HEADINGFONT13, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
            pageDesign.labelLayout(to: mobileNoHeadLbl, textColor: UIColor.black, font_Size: FontSize.HEADINGFONT14, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
            pageDesign.labelLayout(to: enterPassLbl, textColor: UIColor.black, font_Size: FontSize.HEADINGFONT13, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)

            nextButtonHeightConstraint.constant = 50
            nextButtonWidthConstraint.constant = 50

            self.nextButton.layer.cornerRadius = 25
            //self.nextButton.clipsToBounds = true
        }
        self.nextButton.layer.masksToBounds = true
        Utilities.addShadowWithRadius(targetView: nextButton, widthValue: 0.0, HeightValue: 0.0, shadowOpacity: 0.3, radiusValue: 2.5, colorName: UIColor.black)
    }
    
    //TODO :- Func to redirect on Zypp Home Delivery Action
    @objc func signUpScreenAction(_ sender: UITapGestureRecognizer? = nil) {
        signUpForgotPassword(forgotPass: 0)
    }

    @IBAction func nextBtnAction(_ sender: Any) {
        self.view.endEditing(true)
        
//        let nextVC = PrimaryKYCVC()
//        self.navigationController?.pushViewController(nextVC, animated: true)
        
        guard mobileNotextField.text != "" else {
            self.showToast(message: customMessages.ENTER_MOBILE_NO, duration: 2.0)
            return
        }

        guard Utilities.onlyNumeric(value: mobileNotextField.text!) == true  else {
            self.showToast(message: customMessages.ENTER_VALID_MOBILE_NO, duration: 2.0)
            return
        }

        let Base_URL = Utilities.returnBaseURL(liveURL: liveApi)
        if self.setScreen == 0 {
            let apiURl = Base_URL + ApiList.CHECK_MOBILE_NUMBER + "/\(mobileNotextField.text!)"

            self.showActivityLoader()
            ServiceManager.sharedInstance.callApi(url:apiURl, param_Values: nil, requestMethod: "get", callbackSuccess: { (responseData, apiStatusCode) -> Void in

                let responseDic = responseData as! NSDictionary
                let statusValue = responseDic["status"] as! Int
                if statusValue == 0 {
                    let verify_OTPVC = VerifyOtpVC()
                    verify_OTPVC.enteredMobileNo = self.mobileNotextField.text!
                    verify_OTPVC.forgotPassword = 0
                    self.navigationController?.pushViewController(verify_OTPVC, animated: true)
                }
                else {
                    self.setScreen = 1
                    self.passwordDetailView.isHidden = false
                    self.backBtn.isHidden = false
                    self.signUpView.isHidden = true
                    self.skipBtn.isHidden = true
                }
                self.hideActivityLoader()
            },callbackFaliure: { (error:String, message:String) -> Void in
                self.hideActivityLoader()
                self.showToast(message: message, duration: 2.0)
            })
        }
        else {
            guard passwordTextField.text != "" else {
                self.showToast(message: customMessages.ENTER_PASSWORD, duration: 2.0)
                return
            }
            guard passwordTextField.text!.count > 5 else {
                self.showToast(message: customMessages.ENTER_VALID_PASSWORD, duration: 2.0)
                return
            }

            let apiURl = Base_URL + ApiList.USER_LOGIN
            let paramValues = ["username": self.mobileNotextField.text!, "password": self.passwordTextField.text!,] as [String : Any]

            self.view.isUserInteractionEnabled = false
            self.showActivityLoader()
            ServiceManager.sharedInstance.callApi(url:apiURl, param_Values: paramValues, requestMethod: "get", callbackSuccess: { (responseData, apiStatusCode) -> Void in

                self.view.isUserInteractionEnabled = true
                if apiStatusCode == 200 {
                    
                    let userLogin_Data = responseData as! NSDictionary
                    
                    self.defaults.removeObject(forKey:UserDefaults.UserDefaultsKeys.userLoginPassword.rawValue)
                    self.defaults.setUserLoginPassword(value: self.passwordTextField.text!)
                    
                    self.defaults.removeObject(forKey:UserDefaults.UserDefaultsKeys.userLoginData.rawValue)
                    self.defaults.setUserLoginData(value: userLogin_Data)
                    
                    let profilePic = userLogin_Data["profilePicId"] as! Int
                    if profilePic == 0 {
                        let KYCVC = PrimaryKYCVC()
                        KYCVC.userLoginData = userLogin_Data
                        self.navigationController?.pushViewController(KYCVC, animated: true)
                    }
                    else {
                        self.defaults.removeObject(forKey:UserDefaults.UserDefaultsKeys.isLoggedIn.rawValue)
                        self.defaults.setUserLogIn(value: "true")

                        let mainStroyBoard = UIStoryboard(name: "Main", bundle: nil)
                        let tabBarVC = mainStroyBoard.instantiateViewController(withIdentifier: "MainTabBarID") as! MainTabBarViewController
                        self.navigationController?.pushViewController(tabBarVC, animated: true)
                    }
                }
                else {
                    let userLogin_Data = responseData as! NSDictionary

                    let statusValue = userLogin_Data["code"] as! Int
                    let MesgString = userLogin_Data["message"] as! String
                    if statusValue == constantStatusCode.NOT_REGISTERED_USER {
                        Utilities.showAlertView(MesgString, title: "Alert!")
                    }
                    else {
                        Utilities.showAlertView(MesgString, title: "Alert!")
                    }
                }
                self.hideActivityLoader()

            },callbackFaliure: { (error:String, message:String) -> Void in
                self.view.isUserInteractionEnabled = true
                self.hideActivityLoader()
                self.showToast(message: message, duration: 2.0)
            })
        }
    }
    
    //TODO :- Func to set forgot password btn action
    @IBAction func forgotPassBtnAction(_ sender: Any) {
        signUpForgotPassword(forgotPass: 1)
    }
    
    //TODO :- Func to redirect on sign up and forgot password screen
    func signUpForgotPassword(forgotPass : Int) {
        let signUp_VC = SignUpVCVC()
        signUp_VC.forgotPassword = forgotPass
        self.navigationController?.pushViewController(signUp_VC, animated: true)
    }
    
    //TODO :- Func to set skip btn action
    @IBAction func skipBtnAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let HomeVC = storyboard.instantiateViewController(withIdentifier: "MainTabBarID")
        self.navigationController?.pushViewController(HomeVC, animated: true)
    }
    
    //TODO :- Func to perform back btn action
    @IBAction func backBtnAction(_ sender: Any) {
        self.setScreen = 0
        passwordDetailView.isHidden = true
        backBtn.isHidden = true
        signUpView.isHidden = false
        skipBtn.isHidden = false
    }
    
    // TODO :- Function to check mobile character count
    func textField(_ textField: UITextField, shouldChangeCharactersIn range:NSRange, replacementString string: String) -> Bool {
        if textField == mobileNotextField {
            let currentCharacterCount = textField.text?.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            if newLength == 11 {
              self.view.endEditing(true)
            }
            return  newLength <= 10
        }
        else{
            return true
        }
    }
    
    //TODO :- Func to check text field start editing
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == mobileNotextField {
            mobileNoline.backgroundColor = customColor.NEW_THEME_GREEN_COLOR
        }
        else if textField == passwordTextField  {
            passwordLine.backgroundColor = customColor.NEW_THEME_GREEN_COLOR
        }
    }
    
    //TODO :- Func to check text field end editing
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == mobileNotextField {
            mobileNoline.backgroundColor = customColor.TEXT_DARK_GRAY_COLOR
        }
        else if textField == passwordTextField {
            passwordLine.backgroundColor = customColor.TEXT_DARK_GRAY_COLOR
        }
    }
    
}


