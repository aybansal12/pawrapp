//
//  SetPasswordVC.swift
//  PawrApp
//
//  Created by Ayush Bansal on 01/04/21.
//

import UIKit

@available(iOS 13.0, *)
class SetPasswordVC: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var backBtn: UIButton!
    
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var nextButtonHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var nextButtonWidthConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    var userLoginData = NSDictionary()
    let defaults = UserDefaults.standard
    var screenHeight : CGFloat = 0
    var forgotPassword = 0
    var enteredMobile_No : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        passwordTextField.delegate = self
        
        screenHeight = Utilities.getScreenHeight()
        pageDesignLayout()
    }
    
    //TODO :- Func to set page design layout
    func pageDesignLayout() {
        backBtn.backgroundColor = UIColor.white
        backBtn.layer.cornerRadius = backBtn.frame.height/2
        backBtn.layer.masksToBounds = true
        Utilities.addShadowWithRadius(targetView: backBtn, widthValue: 0.0, HeightValue: 0.0, shadowOpacity: 0.3, radiusValue: 1.75, colorName: UIColor.black)
        
        passwordTextField.layer.borderWidth = 1.5
        passwordTextField.layer.borderColor = customColor.TEXT_LIGHT_GRAY_COLOR.cgColor
        passwordTextField.layer.cornerRadius = 10.0
        passwordTextField.layer.masksToBounds = true
        
        pageDesign.textFieldLayout(to: passwordTextField, textColor: UIColor.black, font_Size: FontSize.HEADINGFONT20, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
        
        nextButton.imageView?.contentMode = .scaleAspectFit
        nextButton.imageEdgeInsets = UIEdgeInsets(top: 5.0, left: 5.0, bottom: 5.0, right: 5.0)
        nextButton.backgroundColor = customColor.NEW_THEME_GREEN_COLOR
        
        if screenHeight > 667 {
            nextButtonHeightConstraint.constant = 70
            nextButtonWidthConstraint.constant = 70

            self.nextButton.layer.cornerRadius = 35
        }
        else {
            nextButtonHeightConstraint.constant = 50
            nextButtonWidthConstraint.constant = 50

            self.nextButton.layer.cornerRadius = 25
            //self.nextButton.clipsToBounds = true
        }
        self.nextButton.layer.masksToBounds = true
        Utilities.addShadowWithRadius(targetView: nextButton, widthValue: 0.0, HeightValue: 0.0, shadowOpacity: 0.3, radiusValue: 2.5, colorName: UIColor.black)
    }
    
    //TODO :- Func to set back btn action
    @IBAction func backBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    

    //TODO :- Func to set next btn action
    @IBAction func nextBtnAction(_ sender: Any) {
        guard passwordTextField.text != "" else {
            self.showToast(message: customMessages.ENTER_PASSWORD, duration: 2.0)
            return
        }
        
        guard passwordTextField.text!.count > 5 else {
            self.showToast(message: customMessages.ENTER_VALID_PASSWORD, duration: 2.0)
            return
        }
    
        if forgotPassword == 0 {
            generateNewPassword()
        }
        else {
            resetPassoword()
        }
    }
    
    //TODO :- Func to set re-set password
    func resetPassoword() {
        let Base_URL = Utilities.returnBaseURL(liveURL: liveApi)
        let apiURl = Base_URL + ApiList.SET_USER_FORGOT_PASSWORD + "?otp=\(passwordTextField.text!)&username=\(enteredMobile_No!)&newPassword=\(passwordTextField.text!)"
        
        ServiceManager.sharedInstance.callApi(url:apiURl, param_Values: nil, requestMethod: "get", callbackSuccess: { (responseData, apiStatusCode) -> Void in

            let responseDic = responseData as! NSDictionary
            if apiStatusCode == 200 {
                
                let emailVerify = responseDic["emailVerified"] as! Int
                if emailVerify == 0 {
                    let KYCVC = PrimaryKYCVC()
                    KYCVC.userLoginData = responseDic
                    self.navigationController?.pushViewController(KYCVC, animated: true)
                }
                else {
                    self.defaults.removeObject(forKey:UserDefaults.UserDefaultsKeys.isLoggedIn.rawValue)
                    self.defaults.removeObject(forKey:UserDefaults.UserDefaultsKeys.userLoginPassword.rawValue)
                    self.defaults.removeObject(forKey:UserDefaults.UserDefaultsKeys.userLoginData.rawValue)
                    self.defaults.setUserLoginData(value: responseDic)
                    self.defaults.setUserLogIn(value: "true")
                    self.defaults.setUserLoginPassword(value: self.passwordTextField.text!)
                    
                    let mainStroyBoard = UIStoryboard(name: "Main", bundle: nil)
                    let tabBarVC = mainStroyBoard.instantiateViewController(withIdentifier: "MainTabBarID") as! MainTabBarViewController
                    self.navigationController?.pushViewController(tabBarVC, animated: true)
                }
            }
            else {
                let mesgString = responseDic["message"] as! String
                self.showToast(message: mesgString, duration: 2.0)
            }
            
        },callbackFaliure: { (error:String, message:String) -> Void in
            self.showToast(message: message, duration: 2.0)
        })
    }
    
    //TODO :- Func to generate new password
    func generateNewPassword() {
        let Base_URL = Utilities.returnBaseURL(liveURL: liveApi)
        let apiURl = Base_URL + ApiList.SET_USER_PASSWORD + "?password=\(passwordTextField.text!)"
        
        if userLoginData.count < 1 {
            self.showToast(message: customMessages.SOMETHING_WENT_WRONG, duration: 2.0)
        }
        else {
            let paramValues = ["password": self.passwordTextField.text!] as [String : Any]

            self.view.isUserInteractionEnabled = false
            self.showActivityLoader()
            
            ServiceManager.sharedInstance.callPostApi(url:apiURl, param_Values: paramValues, requestMethod: "post", callbackSuccess: { (responseData, apiStatusCode) -> Void in

                let responseDic = responseData as! NSDictionary
                if apiStatusCode == 200 {
                    
                    let emailVerify = responseDic["emailVerified"] as! Int
                    
                    self.defaults.removeObject(forKey:UserDefaults.UserDefaultsKeys.userLoginPassword.rawValue)
                    self.defaults.setUserLoginPassword(value: self.passwordTextField.text!)
                    
                    if emailVerify == 0 {
                        let KYCVC = PrimaryKYCVC()
                        KYCVC.userLoginData = responseDic
                        self.navigationController?.pushViewController(KYCVC, animated: true)
                    }
                    else {
                        self.defaults.removeObject(forKey:UserDefaults.UserDefaultsKeys.isLoggedIn.rawValue)
                        
                        self.defaults.removeObject(forKey:UserDefaults.UserDefaultsKeys.userLoginData.rawValue)
                        self.defaults.setUserLoginData(value: responseDic)
                        self.defaults.setUserLogIn(value: "true")
                        
                        let mainStroyBoard = UIStoryboard(name: "Main", bundle: nil)
                        let tabBarVC = mainStroyBoard.instantiateViewController(withIdentifier: "MainTabBarID") as! MainTabBarViewController
                        self.navigationController?.pushViewController(tabBarVC, animated: true)
                    }
                }
                else {
                    let mesgString = responseDic["message"] as! String
                    self.showToast(message: mesgString, duration: 2.0)
                }
                self.hideActivityLoader()
                self.view.isUserInteractionEnabled = true
                
            },callbackFaliure: { (error:String, message:String) -> Void in
                self.hideActivityLoader()
                self.view.isUserInteractionEnabled = true
                self.showToast(message: message, duration: 2.0)
            })
        }
    }
    
}
