//
//  SignUpVCVC.swift
//  PawrApp
//
//  Created by Ayush Bansal on 10/03/21.
//

import UIKit

@available(iOS 13.0, *)
class SignUpVCVC: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var mobileHeadLbl: UILabel!
    @IBOutlet weak var mobileCodeLbl: UILabel!
    @IBOutlet weak var mobileTextField: UITextField!
    @IBOutlet weak var mobileLine: UIView!
    @IBOutlet weak var mesgTextView: UITextView!
    
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var nextButtonHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var nextButtonWidthConstraint: NSLayoutConstraint!
    
    var screenHeight : CGFloat = 0
    var enteredMobileNo = ""
    var forgotPassword = 0
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        screenHeight = Utilities.getScreenHeight()
        
        mobileTextField.delegate = self
        mobileTextField.keyboardType = .numberPad
        mobileTextField.text = enteredMobileNo
        
        pageDesignLayout()
    }

    //TODO :- Func to set page design layout
    func pageDesignLayout() {
        backBtn.backgroundColor = UIColor.white
        backBtn.layer.cornerRadius = backBtn.frame.height/2
        backBtn.layer.masksToBounds = true
        Utilities.addShadowWithRadius(targetView: backBtn, widthValue: 0.0, HeightValue: 0.0, shadowOpacity: 0.3, radiusValue: 1.75, colorName: UIColor.black)
        
        mobileHeadLbl.text = "Enter your Mobile Number"
        mobileLine.backgroundColor = customColor.TEXT_DARK_GRAY_COLOR
        
        pageDesign.textFieldLayout(to: mobileTextField, textColor: UIColor.black, font_Size: FontSize.HEADINGFONT20, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
        nextButton.backgroundColor = customColor.NEW_THEME_GREEN_COLOR
        
        mobileCodeLbl.text = "+91"
        pageDesign.labelLayout(to: mobileCodeLbl, textColor: UIColor.black, font_Size: FontSize.HEADINGFONT20, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
        
        if screenHeight > 667 {
            pageDesign.labelLayout(to: mobileHeadLbl, textColor: UIColor.black, font_Size: FontSize.HEADINGFONT15, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
            nextButtonHeightConstraint.constant = 70
            nextButtonWidthConstraint.constant = 70
            
            self.nextButton.layer.cornerRadius = 35
        }
        else {
            pageDesign.labelLayout(to: mobileHeadLbl, textColor: UIColor.black, font_Size: FontSize.HEADINGFONT14, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
            
            nextButtonHeightConstraint.constant = 50
            nextButtonWidthConstraint.constant = 50
            
            self.nextButton.layer.cornerRadius = 25
        }
        
        self.nextButton.layer.masksToBounds = true
        Utilities.addShadowWithRadius(targetView: nextButton, widthValue: 0.0, HeightValue: 0.0, shadowOpacity: 0.3, radiusValue: 2.0, colorName: UIColor.black)
        
        initTOSandPPLinks()
    }
    
    //Bottom text links
    private func initTOSandPPLinks() {
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = .center
        let attributes = [NSAttributedString.Key.foregroundColor : customColor.TEXT_DARK_GRAY_COLOR, .paragraphStyle: paragraph, NSAttributedString.Key.font : UIFont(name: TextFontStyle.MONTSERRAT_SEMIBOLD, size: CGFloat(FontSize.HEADINGFONT13))];
        
        let attributedString = NSMutableAttributedString(string: "By signing up, I confirm that I am at least 15 years old, and that I have read and agreed to Zypp's T&Cs and Privacy Policy.", attributes: attributes as [NSAttributedString.Key : Any])
        let tosRange = (attributedString.string as NSString).range(of: "T&Cs".localized)
        let ppRange = (attributedString.string as NSString).range(of: "Privacy Policy".localized)
        
        attributedString.addAttribute(.link, value: "Https://www.Google.com", range: tosRange)
        attributedString.addAttribute(.link, value: "Https://www.Google.com", range: ppRange)
        
        mesgTextView.attributedText = attributedString
    }
    
    // TODO :- Function to check mobile character count
    func textField(_ textField: UITextField, shouldChangeCharactersIn range:NSRange, replacementString string: String) -> Bool {
        if textField == mobileTextField {
            let currentCharacterCount = textField.text?.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            if newLength == 11 {
              self.view.endEditing(true)
            }
            return  newLength <= 10
        }
        else{
            return true
        }
    }
    
    //TODO :- Func to check text field start editing
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == mobileTextField {
            mobileLine.backgroundColor = customColor.NEW_THEME_GREEN_COLOR
        }
    }
    
    //TODO :- Func to check text field end editing
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == mobileTextField {
            mobileLine.backgroundColor = customColor.TEXT_DARK_GRAY_COLOR
        }
    }
    
    //TODO :- Func to set back btn action
    @IBAction func backBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //TODO :- Func to set next btn action
    @IBAction func nextBtnAction(_ sender: Any) {
        guard mobileTextField.text != "" else {
            self.showToast(message: customMessages.ENTER_MOBILE_NO, duration: 2.0)
            return
        }
        
        let Base_URL = Utilities.returnBaseURL(liveURL: liveApi)
        
        var passwordFlag = false
        if forgotPassword == 0 {
            passwordFlag = false
        }
        else {
            passwordFlag = true
        }
        let apiURl = Base_URL + ApiList.GENERATE_OTP + "?mobile=\(mobileTextField.text!)&forgotPwdFlag=\(passwordFlag)"
        
        ServiceManager.sharedInstance.callApi(url:apiURl, param_Values: nil, requestMethod: "get", callbackSuccess: { (responseData, apiStatusCode) -> Void in

            let responseDic = responseData as! NSDictionary
            
            if apiStatusCode == 200 {
                let verify_OTPVC = VerifyOtpVC()
                verify_OTPVC.enteredMobileNo = self.mobileTextField.text!
                verify_OTPVC.forgotPassword = self.forgotPassword
                self.navigationController?.pushViewController(verify_OTPVC, animated: true)
            }
            else {
                let mesgString = responseDic["message"] as! String
                self.showToast(message: mesgString, duration: 2.0)
            }
            
        },callbackFaliure: { (error:String, message:String) -> Void in
            self.showToast(message: message, duration: 2.0)
        })
    }
    
}
