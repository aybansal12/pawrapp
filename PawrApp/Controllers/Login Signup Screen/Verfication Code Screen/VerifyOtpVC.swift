//
//  VerifyOtpVC.swift
//  PawrApp
//
//  Created by Ayush Bansal on 31/03/21.
//

import UIKit
import MBCircularProgressBar

@available(iOS 13.0, *)
class VerifyOtpVC: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var topMesgLbl: UILabel!
    @IBOutlet weak var enteredMobileLbl: UILabel!
    
    @IBOutlet weak var otpTextField: UITextField!
    @IBOutlet weak var resendBtn: UIButton!

    @IBOutlet weak var bottomMesgLbl: UILabel!
    @IBOutlet weak var progressBarView: MBCircularProgressBarView!
    @IBOutlet weak var verifyOtpBtn: UIButton!
    
    var enteredMobileNo = ""
    var countdownTimer: Timer!
    var totalTime = 59
    var forgotPassword = 0
    
    let defaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()

        otpTextField.delegate = self
        otpTextField.addTarget(self, action: #selector(textFieldEdidtingDidChange(_ :)), for: UIControl.Event.editingChanged)

        resendBtn.isUserInteractionEnabled = false
        progressBarView.value = 59
        progressBarView.unitString = " sec"
        
        reSentOtpApi()
        pageDesignLayout()
        setTextFieldSpacing()
    }
    
    //TODO :- Func to set page design layout
    func pageDesignLayout() {
        backBtn.backgroundColor = UIColor.white
        backBtn.layer.cornerRadius = backBtn.frame.height/2
        backBtn.layer.masksToBounds = true
        Utilities.addShadowWithRadius(targetView: backBtn, widthValue: 0.0, HeightValue: 0.0, shadowOpacity: 0.3, radiusValue: 1.75, colorName: UIColor.black)
        
        topMesgLbl.text = "Please enter the verificaion code sent to"
        pageDesign.labelLayout(to: topMesgLbl, textColor: customColor.LIGHT_BLACK_COLOR, font_Size: FontSize.HEADINGFONT15, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
        
        enteredMobileLbl.text = "+91 \(enteredMobileNo)"
        pageDesign.labelLayout(to: enteredMobileLbl, textColor: customColor.NEW_THEME_GREEN_COLOR, font_Size: FontSize.HEADINGFONT20, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
        
        verifyOtpBtn.layer.cornerRadius = 20.0
        verifyOtpBtn.layer.masksToBounds = true
        verifyOtpBtn.backgroundColor = customColor.NEW_THEME_GREEN_COLOR
        
        otpTextField.layer.borderWidth = 1.5
        otpTextField.layer.borderColor = customColor.TEXT_DARK_GRAY_COLOR.cgColor
        otpTextField.layer.cornerRadius = 10.0
        otpTextField.layer.masksToBounds = true
        
        pageDesign.textFieldLayout(to: otpTextField, textColor: UIColor.black, font_Size: FontSize.HEADINGFONT18, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
        
        resendBtn.setTitle("RESEND", for: .normal)
        resendBtn.setTitleColor(customColor.TEXT_DARK_GRAY_COLOR, for: .normal)
        resendBtn.titleLabel?.font = UIFont(name: TextFontStyle.MONTSERRAT_SEMIBOLD, size: CGFloat(FontSize.HEADINGFONT14))
        
        bottomMesgLbl.text = "Didn't receive a text. Please wait"
        pageDesign.labelLayout(to: bottomMesgLbl, textColor: UIColor.black, font_Size: FontSize.HEADINGFONT12, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
        
        progressBarView.valueFontName = TextFontStyle.MONTSERRAT_SEMIBOLD
        progressBarView.valueFontSize = CGFloat(FontSize.HEADINGFONT20)
        
        progressBarView.unitFontName = TextFontStyle.MONTSERRAT_SEMIBOLD
        progressBarView.unitFontSize = CGFloat(FontSize.HEADINGFONT18)
        
        verifyOtpBtn.setTitle("Verify", for: .normal)
        verifyOtpBtn.setTitleColor(UIColor.white, for: .normal)
        verifyOtpBtn.titleLabel?.font = UIFont(name: TextFontStyle.MONTSERRAT_SEMIBOLD, size: CGFloat(FontSize.HEADINGFONT17))
    }
    
    //TODO :- Func to set back btn action
    @IBAction func backBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //TODO :- Func to set otp to the user mobile number
    func reSentOtpApi() {
        let Base_URL = Utilities.returnBaseURL(liveURL: liveApi)
        
        var passwordFlag = false
        if forgotPassword == 0 {
            passwordFlag = false
        }
        else {
            passwordFlag = true
        }
        let apiURl = Base_URL + ApiList.GENERATE_OTP + "?mobile=\(enteredMobileNo)&forgotPwdFlag=\(passwordFlag)"

        ServiceManager.sharedInstance.callApi(url:apiURl, param_Values: nil, requestMethod: "get", callbackSuccess: { (responseData, apiStatusCode) -> Void in

            let responseDic = responseData as! NSDictionary
            
            if apiStatusCode == 200 {
                self.startTimer()
            }
            else {
                let mesgString = responseDic["message"] as! String
                self.showToast(message: mesgString, duration: 2.0)
            }
            
        },callbackFaliure: { (error:String, message:String) -> Void in
            self.showToast(message: message, duration: 2.0)
        })
    }
    
    //TODO :- Func to set text field spacing
    func setTextFieldSpacing() {
        //spacing for placeholder
        let attributedString = NSMutableAttributedString(string: otpTextField.placeholder!)
        attributedString.addAttribute(NSAttributedString.Key.kern, value: CGFloat(6.0), range: NSRange(location: 0, length: attributedString.length))
        otpTextField.attributedPlaceholder = attributedString
    }
    
    // TODO :- Function to check otp character count
    func textField(_ textField: UITextField, shouldChangeCharactersIn range:NSRange, replacementString string: String) -> Bool {
        if textField == otpTextField {
            let currentCharacterCount = textField.text?.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            if newLength == 7 {
              self.view.endEditing(true)
            }
            return  newLength <= 6
        }
        else{
            return true
        }
    }
    
    @objc func textFieldEdidtingDidChange(_ textField :UITextField) {
        let attributedString = NSMutableAttributedString(string: textField.text!)
        attributedString.addAttribute(NSAttributedString.Key.kern, value: CGFloat(6.0), range: NSRange(location: 0, length: attributedString.length))
        textField.attributedText = attributedString
    }
    
    //TODO :- Func to set resend btn action
    @IBAction func resendBtnAction(_ sender: Any) {
        resendBtn.isUserInteractionEnabled = false
        resendBtn.setTitleColor(customColor.TEXT_DARK_GRAY_COLOR, for: .normal)
        reSentOtpApi()
    }
    
    // function to start timmer
    func startTimer() {
        countdownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
    }
    
    @objc func updateTime() {
        let value = "\(timeFormatted(totalTime))"
        //otpView?.timeLbl.text = "\(value)"
        
        let stringValue = NumberFormatter().number(from: value)
        let floatValue = CGFloat(truncating: stringValue!)
        progressBarView.value = floatValue
        
        if totalTime != 0 {
            totalTime -= 1
        } else {
            endTimer()
        }
    }
    
    func endTimer() {
        resendBtn.isUserInteractionEnabled = true
        resendBtn.setTitleColor(customColor.NEW_THEME_GREEN_COLOR, for: .normal)
        totalTime = 59
        countdownTimer.invalidate()
    }
    
    func timeFormatted(_ totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
        return String(format: "%02d", seconds)
    }
    
    //TODO :- Func to set verify otp btn action
    @IBAction func verifyOtpBtnAction(_ sender: Any) {
        guard otpTextField.text != "" else {
            self.showToast(message: customMessages.Enter_OTP_VALUE, duration: 2.0)
            return
        }
        
        let Base_URL = Utilities.returnBaseURL(liveURL: liveApi)
        let apiURl = Base_URL + ApiList.VERIFY_ENTER_OTP + "?referrerId=-1"
        
        let paramValues = ["username": self.enteredMobileNo, "mobile": self.enteredMobileNo, "otp": self.otpTextField.text!] as [String : Any]

        self.view.isUserInteractionEnabled = false
        self.showActivityLoader()
        ServiceManager.sharedInstance.callPostApi(url:apiURl, param_Values: paramValues, requestMethod: "post", callbackSuccess: { (responseData, apiStatusCode) -> Void in

            let responseDic = responseData as! NSDictionary
            
            if (apiStatusCode == 200) {
                self.defaults.removeObject(forKey:UserDefaults.UserDefaultsKeys.userLoginData.rawValue)
                self.defaults.setUserLoginData(value: responseDic)
                
                let passwordVC = SetPasswordVC()
                passwordVC.userLoginData = responseDic
                passwordVC.forgotPassword = self.forgotPassword
                passwordVC.enteredMobile_No = self.enteredMobileNo
                self.navigationController?.pushViewController(passwordVC, animated: true)
            }
            else {
                let mesgString = responseDic["message"] as! String
                self.showToast(message: mesgString, duration: 2.0)
            }
            self.hideActivityLoader()
            self.view.isUserInteractionEnabled = true
            
        },callbackFaliure: { (error:String, message:String) -> Void in
            self.hideActivityLoader()
            self.view.isUserInteractionEnabled = true
            self.showToast(message: message, duration: 2.0)
        })
    }
    
}
