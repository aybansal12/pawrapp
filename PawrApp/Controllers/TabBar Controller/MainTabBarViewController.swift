//
//  MainTabBarViewController.swift
//  PawrApp
//
//  Created by Ayush Bansal on 09/03/21.
//

import UIKit
import CoreLocation

@available(iOS 13.0, *)
class MainTabBarViewController: UITabBarController, UITabBarControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.delegate = self
        
        UITabBar.appearance().tintColor = customColor.NEW_THEME_GREEN_COLOR
        UITabBar.appearance().unselectedItemTintColor = UIColor.black
        
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: TextFontStyle.MONTSERRAT_SEMIBOLD, size: CGFloat(FontSize.HEADINGFONT13))!], for: .normal)
//        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: TextFontStyle.MONTSERRAT_SEMIBOLD, size: CGFloat(FontSize.HEADINGFONT15))!], for: .highlighted)
//        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: TextFontStyle.MONTSERRAT_SEMIBOLD, size: CGFloat(FontSize.HEADINGFONT20))!], for: .focused)
        
        setTabBarItems()
    }
    
    func setTabBarItems(){
        let myTabBarItem1 = (self.tabBar.items?[0])! as UITabBarItem
        let myTabBarItem2 = (self.tabBar.items?[1])! as UITabBarItem
        
        if Utilities.getScreenHeight() < 736 {
            myTabBarItem1.image = UIImage(named: "batterySwap")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
            myTabBarItem1.selectedImage = UIImage(named: "greenBatterySwap")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
            
            myTabBarItem2.image = UIImage(named: "moreTabIcon")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
            myTabBarItem2.selectedImage = UIImage(named: "morefocus")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
            
        }
        myTabBarItem1.imageInsets = UIEdgeInsets(top: 0, left: 0, bottom: 2, right: 0)
        //myTabBarItem2.imageInsets = UIEdgeInsets(top: 0, left: 0, bottom: 2, right: 0)
    }
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        
        if (viewController is HomeScreenVC || viewController is ChargingStationVC) {
            guard  Utilities.checkLocationPermissions() == true else {
                
                Utilities.showAlertControllerAction(message: "Please allow location permission", title: "Location Access Disabled!", styleType: "alert", onView: self, andFirstButtonTitle: "Cancel", andSecondButtonTitle:"OK", andFirstAction: nil, andSecondAction: #selector(self.allowLocationPermission))
                
                return false
            }
            return true
        }
        else {
            return true
        }
    }
    
    // TODO :- function to allow location permission
    @objc func allowLocationPermission(){
        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
            return
        }
        if UIApplication.shared.canOpenURL(settingsUrl) {
            UIApplication.shared.open(settingsUrl, completionHandler: { (success) in })
        }
    }
}
