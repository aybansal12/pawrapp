//
//  HomeScreenVC.swift
//  PawrApp
//
//  Created by Ayush Bansal on 09/03/21.
//

import UIKit
import CoreLocation
import RxSwift

@available(iOS 13.0, *)
class HomeScreenVC: UIViewController, CLLocationManagerDelegate {

    @IBOutlet weak var dataContainerView: UIView!
    
    let locationManager = CLLocationManager()
    var userCurrentLatitude : Double = 0
    var userCurrentLongitude : Double = 0

    var batterySwapListModel = [newSwappingStationModel]()
    
    var polyLineEnable : Bool!
    //var batterySwapObserved = BehaviorSubject.from(optional: [newChargingStationModel].self)
    
    var swapBatteryLatitude : CLLocationDegrees = 0.0
    var swapBatteryLongitude : CLLocationDegrees = 0.0
    
    let disposeBag = DisposeBag()
    var userLoginStatus : Bool!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Utilities.setStatusBarColor(currentView: self.view)
        
        userLoginStatus = UtilitiesValues.getUserLoginStatus()
        getSwappingStation()
        
        DispatchQueue.main.async {
            self.fetchMobiCoinBalance()
        }
        pageDesignLayout()
    }
    
    //TODO :- Func to set page design layout
    func pageDesignLayout() {
        
    }
    
    //TODO :- Func to fetch all charging stations
    func getSwappingStation() {
//        userCurrentLatitude = UtilitiesValues.getUserCurrentLocation().latitude
//        userCurrentLongitude = UtilitiesValues.getUserCurrentLocation().longitude
        
        let Base_URL = Utilities.returnBaseURL(liveURL: liveApi)
        let apiURl = Base_URL + BatterySwap_ModuleApi.FETCH_BATTERY_SWAP_STATION + "?latitude=\(self.userCurrentLatitude)&longitude=\(self.userCurrentLongitude)"

        ServiceManager.sharedInstance.callApi(url:apiURl, param_Values: nil, requestMethod: "get", callbackSuccess: { (responseData, apiStatusCode) -> Void in

            if apiStatusCode == 200 {
                if !Utilities.isEmptyValue(responseData) {
                    
                    let batterySwapListArrayData = (responseData as! NSArray).mutableCopy() as! NSMutableArray
                    self.batterySwapListModel.removeAll()
                    self.batterySwapListModel = batterySwap_Module.BatterySwapListData(dataArray: batterySwapListArrayData)
                    stationListSingleton.shared.stationListDetails = self.batterySwapListModel
                    
                    self.loadGoogleMap()
                        //self.newsList.onNext(responseObj as! PublishSubject<newChargingStationModel>.Element)
//                            self.batterySwapObserved.asObservable().map(responseObj as! ([newChargingStationModel].Type) throws -> Any).subscribe(onNext: { print($0) }).disposed(by: self.disposeBag)
            
   
//                        do {
//                            let albums = try JSONDecoder().decode([chargingStationModel].self, from: responseData as! Data)
//                            //observer.onNext(albums)
//                        } catch {
//                           // observer.onError(error)
//                        }
                    
                    //self.batterySwappingArray = (responseData as! NSArray).mutableCopy() as! NSMutableArray
                }
                else {
                    self.loadGoogleMap()
                }
            }
            else if apiStatusCode == 401 {
                self.loadGoogleMap()
            }
           
        },callbackFaliure: { (error:String, message:String) -> Void in
            self.loadGoogleMap()
            self.showToast(message: message, duration: 2.0)
        })
    }
    
    //TODO :- Func to load google map view
    func loadGoogleMap() {
        let vcObj = GoogleMapVC()
        vcObj.stationType = 0
        addViewController(vcObj)
    }
    
    func addViewController(_ childController: UIViewController) {
        childController.view.frame = self.dataContainerView.bounds;
        self.addChild(childController)
        self.dataContainerView.addSubview(childController.view)
        childController.didMove(toParent: self)
    }
}
