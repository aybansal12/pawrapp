//
//  PaytmOtpVerificationVC.swift
//  PawrApp
//
//  Created by Ayush Bansal on 19/04/21.
//

import UIKit

class PaytmOtpVerificationVC: UIViewController, navBakButtonWithPressed, UITextFieldDelegate {

    @IBOutlet weak var navHeaderBarView: UIView!
    
    @IBOutlet weak var paymentImageView: UIImageView!
    @IBOutlet weak var otpTextField: UITextField!
    @IBOutlet weak var resendOtpBtn: UIButton!
    @IBOutlet weak var submitBtn: UIButton!
    
    var defaults = UserDefaults.standard
    var navBarView : NavBackView?
    var paymentWallet : String!
    var selected_Module : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navBarView = Bundle.main.loadNibNamed("NavBackView", owner: self, options: nil)?.first as? NavBackView
        navBarView?.navBackBtnDelegates = self
        navBarView?.navTitleLabel.text = "OTP Verification"
        navBarView?.navTitleLabel.textColor = UIColor.black
        
        navBarView?.navBackButton.backgroundColor = UIColor.white
        navBarView?.navBackButton.setImage(UIImage(named: "filledBackIcon"), for: .normal)
        navBarView?.navBackButton.layer.cornerRadius = (navBarView?.navBackButton.frame.height)!/2
        navBarView?.navBackButton.layer.masksToBounds = true
        Utilities.addShadowWithRadius(targetView: (navBarView?.navBackButton)!, widthValue: 0.0, HeightValue: 0.0, shadowOpacity: 0.3, radiusValue: 2.5, colorName: UIColor.black)
        
        navBarView?.mainContentView.frame = navHeaderBarView.frame
        navBarView?.mainContentView.backgroundColor = UIColor.white
        
        self.navHeaderBarView.addSubview(navBarView!)
        
        otpTextField.delegate = self
        otpTextField.addTarget(self, action: #selector(textFieldEdidtingDidChange(_ :)), for: UIControl.Event.editingChanged)
        
        if paymentWallet == paymentTypeConstant.PAYMENT_TYPE_PAYTM {
            paymentImageView.image = UIImage(named: "Paytmlogo")
        }
        else {
            paymentImageView.image = UIImage(named: "Mobikwik")
        }
        pageDesignLayout()
    }
    
    //TODO :- Func to set page design layout
    func pageDesignLayout() {
        otpTextField.layer.borderWidth = 1.5
        otpTextField.layer.borderColor = customColor.TEXT_DARK_GRAY_COLOR.cgColor
        otpTextField.layer.cornerRadius = 10.0
        otpTextField.layer.masksToBounds = true
        
        pageDesign.textFieldLayout(to: otpTextField, textColor: UIColor.black, font_Size: FontSize.HEADINGFONT18, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
        
        resendOtpBtn.setTitle("Resend OTP", for: .normal)
        pageDesign.buttonLayout(to: resendOtpBtn, textColor: customColor.NEW_THEME_GREEN_COLOR, font_Size: FontSize.HEADINGFONT14, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD, bgColor: UIColor.clear)
        
        submitBtn.layer.cornerRadius = 5.0
        submitBtn.layer.masksToBounds = true
        submitBtn.setTitle("Submit", for: .normal)
        
        pageDesign.buttonLayout(to: submitBtn, textColor: UIColor.white, font_Size: FontSize.HEADINGFONT16, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD, bgColor: customColor.NEW_THEME_GREEN_COLOR)
    }

    //TODO :- Did Press Back Button
    func didPressButton(button: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    // TODO :- Function to check otp character count
    func textField(_ textField: UITextField, shouldChangeCharactersIn range:NSRange, replacementString string: String) -> Bool {
        if textField == otpTextField {
            let currentCharacterCount = textField.text?.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            if newLength == 7 {
              self.view.endEditing(true)
            }
            return  newLength <= 6
        }
        else{
            return true
        }
    }
    
    @objc func textFieldEdidtingDidChange(_ textField :UITextField) {
        let attributedString = NSMutableAttributedString(string: textField.text!)
        attributedString.addAttribute(NSAttributedString.Key.kern, value: CGFloat(6.0), range: NSRange(location: 0, length: attributedString.length))
        textField.attributedText = attributedString
    }
    
    //TODO :- Func to set resend otp btn action
    @IBAction func resendOtpBtnAction(_ sender: Any) {
        self.sentOtpAction(paymentType: paymentTypeConstant.PAYMENT_TYPE_PAYTM)
    }
    
    //TODO :- Func to set send otp btn action
    func sentOtpAction(paymentType: String) {
        let Base_URL = Utilities.returnBaseURL(liveURL: liveApi)
        let apiURl = Base_URL + PaymentApi.GENERATE_WALLET_OTP_URL + "?selectedWallet=\(paymentType)"

        self.view.isUserInteractionEnabled = false
        self.showActivityLoader()
        ServiceManager.sharedInstance.callApi(url:apiURl, param_Values: nil, requestMethod: "post", callbackSuccess: { (responseData, apiStatusCode) -> Void in

            if apiStatusCode == 200 {
                if !Utilities.isEmptyValue(responseData) {
                    let responseDic = responseData as! NSDictionary
                    let statsuCode = responseDic["code"] as! Int
                    
                    if statsuCode == 2000 {
                        let statsuMesg = responseDic["message"] as! String
                        self.showToast(message: statsuMesg, duration: 2.0)
                    }
                    else if statsuCode == paymentTypeConstant.OTP_SEND_FAIL {
                        let statsuMesg = responseDic["message"] as! String
                        self.showToast(message: statsuMesg, duration: 2.0)
                    }
                }
            }
            else if apiStatusCode == 401 {
                let responseDic = responseData as! NSDictionary
                let statusCode =  responseDic["code"] as! Int
                if statusCode == constantStatusCode.NOT_REGISTERED_USER {
                    let mesgString = responseDic["message"] as! String
                    self.showToast(message: mesgString, duration: 2.0)
                }
            }
            self.view.isUserInteractionEnabled = true
            self.hideActivityLoader()
        },
        callbackFaliure: { (error:String, message:String) -> Void in
            self.view.isUserInteractionEnabled = true
            self.hideActivityLoader()
            self.showToast(message: message, duration: 2.0)
        })
    }
    
    //TODO :- Func to set submit btn action
    @IBAction func submitBtnAction(_ sender: Any) {
        guard otpTextField.text != "" else {
            self.showToast(message: customMessages.Enter_OTP_VALUE, duration: 2.0)
            return
        }
        guard otpTextField.text?.count == 6 else {
            self.showToast(message: customMessages.Enter_OTP_VALUE, duration: 2.0)
            return
        }
        verifyOtpApi()
    }
    
    //TODO :- Func to verify otp
    func verifyOtpApi() {
        let Base_URL = Utilities.returnOld_BASEURL(liveURL: liveApi)
        let apiURl = Base_URL + PaymentApi.VALIDATE_WALLET_OTP_URL + "?otp=\(otpTextField.text!)&selectedWallet=\(paymentWallet!)"

        self.view.isUserInteractionEnabled = false
        self.showActivityLoader()
        ServiceManager.sharedInstance.callApi(url:apiURl, param_Values: nil, requestMethod: "post", callbackSuccess: { (responseData, apiStatusCode) -> Void in

            if apiStatusCode == 200 {
                if !Utilities.isEmptyValue(responseData) {
                    let responseDic = responseData as! NSDictionary
                    let statsuCode = responseDic["code"] as! Int
                    
                    if statsuCode == 2000 {
                        self.defaults.removeObject(forKey:UserDefaults.UserDefaultsKeys.paymentLinkedWalletData.rawValue)
                        self.defaults.setUserPaymentLinkWallet(value: self.paymentWallet)
                        
                        let viewControllers: [UIViewController] = self.navigationController!.viewControllers
                        if self.selected_Module == "MoreMenu" {
                            self.navigationController!.popToViewController(viewControllers[viewControllers.count - 2], animated: true)
                        }
                        else {
                            var userInfoDic : [String: String]!
                            if self.paymentWallet == paymentTypeConstant.PAYMENT_TYPE_PAYTM {
                                userInfoDic = ["userPaymentType": "PayTm", "userPaymentString": ""]
                            }
                            else {
                                userInfoDic = ["userPaymentType": "Mobikwik", "userPaymentString": ""]
                            }
                            NotificationCenter.default.post(name: NSNotification.Name("Payment_Linked_Action"), object: userInfoDic)
                            self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: true)
                        }
                    }
                }
            }
            else if apiStatusCode == 401 {
                let responseDic = responseData as! NSDictionary
                let statusCode =  responseDic["code"] as! Int
                if statusCode == constantStatusCode.NOT_REGISTERED_USER {
                    let mesgString = responseDic["message"] as! String
                    self.showToast(message: mesgString, duration: 2.0)
                }
            }
            self.view.isUserInteractionEnabled = true
            self.hideActivityLoader()
        },
        callbackFaliure: { (error:String, message:String) -> Void in
            self.view.isUserInteractionEnabled = true
            self.hideActivityLoader()
            self.showToast(message: message, duration: 2.0)
        })
    }
}
