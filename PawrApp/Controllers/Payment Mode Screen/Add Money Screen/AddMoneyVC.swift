//
//  AddMoneyVC.swift
//  PawrApp
//
//  Created by Ayush Bansal on 21/04/21.
//

import UIKit

class AddMoneyVC: UIViewController, navBakButtonWithPressed {
    
    @IBOutlet weak var navHeaderBarView: UIView!
    @IBOutlet weak var walletAmountLbl: UILabel!
    
    @IBOutlet weak var paymentDetailsView: UIView!
    @IBOutlet weak var paymentHeadLbl: UILabel!
    @IBOutlet weak var paymentMesgLbl: UILabel!
    
    @IBOutlet weak var addMoneyBtn: UIButton!
    
    var navBarView : NavBackView?
    var paymentMethod : String!
    var addAmount : Int!
    var moduleName : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navBarView = Bundle.main.loadNibNamed("NavBackView", owner: self, options: nil)?.first as? NavBackView
        navBarView?.navBackBtnDelegates = self
        navBarView?.navTitleLabel.text = "Add Money"
        navBarView?.navTitleLabel.textColor = UIColor.black
        
        navBarView?.navBackButton.backgroundColor = UIColor.white
        navBarView?.navBackButton.setImage(UIImage(named: "filledBackIcon"), for: .normal)
        navBarView?.navBackButton.layer.cornerRadius = (navBarView?.navBackButton.frame.height)!/2
        navBarView?.navBackButton.layer.masksToBounds = true
        Utilities.addShadowWithRadius(targetView: (navBarView?.navBackButton)!, widthValue: 0.0, HeightValue: 0.0, shadowOpacity: 0.3, radiusValue: 2.5, colorName: UIColor.black)
        
        navBarView?.mainContentView.frame = navHeaderBarView.frame
        navBarView?.mainContentView.backgroundColor = UIColor.clear
        
        self.navHeaderBarView.addSubview(navBarView!)
        
        setPageData()
        pageDesignLayout()
    }
    
    //TODO :- Did Press Back Button
    func didPressButton(button: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //TODO :- Func to set page design layout
    func pageDesignLayout(){
        
        addMoneyBtn.layer.cornerRadius = 5.0
        addMoneyBtn.layer.masksToBounds = true
        
        addMoneyBtn.setTitle("Add Money", for: .normal)
        pageDesign.buttonLayout(to: addMoneyBtn, textColor: UIColor.white, font_Size: FontSize.HEADINGFONT16, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD, bgColor: customColor.NEW_THEME_GREEN_COLOR)
        
        paymentDetailsView.layer.cornerRadius = 10.0
        paymentDetailsView.layer.masksToBounds = true
        Utilities.addShadowWithRadius(targetView: paymentDetailsView, widthValue: 0.0, HeightValue: 0.0, shadowOpacity: 0.5, radiusValue: 2.25, colorName: UIColor.black)
        
        paymentHeadLbl.text = "Add Money to wallet"
        pageDesign.labelLayout(to: paymentHeadLbl, textColor: customColor.NEW_THEME_GREEN_COLOR, font_Size: FontSize.HEADINGFONT18, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
        
        pageDesign.labelLayout(to: paymentMesgLbl, textColor: customColor.TEXT_DARK_GRAY_COLOR, font_Size: FontSize.HEADINGFONT14, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
        
        pageDesign.labelLayout(to: walletAmountLbl, textColor: customColor.NEW_THEME_GREEN_COLOR, font_Size: FontSize.HEADINGFONT22, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
    }
    
    //TODO :- Func to set page data
    func setPageData() {
        walletAmountLbl.text = "\u{20B9} \(String(addAmount))"
        
        if paymentMethod! == paymentTypeConstant.PAYMENT_TYPE_PAYTM {
            paymentMesgLbl.text = "Insufficient Balance in PayTM wallet. Add Rs. \(String(addAmount)) to your PayTM Wallet"
        }
        else if paymentMethod! == paymentTypeConstant.PAYMENT_TPYE_MOBIKWIK_WALLET {
            paymentMesgLbl.text = "Insufficient Balance in PayTM wallet. Add Rs. \(String(addAmount)) to your Mobikwik Wallet"
        }
    }

    //TODO :- Func to set add money btn action
    @IBAction func addMoneyBtnAction(_ sender: Any) {
        let paytmMoneyVC = PaytmAddMoneyVC()
        paytmMoneyVC.add_Amount = addAmount
        paytmMoneyVC.payment_Method = paymentMethod
        paytmMoneyVC.module_Name = moduleName
        self.navigationController?.pushViewController(paytmMoneyVC, animated: true)
    }
    
}
