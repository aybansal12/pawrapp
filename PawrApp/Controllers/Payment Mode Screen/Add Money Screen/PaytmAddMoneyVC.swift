//
//  PaytmAddMoneyVC.swift
//  PawrApp
//
//  Created by Ayush Bansal on 21/04/21.
//

import UIKit
import WebKit

class PaytmAddMoneyVC: UIViewController, navBakButtonWithPressed, WKUIDelegate, WKNavigationDelegate {
    
    @IBOutlet weak var navHeaderBarView: UIView!
    @IBOutlet weak var paytmWebView: WKWebView!
    
    let defaults = UserDefaults.standard
    var navBarView : NavBackView?
    
    var add_Amount : Int!
    var payment_Method : String!
    var module_Name : String!
    var userID : Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navBarView = Bundle.main.loadNibNamed("NavBackView", owner: self, options: nil)?.first as? NavBackView
        navBarView?.navBackBtnDelegates = self
        navBarView?.navTitleLabel.text = "Add Money"
        navBarView?.navTitleLabel.textColor = UIColor.black
        
        navBarView?.navBackButton.backgroundColor = UIColor.white
        navBarView?.navBackButton.setImage(UIImage(named: "filledBackIcon"), for: .normal)
        navBarView?.navBackButton.layer.cornerRadius = (navBarView?.navBackButton.frame.height)!/2
        navBarView?.navBackButton.layer.masksToBounds = true
        Utilities.addShadowWithRadius(targetView: (navBarView?.navBackButton)!, widthValue: 0.0, HeightValue: 0.0, shadowOpacity: 0.3, radiusValue: 2.5, colorName: UIColor.black)
        
        navBarView?.mainContentView.frame = navHeaderBarView.frame
        navBarView?.mainContentView.backgroundColor = UIColor.clear
        self.navHeaderBarView.addSubview(navBarView!)
        
        paytmWebView.uiDelegate = self
        paytmWebView.navigationDelegate = self
        paytmWebView.scrollView.bounces = false
        paytmWebView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        if (defaults.object(forKey:UserDefaults.UserDefaultsKeys.userLoginData.rawValue) != nil) {
            let userLoginData = defaults.getUserLoginData() as NSDictionary
            
            if userLoginData.count > 0 {
                self.userID = userLoginData["userId"] as! Int
            }
        }
        
        let Base_URL = Utilities.returnPayment_BaseURL(liveURL: liveApi)
        let urlString = Base_URL + PaymentApi.PAYTM_WALLET_ADD_MONEY + "?userId=\(String(userID))&amount=\(String(add_Amount))"
        
        if let url = URL(string: urlString) {
            let requestObj = URLRequest(url: url)
            paytmWebView.load(requestObj)
        }
    }

    //TODO :- Did Press Back Button
    func didPressButton(button: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        
        if let url = navigationAction.request.url {
            if url.absoluteString.contains(PaymentApi.PAYTM_ADD_MONEY_SUCCESS) {
                if self.module_Name == "BatterySwap" {
                    
                    self.showToast(message: "Payment added successfully", duration: 2.0)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.50) {
                        let viewControllers: [UIViewController] = self.navigationController!.viewControllers
                        self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: true)
                    }
                }
                print("Payment Success")
            }
            else if url.absoluteString.contains(PaymentApi.PAYTM_BROWSER_FAILURE_URL){
                print("Payment failed")
            }
            else {
                print("Unknown Error")
            }
        }
        decisionHandler(.allow)
    }
}
