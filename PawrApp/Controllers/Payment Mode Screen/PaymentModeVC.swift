//
//  PaymentModeVC.swift
//  PawrApp
//
//  Created by Ayush Bansal on 15/04/21.
//

import UIKit

protocol SetSelectedPaymentProtocol {
    func setSelectedPayment(paymentString: String)
}

@available(iOS 13.0, *)
class PaymentModeVC: UIViewController, navBakButtonWithPressed, UITableViewDelegate, UITableViewDataSource, switchSelectionProtocol {
    
    @IBOutlet weak var navHeaderBarView: UIView!
    @IBOutlet weak var paymentModeTableView: UITableView!
    
    @IBOutlet weak var zyppCoinDetailsView: UIView!
    
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var zyppNameLbl: UILabel!
    @IBOutlet weak var zyppCoinHeadLbl: UILabel!
    @IBOutlet weak var coinBalanceLbl: UILabel!
    
    @IBOutlet weak var addCoinBtn: UIButton!
    
    let defaults = UserDefaults.standard
    var navBarView : NavBackView?
    
    var paymentListArray = NSMutableArray()
    var SetSelectedPaymentDelegate : SetSelectedPaymentProtocol!
    
    var defaultPaymentArray = NSMutableArray()
    var linkedPaymentArray = NSMutableArray()
    var remainingPaymentArray = NSMutableArray()
    var sectionCount = 3
    
    var screenWidth: CGFloat = 0
    var screenHeight : CGFloat = 0
    var selectedModule : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        screenWidth = Utilities.getScreenWidth()
        screenHeight = Utilities.getScreenHeight()

        navBarView = Bundle.main.loadNibNamed("NavBackView", owner: self, options: nil)?.first as? NavBackView
        navBarView?.navBackBtnDelegates = self
        navBarView?.navTitleLabel.text = "Payment Option"
        navBarView?.navTitleLabel.textColor = UIColor.black
        
        navBarView?.navBackButton.backgroundColor = UIColor.white
        navBarView?.navBackButton.setImage(UIImage(named: "filledBackIcon"), for: .normal)
        navBarView?.navBackButton.layer.cornerRadius = (navBarView?.navBackButton.frame.height)!/2
        navBarView?.navBackButton.layer.masksToBounds = true
        Utilities.addShadowWithRadius(targetView: (navBarView?.navBackButton)!, widthValue: 0.0, HeightValue: 0.0, shadowOpacity: 0.3, radiusValue: 2.5, colorName: UIColor.black)
        
        navBarView?.mainContentView.frame = navHeaderBarView.frame
        navBarView?.mainContentView.backgroundColor = UIColor.white
        
        self.navHeaderBarView.addSubview(navBarView!)
        
        paymentModeTableView.dataSource = self
        paymentModeTableView.delegate = self
        paymentModeTableView.separatorStyle = .none
        
        let paymentModeList = UINib(nibName : "PaymentModeTableViewCell" , bundle:nil)
        paymentModeTableView.register(paymentModeList, forCellReuseIdentifier: "PaymentModeTableCell")
        
        DispatchQueue.main.async {
            self.fetchPaymentModes()
        }
        
        pageDesignLayout()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.paymentModeTableView.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        setPageData()
    }

    //TODO :- Did Press Back Button
    func didPressButton(button: UIButton) {
        self.navigationController?.popViewController(animated: true)
//        dismiss(animated: false, completion: nil)
//        dismiss(animated: true) {
//
//        }
    }
    
    //TODO :- Func to set page data
    func setPageData() {
        let zyppCoins = defaults.getUserZyppCoinBalance()
        coinBalanceLbl.text = zyppCoins
    }
    
    //TODO :- Func to set page design layout
    func pageDesignLayout() {
        zyppCoinDetailsView.layer.cornerRadius = 15.0
        zyppCoinDetailsView.layer.masksToBounds = true
        Utilities.addShadowWithRadius(targetView: zyppCoinDetailsView, widthValue: 0.0, HeightValue: 0.0, shadowOpacity: 0.3, radiusValue: 1.75, colorName: UIColor.black)
        //zyppCoinDetailsView.backgroundColor = customColor.NEW_THEME_GREEN_COLOR
        
        userNameLbl.text = UtilitiesValues.getLoginData_ParameterString(parameterName: "name")
        pageDesign.labelLayout(to: userNameLbl, textColor: UIColor.black, font_Size: FontSize.HEADINGFONT15, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
        
        zyppNameLbl.text = "ZYPP"
        pageDesign.labelLayout(to: zyppNameLbl, textColor: customColor.NEW_THEME_GREEN_COLOR, font_Size: FontSize.HEADINGFONT18, fontFamily: TextFontStyle.MONTSERRAT_BOLD)
        
        zyppCoinHeadLbl.text = "Zypp Coin Balance"
        pageDesign.labelLayout(to: zyppCoinHeadLbl, textColor: UIColor.black, font_Size: FontSize.HEADINGFONT15, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
        
        pageDesign.labelLayout(to: coinBalanceLbl, textColor: UIColor.black, font_Size: FontSize.HEADINGFONT18, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
        
        addCoinBtn.layer.cornerRadius = 10.0
        addCoinBtn.layer.masksToBounds = true
        addCoinBtn.setTitle("Add Zypp Coins", for: .normal)
        pageDesign.buttonLayout(to: addCoinBtn, textColor: UIColor.white, font_Size: FontSize.HEADINGFONT13, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD, bgColor: customColor.NEW_THEME_GREEN_COLOR)
    }
    
    //TODO :- Func to fetch payment mode
    func fetchPaymentModes() {
    
        let Base_URL = Utilities.returnBaseURL(liveURL: liveApi)
        let apiURl = Base_URL + PaymentApi.FETCH_PAYMENT_MODE

        self.showActivityLoader()
        ServiceManager.sharedInstance.callApi(url:apiURl, param_Values: nil, requestMethod: "get", callbackSuccess: { (responseData, apiStatusCode) -> Void in
            self.hideActivityLoader()
            
            if apiStatusCode == 200 {
                if !Utilities.isEmptyValue(responseData) {
                    self.defaultPaymentArray.removeAllObjects()
                    self.linkedPaymentArray.removeAllObjects()
                    self.remainingPaymentArray.removeAllObjects()
                    
                    let paymentListArray = (responseData as! NSArray).mutableCopy() as! NSMutableArray
                    
                    for paymentObj in paymentListArray {
                        let indexDataDic = paymentObj as! NSDictionary
                        
                        let linkedStatus = indexDataDic["linkedStatus"] as! Int
                        let defaultStatus = indexDataDic["defaultStatus"] as! Int
                        if defaultStatus != 0 {
                            self.defaultPaymentArray.add(indexDataDic)
                        }
                        else if linkedStatus != 0 {
                            self.linkedPaymentArray.add(indexDataDic)
                        }
                        else {
                            self.remainingPaymentArray.add(indexDataDic)
                        }
                        
//                        self.defaultPaymentArray.add(indexDataDic)
//                        self.linkedPaymentArray.add(indexDataDic)
//                        self.remainingPaymentArray.add(indexDataDic)
                    }
        
                    self.paymentModeTableView.reloadData()
                    self.fetchMobiCoinBalance()
                }
            }
        },callbackFaliure: { (error:String, message:String) -> Void in
            self.hideActivityLoader()
            self.showToast(message: message, duration: 2.0)
        })
    }
    
    //TODO :- Func to set table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionCount
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            if defaultPaymentArray.count > 0 {
                return defaultPaymentArray.count
            }
            else {
                return 0
            }
        }
        else if section == 1 {
            if linkedPaymentArray.count > 0 {
                return linkedPaymentArray.count
            }
            else {
                return 0
            }
        }
        else {
            if remainingPaymentArray.count > 0 {
                return remainingPaymentArray.count
            }
            else {
                return 0
            }
        }
    }
    
    //Height for Header Section
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            if defaultPaymentArray.count > 0 {
                return 50
            }
            else {
                return 0
            }
        }
        else if section == 1 {
            if linkedPaymentArray.count > 0 {
                return 50
            }
            else {
                return 0
            }
        }
        else {
            if remainingPaymentArray.count > 0 {
                return 50
            }
            else {
                return 0
            }
        }
    }
    
    //View for Header Section
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let mainView = UIView(frame: CGRect(x: 0, y:0, width: self.screenWidth, height: self.screenHeight))
        mainView.backgroundColor = customColor.BG_GRAY_COLOR
        
        let mesgLabel = UILabel(frame: CGRect(x: 15, y: 10, width: screenWidth - 30, height: 30))
        if section == 0 {
            mesgLabel.text = "Default Payment Option"
        }
        else if section == 1 {
            mesgLabel.text = "Linked Payment Option"
        }
        else {
            mesgLabel.text = "Available Payment Option"
        }
        pageDesign.labelLayout(to: mesgLabel, textColor: UIColor.black, font_Size: FontSize.HEADINGFONT15, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
        
        mainView.addSubview(mesgLabel)
        return mainView
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = paymentModeTableView.dequeueReusableCell(withIdentifier: "PaymentModeTableCell", for: indexPath as IndexPath) as! PaymentModeTableViewCell
        cell.selectionStyle = .none
        
        var indexDataDic = NSDictionary()
        if indexPath.section == 0 {
            indexDataDic = defaultPaymentArray[indexPath.row] as! NSDictionary
        }
        else if indexPath.section == 1 {
            indexDataDic = linkedPaymentArray[indexPath.row] as! NSDictionary
        }
        else {
            indexDataDic = remainingPaymentArray[indexPath.row] as! NSDictionary
        }
        
        cell.switchSelectionDelegate = self
        cell.setCellData(dataDic : indexDataDic)
        cell.selectedIndex = indexPath.row
        cell.selectedSection = indexPath.section
        cell.paymentSwitch.tag = indexPath.row;
    
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var indexDataDic = NSDictionary()
        
        if indexPath.section == 0 {
            indexDataDic = defaultPaymentArray[indexPath.row] as! NSDictionary
        }
        else if indexPath.section == 1 {
            indexDataDic = linkedPaymentArray[indexPath.row] as! NSDictionary
        }
        else {
            indexDataDic = remainingPaymentArray[indexPath.row] as! NSDictionary
        }

        let defaultPayment = indexDataDic["defaultStatus"] as! Int

        if defaultPayment == 0 {
            let paymentLinked = indexDataDic["linkedStatus"] as! Int
            if paymentLinked == 0 {
                self.showToast(message: paymentCustomMesg.LINKED_PAYMENT_FIRST, duration: 2.0)
            }
            else {
                let paymentMode = indexDataDic["paymentMode"] as! String
                self.setWalletAsDefault(paymentModeString: paymentMode)
            }
        }
        else {
            self.showToast(message: paymentCustomMesg.DEFAULT_PAYMENT_MODE, duration: 2.0)
        }
    }
    
    //TODO :- Func to set payment mode default
    func setWalletAsDefault(paymentModeString: String) {
        let Base_URL = Utilities.returnBaseURL(liveURL: liveApi)
        let apiURl = Base_URL + PaymentApi.SWITCH_PAYMENT_MODE + "?selectedMode=\(paymentModeString)"

        self.view.isUserInteractionEnabled = false
        self.showActivityLoader()
        ServiceManager.sharedInstance.callApi(url:apiURl, param_Values: nil, requestMethod: "post", callbackSuccess: { (responseData, apiStatusCode) -> Void in

            if apiStatusCode == 200 {
                if !Utilities.isEmptyValue(responseData) {
                    let responseDic = responseData as! NSDictionary
                    let statsuCode = responseDic["code"] as! Int
                    
                    if statsuCode == 2000 {
                        self.defaults.removeObject(forKey:UserDefaults.UserDefaultsKeys.paymentLinkedWalletData.rawValue)
                        self.defaults.setUserPaymentLinkWallet(value: paymentModeString)
                        
                        self.SetSelectedPaymentDelegate.setSelectedPayment(paymentString: paymentModeString)
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            }
            else if apiStatusCode == 401 {
                let responseDic = responseData as! NSDictionary
                let statusCode =  responseDic["code"] as! Int
                if statusCode == constantStatusCode.NOT_REGISTERED_USER {
                    let mesgString = responseDic["message"] as! String
                    self.showToast(message: mesgString, duration: 2.0)
                }
                self.paymentModeTableView.reloadData()
            }
            else if apiStatusCode == 403 {
                let responseDic = responseData as! NSDictionary
                let statusCode =  responseDic["code"] as! Int
                if statusCode == paymentTypeConstant.PAYMENT_WALLET_NOT_ATTACHED {
                    let mesgString = responseDic["message"] as! String
                    self.showToast(message: mesgString, duration: 2.0)
                }
            }
            self.view.isUserInteractionEnabled = true
            self.hideActivityLoader()
        },
        callbackFaliure: { (error:String, message:String) -> Void in
            self.view.isUserInteractionEnabled = true
            self.hideActivityLoader()
            self.showToast(message: message, duration: 2.0)
        })
    }
    
    //TODO :- Func to set change switch btn state
    func switchSelectionAction(selected_Index: Int, selected_Section:Int, selectedCell: PaymentModeTableViewCell) { //switchValue: Int, selected_Index: Int
        
        var indexDataDic = NSDictionary()
        
        if selected_Section == 0 {
            indexDataDic = defaultPaymentArray[selected_Index] as! NSDictionary
        }
        else if selected_Section == 1 {
            indexDataDic = linkedPaymentArray[selected_Index] as! NSDictionary
        }
        else {
            indexDataDic = remainingPaymentArray[selected_Index] as! NSDictionary
        }
        
        let paymentMode = indexDataDic["paymentMode"] as! String
        let switchOnStatus = selectedCell.paymentSwitch.isOn
        
        if switchOnStatus == true {
            if (paymentMode == paymentTypeConstant.PAYMENT_TYPE_PAYTM || paymentMode == paymentTypeConstant.PAYMENT_TPYE_MOBIKWIK_WALLET) {
                self.sentOtpAction(paymentType: paymentMode)
            }
        }
        else {
            self.deLinkWalletAction(paymentType: paymentMode)
        }
    }
    
    //TODO :- Func to set send otp btn action
    func sentOtpAction(paymentType: String) {
        let Base_URL = Utilities.returnBaseURL(liveURL: liveApi)
        let apiURl = Base_URL + PaymentApi.GENERATE_WALLET_OTP_URL + "?selectedWallet=\(paymentType)"

        self.view.isUserInteractionEnabled = false
        self.showActivityLoader()
        ServiceManager.sharedInstance.callApi(url:apiURl, param_Values: nil, requestMethod: "post", callbackSuccess: { (responseData, apiStatusCode) -> Void in

            if apiStatusCode == 200 {
                if !Utilities.isEmptyValue(responseData) {
                    let responseDic = responseData as! NSDictionary
                    let statsuCode = responseDic["code"] as! Int
                    
                    if statsuCode == 2000 {
                        let otpVerifyVC = PaytmOtpVerificationVC()
                        otpVerifyVC.paymentWallet = paymentType
                        otpVerifyVC.selected_Module = self.selectedModule
                        self.navigationController?.pushViewController(otpVerifyVC, animated: true)
                    }
                    else if statsuCode == paymentTypeConstant.OTP_SEND_FAIL {
                        let statsuMesg = responseDic["message"] as! String
                        self.showToast(message: statsuMesg, duration: 2.0)
                    }
                    self.paymentModeTableView.reloadData()
                }
            }
            else if apiStatusCode == 401 {
                let responseDic = responseData as! NSDictionary
                let statusCode =  responseDic["code"] as! Int
                if statusCode == constantStatusCode.NOT_REGISTERED_USER {
                    let mesgString = responseDic["message"] as! String
                    self.showToast(message: mesgString, duration: 2.0)
                }
                self.paymentModeTableView.reloadData()
            }
            self.view.isUserInteractionEnabled = true
            self.hideActivityLoader()
        },
        callbackFaliure: { (error:String, message:String) -> Void in
            self.view.isUserInteractionEnabled = true
            self.hideActivityLoader()
            self.showToast(message: message, duration: 2.0)
        })
    }
 
    //TODO :- Func to de link wallet action
    func deLinkWalletAction(paymentType: String) {
        let Base_URL = Utilities.returnBaseURL(liveURL: liveApi)
        let apiURl = Base_URL + PaymentApi.DELINK_PAYMENT_MODE + "?selectedMode=\(paymentType)&cardToken=\("")"

        self.view.isUserInteractionEnabled = false
        ServiceManager.sharedInstance.callApi(url:apiURl, param_Values: nil, requestMethod: "post", callbackSuccess: { (responseData, apiStatusCode) -> Void in

            if apiStatusCode == 200 {
                if !Utilities.isEmptyValue(responseData) {
                    let responseDic = responseData as! NSDictionary
                    let statsuCode = responseDic["code"] as! Int
                    
                    if statsuCode == 2000 {
                        self.defaults.removeObject(forKey:UserDefaults.UserDefaultsKeys.paymentLinkedWalletData.rawValue)
                        self.defaults.setUserPaymentLinkWallet(value: "")
                        self.fetchPaymentModes()
                    }
                }
            }
            self.view.isUserInteractionEnabled = true
        },
        callbackFaliure: { (error:String, message:String) -> Void in
            self.view.isUserInteractionEnabled = true
            self.showToast(message: message, duration: 2.0)
        })
    }
    
    //TODO :- Func to add zypp coin btn action
    @IBAction func coinBtnAction(_ sender: Any) {
        let ZyppCoinsVC = ZyppCoinsMainVC()
        self.navigationController?.pushViewController(ZyppCoinsVC, animated: true)
    }
    
}
