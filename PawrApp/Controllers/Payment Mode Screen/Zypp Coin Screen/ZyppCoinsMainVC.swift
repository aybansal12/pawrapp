//
//  ZyppCoinsMainVC.swift
//  PawrApp
//
//  Created by Ayush Bansal on 04/05/21.
//

import UIKit

class ZyppCoinsMainVC: UIViewController, navBakButtonWithPressed, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var navHeaderBarView: UIView!
    @IBOutlet weak var zyppCoinCollectionView: UICollectionView!
    
    @IBOutlet weak var proceedBtn: UIButton!
    
    var navBarView : NavBackView?
    var zyppCoinPlanArray = NSMutableArray()
    var selectedPlan = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navBarView = Bundle.main.loadNibNamed("NavBackView", owner: self, options: nil)?.first as? NavBackView
        navBarView?.navBackBtnDelegates = self
        navBarView?.navTitleLabel.text = "Zypp Coins Plans"
        navBarView?.navTitleLabel.textColor = UIColor.black
        
        navBarView?.navBackButton.backgroundColor = UIColor.white
        navBarView?.navBackButton.setImage(UIImage(named: "filledBackIcon"), for: .normal)
        navBarView?.navBackButton.layer.cornerRadius = (navBarView?.navBackButton.frame.height)!/2
        navBarView?.navBackButton.layer.masksToBounds = true
        Utilities.addShadowWithRadius(targetView: (navBarView?.navBackButton)!, widthValue: 0.0, HeightValue: 0.0, shadowOpacity: 0.3, radiusValue: 2.5, colorName: UIColor.black)
        
        navBarView?.mainContentView.frame = navHeaderBarView.frame
        navBarView?.mainContentView.backgroundColor = UIColor.white
        self.navHeaderBarView.addSubview(navBarView!)
        
        DispatchQueue.main.async {
            self.getZyppCoinPlans()
        }
        
        zyppCoinCollectionView.dataSource = self
        zyppCoinCollectionView.delegate = self
        
        let coinPlans = UINib(nibName : "ZyppCoinCollectionViewCell" , bundle:nil)
        zyppCoinCollectionView.register(coinPlans, forCellWithReuseIdentifier: "ZyppCoinCollectionCell")
        
        pageDesignLayout()
    }
    
    //TODO :- Did Press Back Button
    func didPressButton(button: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //TODO :- Func to set page design layout
    func pageDesignLayout() {
        //zyppCoinCollectionView.backgroundColor = customColor.BG_GRAY_COLOR
        
        proceedBtn.setTitle("Proceed", for: .normal)
        pageDesign.buttonLayout(to: proceedBtn, textColor: UIColor.white, font_Size: FontSize.HEADINGFONT16, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD, bgColor: customColor.NEW_THEME_GREEN_COLOR)
        
        proceedBtn.layer.cornerRadius = 5.0
        proceedBtn.layer.masksToBounds = true
    }
    
    //TODO :- Func to fetch zypp coin plan
    func getZyppCoinPlans() {
        let Base_URL = Utilities.returnBaseURL(liveURL: liveApi)
        let apiURl = Base_URL + ZYPP_COIN_API.FETCH_ZYPP_COIN_PACKAGES

        self.view.isUserInteractionEnabled = false
        self.showActivityLoader()
        ServiceManager.sharedInstance.callApi(url:apiURl, param_Values: nil, requestMethod: "get", callbackSuccess: { (responseData, apiStatusCode) -> Void in

            if apiStatusCode == 200 {
                if !Utilities.isEmptyValue(responseData) {
                    self.zyppCoinPlanArray.removeAllObjects()
                    self.zyppCoinPlanArray = (responseData as! NSArray).mutableCopy() as! NSMutableArray
                    
                    if self.zyppCoinPlanArray.count > 0 {
                        self.zyppCoinCollectionView.reloadData()
                    }
                }
            }
            self.view.isUserInteractionEnabled = true
            self.hideActivityLoader()
        },
        callbackFaliure: { (error:String, message:String) -> Void in
            self.view.isUserInteractionEnabled = true
            self.hideActivityLoader()
            self.showToast(message: message, duration: 2.0)
        })
    }
    
    //TODO :- Func to set collection view data source and delegates
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.zyppCoinPlanArray.count > 0 {
            return self.zyppCoinPlanArray.count
        }
        else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = zyppCoinCollectionView.dequeueReusableCell(withReuseIdentifier: "ZyppCoinCollectionCell", for: indexPath as IndexPath) as! ZyppCoinCollectionViewCell
        
        let indexDic = self.zyppCoinPlanArray[indexPath.row] as! NSDictionary
        cell.setCellData(dataDic: indexDic)
        
        if indexPath.row == self.selectedPlan {
            cell.contentView.layer.borderColor = customColor.NEW_THEME_GREEN_COLOR.cgColor
        }
        else {
            cell.contentView.layer.borderColor = customColor.BG_GRAY_COLOR.cgColor
        }
        
        cell.planDetailBtn.isUserInteractionEnabled = true
        let planDetailsGesture = UITapGestureRecognizer(target: self, action: #selector(ZyppCoinsMainVC.checkPlanDetailsAction(_:)))
        cell.planDetailBtn.tag = indexPath.row
        cell.planDetailBtn.addGestureRecognizer(planDetailsGesture)
        
        return cell
    }
    
    //TODO :- Func to select zypp coin plans
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.selectedPlan = indexPath.row
        self.zyppCoinCollectionView.reloadData()
    }
    
    //TODO :- Collection view flow layouts
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if Utilities.getScreenWidth() > 428 {
            let cellSize = CGSize(width: (collectionView.bounds.width - 50)/3, height: 120)
            return cellSize
        }
        else {
            let cellSize = CGSize(width: (collectionView.bounds.width - 50)/2, height: 120)
            return cellSize
        }
    }
    
    // TODO :- When map image is clicked
    @objc func checkPlanDetailsAction(_ sender:UITapGestureRecognizer){
        if let tag = sender.view?.tag {
            let indexDic = self.zyppCoinPlanArray[tag] as! NSDictionary
            
            let description = indexDic["description"] as! String
            Utilities.showAlertView(description, title: "Zypp Coins")
        }
    }
    
    //TODO :- Func to set proceed btn action
    @IBAction func proceedBtnAction(_ sender: Any) {
        if self.selectedPlan == -1 {
            self.showToast(message: "Select Zypp Coin Plan First!", duration: 2.0)
        }
        else {
            let indexDic = self.zyppCoinPlanArray[selectedPlan] as! NSDictionary
            let zyppCoin = indexDic["mobyCoins"] as! Int
            let zyppCoinPrice = indexDic["amountPay"] as! Int
            
            let messageString = "Kindly confirm: \(zyppCoin) MobyCoins in ₹ \(zyppCoinPrice)."
            Utilities.showAlertControllerAction(message: messageString, title: "Confirmation", styleType: "alert", onView: self, andFirstButtonTitle: "Cancel", andSecondButtonTitle:"OK", andFirstAction: nil, andSecondAction: #selector(self.purchaseZyppCoinAction))
        }
    }
    
    // TODO :- Func to logout btn action
    @objc func purchaseZyppCoinAction(){
        let indexDic = self.zyppCoinPlanArray[selectedPlan] as! NSDictionary
        let packageID = indexDic["packageId"] as! Int
        
        let Base_URL = Utilities.returnBaseURL(liveURL: liveApi)
        let apiURl = Base_URL + ZYPP_COIN_API.PURCHASE_ZYPP_COIN + "/\(packageID)"

        self.view.isUserInteractionEnabled = false
        self.showActivityLoader()
        ServiceManager.sharedInstance.callApi(url:apiURl, param_Values: nil, requestMethod: "post", callbackSuccess: { (responseData, apiStatusCode) -> Void in

            if apiStatusCode == 200 {
                if !Utilities.isEmptyValue(responseData) {
                    let responseDic = responseData as! NSDictionary
                    let statsuCode = responseDic["code"] as! Int
                    
                    if statsuCode == 200 {
                        print("successfully")
                    }
                    else {
                        let mesgString = responseDic["message"] as! String
                        self.showToast(message: mesgString, duration: 2.0)
                    }
                   
                }
            }
            self.view.isUserInteractionEnabled = true
            self.hideActivityLoader()
        },
        callbackFaliure: { (error:String, message:String) -> Void in
            self.view.isUserInteractionEnabled = true
            self.hideActivityLoader()
            self.showToast(message: message, duration: 2.0)
        })
    }
    
}

