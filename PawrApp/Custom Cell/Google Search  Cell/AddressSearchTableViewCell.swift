//
//  AddressSearchTableViewCell.swift
//  PawrApp
//
//  Created by Ayush Bansal on 06/05/21.
//

import UIKit

class AddressSearchTableViewCell: UITableViewCell {

    @IBOutlet weak var LocationHeadLbl: UILabel!
    @IBOutlet weak var completeAddressLbl: UILabel!
    @IBOutlet weak var seperateLine: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        cellDesignLayout()
    }
    
    //TODO :- Func to set cell design layout
    func cellDesignLayout() {
        pageDesign.labelLayout(to: LocationHeadLbl, textColor: UIColor.black, font_Size: FontSize.HEADINGFONT15, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
        
        pageDesign.labelLayout(to: completeAddressLbl, textColor: customColor.LIGHT_BLACK_COLOR, font_Size: FontSize.HEADINGFONT13, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
        
        seperateLine.backgroundColor = customColor.BG_GRAY_COLOR
    }
    
    //TODO :- Func to set cell data
    func setCellData(dataDic: NSDictionary) {
        let addressDes = dataDic["description"] as! String
        
        let termArray = dataDic["terms"] as! NSArray
        let termIndexArray = termArray[0] as! NSDictionary
        let termValue = termIndexArray["value"] as! String
        
        LocationHeadLbl.text = termValue
        completeAddressLbl.text = addressDes
     }
}
