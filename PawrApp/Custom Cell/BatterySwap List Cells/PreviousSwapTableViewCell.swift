//
//  PreviousSwapTableViewCell.swift
//  PawrApp
//
//  Created by Ayush Bansal on 12/03/21.
//

import UIKit

@available(iOS 13.0, *)
class PreviousSwapTableViewCell: UITableViewCell {

    @IBOutlet weak var dataContentView: UIView!
    @IBOutlet weak var stationNameLbl: UILabel!
    @IBOutlet weak var swapDateLbl: UILabel!
    @IBOutlet weak var swapPriceLbl: UILabel!
    @IBOutlet weak var seperatorLine1: UIView!
    @IBOutlet weak var oldBatteryQRLbl: UILabel!
    @IBOutlet weak var oldQrCodeLbl: UILabel!
    @IBOutlet weak var oldSwapAtLbl: UILabel!
    @IBOutlet weak var oldSwapPerLbl: UILabel!
    @IBOutlet weak var newBatteryQrLbl: UILabel!
    @IBOutlet weak var newBatteryQrCodeLbl: UILabel!
    @IBOutlet weak var newSwapAtLbl: UILabel!
    @IBOutlet weak var newSwapPerLbl: UILabel!
    @IBOutlet weak var seperatorLine2: UIView!
    @IBOutlet weak var repeatBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        cellDesignLayout()
    }
    
    //TODO :- Func to set cell design layout
    func cellDesignLayout() {
        //self.contentView.backgroundColor = customColor.BG_GRAY_COLOR
        
        dataContentView.layer.cornerRadius = 5.0
        dataContentView.layer.masksToBounds = true
        Utilities.addShadowWithRadius(targetView: dataContentView, widthValue: 0.0, HeightValue: 0.0, shadowOpacity: 0.3, radiusValue: 2.0, colorName: UIColor.black)
        
        seperatorLine1.backgroundColor = customColor.TEXT_LIGHT_GRAY_COLOR
        seperatorLine2.backgroundColor = customColor.TEXT_LIGHT_GRAY_COLOR
        
        pageDesign.labelLayout(to: stationNameLbl, textColor: UIColor.black, font_Size: FontSize.HEADINGFONT15, fontFamily: TextFontStyle.MONTSERRAT_BOLD)
        
        pageDesign.labelLayout(to: swapDateLbl, textColor: customColor.TEXT_DARK_GRAY_COLOR, font_Size: FontSize.HEADINGFONT12, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
        
        pageDesign.labelLayout(to: swapPriceLbl, textColor: UIColor.black, font_Size: FontSize.HEADINGFONT16, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
        
        oldBatteryQRLbl.text = "Old Battery QR Code:"
        pageDesign.labelLayout(to: oldBatteryQRLbl, textColor: customColor.TEXT_DARK_GRAY_COLOR, font_Size: FontSize.HEADINGFONT12, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
        
        pageDesign.labelLayout(to: oldQrCodeLbl, textColor: UIColor.black, font_Size: FontSize.HEADINGFONT14, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
        
        oldSwapAtLbl.text = "Swapped At:"
        pageDesign.labelLayout(to: oldSwapAtLbl, textColor: customColor.TEXT_DARK_GRAY_COLOR, font_Size: FontSize.HEADINGFONT12, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
        
        oldSwapPerLbl.text = "7%"
        pageDesign.labelLayout(to: oldSwapPerLbl, textColor: UIColor.black, font_Size: FontSize.HEADINGFONT14, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
        
        newBatteryQrLbl.text = "New Battery QR Code:"
        pageDesign.labelLayout(to: newBatteryQrLbl, textColor: customColor.TEXT_DARK_GRAY_COLOR, font_Size: FontSize.HEADINGFONT12, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
        
        pageDesign.labelLayout(to: newBatteryQrCodeLbl, textColor: UIColor.black, font_Size: FontSize.HEADINGFONT14, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
        
        newSwapAtLbl.text = "Swapped At:"
        pageDesign.labelLayout(to: newSwapAtLbl, textColor: customColor.TEXT_DARK_GRAY_COLOR, font_Size: FontSize.HEADINGFONT12, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
        
        newSwapPerLbl.text = "7%"
        pageDesign.labelLayout(to: newSwapPerLbl, textColor: UIColor.black, font_Size: FontSize.HEADINGFONT14, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
        
        
        repeatBtn.backgroundColor = customColor.NEW_THEME_GREEN_COLOR
        repeatBtn.layer.cornerRadius = 7.0
        repeatBtn.layer.masksToBounds = true
        
        repeatBtn.setTitle("Repeat", for: .normal)
        pageDesign.buttonLayout(to: repeatBtn, textColor: UIColor.white, font_Size: FontSize.HEADINGFONT13, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD, bgColor: customColor.NEW_THEME_GREEN_COLOR)
        
        repeatBtn.isHidden = true
        newSwapPerLbl.isHidden = true
        newSwapAtLbl.isHidden = true
        oldSwapPerLbl.isHidden = true
        oldSwapAtLbl.isHidden = true
    }
    
    //TODO :- Func to set cell data
    func setCellData(dataDic : NSDictionary) {
        let placeName = dataDic["placeName"] as! String
        let newQR_Code = dataDic["newQrCode"] as! String
        let oldQR_Code = dataDic["oldQrCode"] as! String
        let swapAmount = dataDic["paidAmount"] as! Double
        let modifiedTime = dataDic["modifiedTime"] as! String
        
        print(modifiedTime)
        if modifiedTime != "" {
            let dateString = UtilitiesValues.compareCurrentTimeDeliveryTime(batterySwapTime: modifiedTime)
            swapDateLbl.text = dateString
            swapDateLbl.isHidden = false
        }
        else {
            swapDateLbl.isHidden = true
        }
        
        stationNameLbl.text = placeName
        oldQrCodeLbl.text = oldQR_Code
        newBatteryQrCodeLbl.text = newQR_Code
        swapPriceLbl.text = "\u{20B9} \(String(swapAmount))"
    }
}
