//
//  BluetoothListTableViewCell.swift
//  PawrApp
//
//  Created by Ayush Bansal on 23/03/21.
//

import UIKit

class BluetoothListTableViewCell: UITableViewCell {

    @IBOutlet weak var bluetoothNameLbl: UILabel!
    @IBOutlet weak var seperateLine: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        cellDesignLayout()
    }
    
    //TODO :- Func to set cell design layout
    func cellDesignLayout() {
        pageDesign.labelLayout(to: bluetoothNameLbl, textColor: UIColor.black, font_Size: FontSize.HEADINGFONT16, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
        
        seperateLine.backgroundColor = customColor.BG_GRAY_COLOR
    }
    
    //TODO :- Func to set cell data
    func setCellData(textString: String) {
        bluetoothNameLbl.text = textString
    }
}
