//
//  PaymentPlanCollectionViewCell.swift
//  PawrApp
//
//  Created by Ayush Bansal on 07/04/21.
//

import UIKit

class PaymentPlanCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var dataContainerView: UIView!
    @IBOutlet weak var planNameLbl: UILabel!
    @IBOutlet weak var planPriceLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        cellDesignLayout()
    }
    
    //TODO :- Func to set cell design layout
    func cellDesignLayout() {
        
        dataContainerView.layer.cornerRadius = 10.0
        dataContainerView.layer.masksToBounds = true
        
        planNameLbl.textColor = customColor.LIGHT_BLACK_COLOR
        planNameLbl.font = UIFont(name: TextFontStyle.MONTSERRAT_SEMIBOLD, size: CGFloat(FontSize.HEADINGFONT14))
        
        planPriceLbl.textColor = customColor.LIGHT_BLACK_COLOR
        planPriceLbl.font = UIFont(name: TextFontStyle.MONTSERRAT_SEMIBOLD, size: CGFloat(FontSize.HEADINGFONT20))
    }
    
    func changeSelectedCellShadow(selectedStatus : Bool) {
        if (selectedStatus == true) {
            dataContainerView.layer.borderWidth = 1.5
            dataContainerView.layer.borderColor = customColor.NEW_THEME_GREEN_COLOR.cgColor
//            Utilities.addShadowWithRadius(targetView: dataContainerView, widthValue: 0.0, HeightValue: 0.0, shadowOpacity: 0.5, radiusValue: 5.0, colorName: customColor.NEW_THEME_GREEN_COLOR)
        }
        else {
            dataContainerView.layer.borderWidth = 1.5
            dataContainerView.layer.borderColor = customColor.BG_GRAY_COLOR.cgColor
//            Utilities.addShadowWithRadius(targetView: dataContainerView, widthValue: 0.0, HeightValue: 0.0, shadowOpacity: 0.3, radiusValue: 2.0, colorName: UIColor.black)
        }
    }

}
