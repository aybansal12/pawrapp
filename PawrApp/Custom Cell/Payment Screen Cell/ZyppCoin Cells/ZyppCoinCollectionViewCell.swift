//
//  ZyppCoinCollectionViewCell.swift
//  PawrApp
//
//  Created by Ayush Bansal on 10/05/21.
//

import UIKit

class ZyppCoinCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var dataContentView: UIView!
    
    @IBOutlet weak var getLbl: UILabel!
    @IBOutlet weak var coinValueBtn: UIButton!
    @IBOutlet weak var coinPriceBtn: UIButton!
    @IBOutlet weak var planDetailBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        cellDesignLayout()
    }

    //TODO :- Func to set cell design layout
    func cellDesignLayout(){
        dataContentView.backgroundColor = customColor.BG_GRAY_COLOR
        self.contentView.layer.cornerRadius = 20.0
        self.contentView.layer.masksToBounds = true
        
        self.contentView.layer.borderWidth = 1.25
        
        pageDesign.labelLayout(to: getLbl, textColor: customColor.LIGHT_BLACK_COLOR, font_Size: FontSize.HEADINGFONT14, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
        
        pageDesign.buttonLayout(to: coinValueBtn, textColor: UIColor.black, font_Size: FontSize.HEADINGFONT16, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD, bgColor: UIColor.clear)
        
        pageDesign.buttonLayout(to: coinPriceBtn, textColor: UIColor.black, font_Size: FontSize.HEADINGFONT15, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD, bgColor: UIColor.clear)
    }
    
    //TODO :- Func to set cell data
    func setCellData(dataDic: NSDictionary) {
        let zyppCoin = dataDic["mobyCoins"] as! Int
        let zyppCoinPrice = dataDic["amountPay"] as! Int
        
        let coinValue = String(zyppCoin) + " in"
        coinValueBtn.setTitle(coinValue, for: .normal)
        
        let coinPrice = String(zyppCoinPrice)
        coinPriceBtn.setTitle(coinPrice, for: .normal)
    }
    
}
