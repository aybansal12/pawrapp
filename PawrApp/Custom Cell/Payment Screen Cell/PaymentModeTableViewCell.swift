//
//  PaymentModeTableViewCell.swift
//  PawrApp
//
//  Created by Ayush Bansal on 15/04/21.
//

import UIKit
import SDWebImage

protocol switchSelectionProtocol {
    func switchSelectionAction(selected_Index: Int, selected_Section: Int, selectedCell: PaymentModeTableViewCell)
}

class PaymentModeTableViewCell: UITableViewCell {

    @IBOutlet weak var dataContentView: UIView!
    @IBOutlet weak var paymentImageView: UIImageView!
    @IBOutlet weak var paymentTitleLbl: UILabel!
    @IBOutlet weak var paymentSwitch: UISwitch!
    @IBOutlet weak var paymentLinkLbl: UILabel!
    
    var switchSelectionDelegate : switchSelectionProtocol!
    var selectedIndex : Int!
    var selectedSection : Int!
    var switchState : Int!
        
    override func awakeFromNib() {
        super.awakeFromNib()
        
        cellDesignLayout()
    }
    
    //TODO :- Func to set cell design layout
    func cellDesignLayout() {
        
        dataContentView.layer.cornerRadius = 5.0
        dataContentView.layer.masksToBounds = true
        Utilities.addShadowWithRadius(targetView: dataContentView, widthValue: 0.0, HeightValue: 0.0, shadowOpacity: 0.3, radiusValue: 1.75, colorName: UIColor.black)
//        dataContentView.layer.borderWidth = 1.5
//        dataContentView.layer.borderColor = customColor.NEW_THEME_GREEN_COLOR.cgColor
        
        pageDesign.labelLayout(to: paymentTitleLbl, textColor: UIColor.black, font_Size: FontSize.HEADINGFONT15, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
        
        pageDesign.labelLayout(to: paymentLinkLbl, textColor: customColor.NEW_THEME_GREEN_COLOR, font_Size: FontSize.HEADINGFONT12, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
    }
    
    //TODO :- Func to set cell data
    func setCellData(dataDic: NSDictionary) {
        
        var placeholderImage = UIImage(named: "")
        let paymentMode = dataDic["paymentMode"] as! String
        let linkedStatus = dataDic["linkedStatus"] as! Int

        if paymentMode == paymentTypeConstant.PAYMENT_TYPE_PAYTM {
            paymentTitleLbl.text = "Paytm Wallet"
            placeholderImage = UIImage(named: "Paytmlogo")
        }
        else if paymentMode == paymentTypeConstant.PAYMENT_TPYE_MOBIKWIK_WALLET {
            paymentTitleLbl.text = "Mobikwik Wallet"
            placeholderImage = UIImage(named: "Mobikwik")
        }

        if linkedStatus == 0 {
            paymentSwitch.isOn = false
            switchState = 0
            paymentLinkLbl.isHidden = true
        }
        else {
            paymentLinkLbl.isHidden = false
            paymentLinkLbl.text = "Linked"
            paymentSwitch.isOn = true
            switchState = 1
        }
        
        var paymentOptionImgID = 0
        if ((dataDic.value(forKey: "paymentImg")) != nil) {
            paymentOptionImgID = dataDic["paymentImg"] as! Int
        }
        else {
            paymentOptionImgID = 0
        }
        
        let Base_URL = Utilities.returnBaseURL(liveURL: liveApi)
        let urlString = Base_URL + ApiList.LOAD_PIC_FROM_SERVER + String(paymentOptionImgID)

        paymentImageView.sd_setImage(with: URL(string: urlString), placeholderImage: placeholderImage, completed: { image, error, cacheType, imageURL in
            if image != nil {
                self.paymentImageView.image = image
            }
        })
    }
    
    //TODO :- Func to select switch btn action
    @IBAction func switchBtnAction(_ sender: Any) {
        switchSelectionDelegate.switchSelectionAction(selected_Index: selectedIndex!, selected_Section:selectedSection, selectedCell: self)
    }
}
