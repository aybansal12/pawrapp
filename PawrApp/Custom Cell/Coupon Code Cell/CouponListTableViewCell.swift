//
//  CouponListTableViewCell.swift
//  PawrApp
//
//  Created by Ayush Bansal on 17/05/21.
//

import UIKit

class CouponListTableViewCell: UITableViewCell {

    @IBOutlet weak var dataContentView: UIView!
    @IBOutlet weak var couponTitleLbl: UILabel!
    @IBOutlet weak var useCouponBtn: UIButton!
    @IBOutlet weak var couponDesLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        cellDesignLayout()
    }
    
    //TODO :- Func to set cell design layout
    func cellDesignLayout() {
        pageDesign.labelLayout(to: couponTitleLbl, textColor: UIColor.black, font_Size: FontSize.HEADINGFONT16, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
        
        useCouponBtn.setTitle("Use Code", for: .normal)
        pageDesign.buttonLayout(to: useCouponBtn, textColor: customColor.NEW_THEME_GREEN_COLOR, font_Size: FontSize.HEADINGFONT14, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD, bgColor: UIColor.clear)
        
        pageDesign.labelLayout(to: couponDesLbl, textColor: customColor.TEXT_DARK_GRAY_COLOR, font_Size: FontSize.HEADINGFONT12, fontFamily: TextFontStyle.MONTSERRAT_SEMIBOLD)
    }
    
    //TODO :- Func to set cell data
    func setCellData(DataDic : NSDictionary){
        let couponCode = DataDic["couponCode"] as! String
        let couponDescription = DataDic["description"] as! String
        
        couponTitleLbl.text = couponCode
        couponDesLbl.text = couponDescription
    }
    
}
